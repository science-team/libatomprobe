#!/bin/bash

# Do not use cmake for testing.
# ctest is very inflexible and a pain to use.
# instead this script will just run each file in turn

make 

pushd test

if [ ! -f naturalAbundance.xml ] ; then
	ln -s ../data/naturalAbundance.xml
fi


ls

FAILED=""
for i in KDTest rangetest unittests
do
	echo
	echo
	echo " --- RUNNING TEST : $i --- "
	./${i}
	if [ $? -ne 0 ] ; then
		FAILED="${FAILED} ${i}"
	fi
	echo " ------------------------- "
	echo
	echo
done
if [ x"$FAILED" != x"" ] ; then
	echo " !!-- UNIT TEST FAILURES: $FAILED -- !!"
	sleep 1
fi 

popd 

