#!/bin/bash

#Script to bootstrap libatomprobe cross-compilation under debian
# and debian-like systems	
# Note that building wx requires ~8GB of ram (or swap)

# Its unlikely that this script will work first-time, or even second-time
# you will need to do quite some work to be able to make this run

#--- Determine which system we wish to build for
HOST_VAL=x86_64-w64-mingw32 #For mingw64 (windows 64 bit)
#HOST_VAL=i686-w64-mingw32 #For mingw32 (Windows 32 bit)

#Should we use Hg to track changes in the unpacked dependencies (where enabled)?
USE_HG=1

#Setup clone of hg repository, if required
if [ ! -d code ] || [ ! -d code/libatomprobe ]  ; then
	read -p "Code dir is missing -- shall we clone it?? (y/n):" answer
	
	case $answer in
		y | Y ) 
			if [ x"`which hg`" == x"" ] ; then
				echo "No mercurial.. Can't clone, aborting."
				exit 1
			fi

			DEFCLONEURL="https://hg.sr.ht/~mycae/libatomprobe"

			read -p "Path to clone (include protocol if url...), blank for default" clonedir

			clonedir=`echo $clonedir | sed 's/^\s*//'`
		
			if [ x"$clonedir" == x"" ] ; then 
				CLONEURL=$DEFCLONEURL
			else
				echo "wront branch"
				CLONEURL=$clonedir	
			fi

			mkdir -p code  || { echo "failed to create code dir" ; exit 1 ; }
			pushd code
			
			#Clone code
			hg clone $CLONEURL  libatomprobe || { echo "Failed to clone" ; exit 1 ; }
			popd

			#obtain patches, and then put it into place
			cp -R code/libatomprobe/packaging/mingw-debian-cross/patches . || { echo "Failed to obtain patches from mingw packging dir" ; exit 1 ; }



			;; 
		n | N )
			echo "You said no."
			;;
		*) 
			echo "Aborting - please answer Y, or N, or create the \"code\" directory"
			exit 1
			;;
	esac #ends the case list
fi

if [ ! -f host_val ] ; then
	echo "Please select 32 or 64 bit by typing \"32\" or \"64\" (32/64)"
	read HOST_VAL
	
	case $HOST_VAL in
		32)
			HOST_VAL="i686-w64-mingw32"
			;;
		64)
			HOST_VAL="x86_64-w64-mingw32"
			;;
		
		*) 
			echo "Didn't understand HOST_VAL. You can override this by editing the script"
			exit 1
		;;
	esac

	#Save for next run
	echo $HOST_VAL > host_val
else
	HOST_VAL=`cat host_val`
fi


if [ $HOST_VAL != "x86_64-w64-mingw32" ] && [ $HOST_VAL != i686-w64-mingw32 ] ; then
	echo "Unknown HOST_VAL"
	exit 1
else 
	case $HOST_VAL in
		x86_64-w64-mingw32)
			BITS_VAL=64
			;;
		i686-w64-mingw32)
			BITS_VAL=32
			;;
		*)
			echo "Should not have got here - bug!"
			exit 1
			;;
	esac
fi

#----

if [ ! -d code/libatomprobe ] || [ ! -f code/libatomprobe/src/libatomprobe.cpp ] ; then
	echo "libatomprobe code dir, \"code/libatomprobe\", appears to be missing. Please place libatomprobe source code in this location"
	echo "Aborting"
	exit 1
fi


BASE=`pwd`
PREFIX=/
NUM_PROCS=4

IS_RELEASE=0

if [ `id -u` -eq 0 ]; then
	echo "This script should not be run as root."
	echo " If you know what you are doing, you can disable this check, but be aware you _can_ break your system by doing this."
	exit 1;
fi

PATCHES_QHULL="qhull-ptr.patch" # qhull-2009-configure_ac.patch"
PATCH_LIST="$PATCHES_GSL $PATCHES_QHULL "

BUILD_STATUS_FILE="$BASE/build-status"
PATCH_STATUS_FILE="$BASE/patch-status"
PATCH_LEVEL=1

function isBuilt()
{
	if [ x`cat ${BUILD_STATUS_FILE} | grep $ISBUILT_ARG` != x"" ]  ; then
		ISBUILT=1
	else
		ISBUILT=0
	fi
}

function createHgRepoAsNeeded
{
	if [ $USE_HG -ne 0 ] ; then

		if [ ! -d .hg ] ; then
			hg init .
			hg add .
			hg commit  -u me -m "Automatic initial commit"
		fi
	fi
}

function applyPatches()
{
	for i in $APPLY_PATCH_ARG
	do
		if [ x"`cat $PATCH_STATUS_FILE | grep -x "$i"`" != x"" ] ; then
			echo "Patch already applied :" $i
			continue
		fi

		echo "Applying patch:" $i
		patch -tN -p$PATCH_LEVEL  < $BASE/patches/$i
		
		if [ $? -ne 0 ] ; then
			echo "Failed applying patch :" $i
			exit 1
		fi
		echo "applied patch"
		echo $i >> $PATCH_STATUS_FILE
	done
}

function install_mingw()
{

	echo "Checking mingw install"
	#install mingw and libtool (we will need it...)

	GET_PACKAGES="";
	for i in $MINGW_PACKAGES
	do
		if [ x`dpkg --get-selections | grep ^$i | awk '{print $1}'  ` != x"$i" ] ; then
			GET_PACKAGES="$GET_PACKAGES $i";
		fi
	done

	if [ x"$GET_PACKAGES" != x"" ] ; then
		echo "Requesting install of mingw :" $GET_PACKAGES
		sudo apt-get install $GET_PACKAGES libtool || { echo "Mingw install failed";  exit 1 ; }
	fi

}

function grabDeps()
{
	pushd deps 2>/dev/null

	DEB_PACKAGES="qhull gsl libxml2"
	DEB_PACKAGES="$DEB_PACKAGES $LIBJPEGNAME"

	GET_PACKAGES=""
	for i in $DEB_PACKAGES
	do

		FNAME=`ls packages/${i}_*.orig.* 2> /dev/null`

		#If filename is empty, we will need to retreive it from
		# interwebs
		if [ x"$FNAME" == x"" ] ; then
			GET_PACKAGES="${GET_PACKAGES} $i"
		fi
	done

	#grab packages if they are not already on-disk
	if [ x"$GET_PACKAGES" != x"" ] ; then
		apt-get source $GET_PACKAGES

		if [ $? -ne 0 ] ; then
			echo "Package retrieval failed"
			echo "apt-get source failed... Maybe check internet connection, then try updating package database, then re-run?"
			echo " other possibilities could include, eg, that the required package is not available in the debian archive.."
			exit 1
		fi

		#Strip patches from the build and patch status files, 
		# if we are retriving new packages
		for i in $GET_PACKAGES
		do
			grep -v $i ../build-status > tmp
			mv tmp ../build-status
			
			grep -v $i ../patch-status > tmp
			mv tmp ../patch-status


		done
	fi


	#Move debian stuff into packages folder
	if [ x"$GET_PACKAGES" != x"" ] ; then
		mv *.orig.* *.debian.* *.dsc *.diff.* packages 
	fi


	#Check that we have untarred all the required packages
	# (eg we downloaded one, and wiped it at some stage)
	# if not, pull the package out, and re-build it
	GET_PACKAGES=""
	for i in $DEB_PACKAGES
	do
		#if we have a package file (dsc), and no folder, add it to the list of packages
		FNAME=`ls packages/$i*dsc 2> /dev/null` 
		DNAME=`ls -ld $i* | grep ^d | awk '{print $NF}'`
		if [ x"$FNAME" != x"" ] && [ x"$DNAME" == x""  ] ; then
			GET_PACKAGES="${GET_PACKAGES} $i"
		fi
	done
	
	#Unpack pre-existing package
	for i in $GET_PACKAGES
	do
		mv packages/$i*.* . || { echo "existing package extraction failed "; exit 1; } 
		dpkg-source -x $i*dsc
		#move package back
		mv ${i}_*.* packages/
		
		#wipe record of any patches for this package
		grep -v $i ../patch-status  > tmp
		mv tmp ../patch-status
		
		#wipe record of build
		grep -v $i ../build-status  > tmp
		mv tmp ../build-status
	done

	#----


	popd 2> /dev/null

}

function createBaseBinaries()
{
	pushd bin >/dev/null

	#Create symlinks to the compiler, which will be in our
	# primary path, and thus override the normal compiler
	
	#use ccache if it is around
	if [  x`which ccache` != x"" ] ; then
		echo -n "Enabling ccache support..."
		USE_CCACHE=1
	else
		echo -n "Making direct compiler symlinks..."
		USE_CCACHE=0
	fi

	for i in g++ cpp gcc c++
	do
		#Skip existing links
		if [ -f ${HOST_VAL}-$i ] || [ -L ${HOST_VAL}-$i ] ; then
			continue;
		fi

		if [ $USE_CCACHE == 1 ] ; then

			ln -s `which ccache` ${HOST_VAL}-$i
		else

			ln -s `which  ${HOST_VAL}-$i`
			
		fi
		
		if [ $? -ne 0 ] ; then
			echo "Failed when making compiler symlinks"
			exit 1
		fi
	done

	echo "done"
	popd >/dev/null
}

function fix_la_file
{
	#Need to fix .la files which have wrong path
	# due to libtool not liking cross-compile
	pushd lib/ >/dev/null
	sed -i 's@//lib@'${BASE}/lib@ ${FIX_LA_FILE_ARG}*.la
	sed -i 's@/lib/lib/@/lib/@'  ${FIX_LA_FILE_ARG}*.la
	popd >/dev/null
}

function build_zlib()
{
	ISBUILT_ARG="zlib"
	isBuilt
	if [ $ISBUILT -eq 1 ] ; then
		return;
	fi

	pushd deps >/dev/null
	pushd zlib-* >/dev/null


	if [ $? -ne 0 ] ; then
		echo "Zlib dir missing, or duplicated?"
		exit 1
	fi

	createHgRepoAsNeeded
	
	make clean
	rm -f configure.log

	
	./configure  --prefix="/" || { echo "ZLib configure failed"; exit 1; } 
	
	APPLY_PATCH_ARG="$PATCHES_ZLIB"
	applyPatches

	make -j $NUM_PROCS || { echo "ZLib build failed"; exit 1; } 

	make install DESTDIR="$BASE"|| { echo "ZLib install failed"; exit 1; } 

	#Move the .so to a .dll
	mv ${BASE}/lib/libz.so ${BASE}/lib/libz.dll

	#Remove the static zlib. We don't want it
	rm $BASE/lib/libz.a

	popd >/dev/null
	popd >/dev/null

	echo "zlib" >> $BUILD_STATUS_FILE
}


function build_libpng()
{
	ISBUILT_ARG="libpng"
	isBuilt
	if [ $ISBUILT -eq 1 ] ; then
		return;
	fi
	pushd deps >/dev/null
	pushd libpng[0-9.]*-* >/dev/null
	
	if [ $? -ne 0 ] ; then
		echo "libpng dir missing, or duplicated?"
		exit 1
	fi

	createHgRepoAsNeeded

	make clean

	./configure --host=$HOST_VAL --prefix=/ --enable-shared --disable-static || { echo "Libpng configure failed"; exit 1; } 

	#Hack to strip linker version script
	# eg as discussed : 
	# https://github.com/Pivosgroup/buildroot-linux/issues/2
	# The error is not 100% clear. if the build script has 
	# access to standard debian -I paths, then it goes away
	# version script documentation is sparse to non-existant.
	# the only really useful thing i've found is this:
	# http://www.akkadia.org/drepper/dsohowto.pdf
	sed -i 's/.*version-script.*//' Makefile Makefile.am

	make -j $NUM_PROCS  || { echo "libpng build failed"; exit 1; } 
	
	make install DESTDIR="$BASE"|| { echo "libpng install failed"; exit 1; } 

	#The new libpng shoudl install (when it works, which it does not for us)
	# the headers in a new subfolder, which is not compatible with most buildsystems
	# so make some symlinks
	#---
	# First simulate libpng's installer working (TODO: Why is this not working? It did before..)
	mkdir -p ../../include/libpng12/ 
	cp -p png.h pngconf.h ../../include/libpng12/
	# Then actually make the symlinks
	pushd ../../include/ > /dev/null
	ln -s libpng12/pngconf.h
	ln -s libpng12/png.h
	popd >/dev/null

	#Aaand it doesn't install the libraries, so do that too.
	cp -p .libs/*.dll $BASE/lib/
	cp -p .libs/*.la $BASE/lib/
	#----


	#Remove superseded libpng3
	rm ${BASE}/lib/libpng-3*
	ln -s ${BASE}/lib/libpng12-0.dll  ${BASE}/lib/libpng.dll

	popd >/dev/null
	popd >/dev/null

	FIX_LA_FILE_ARG=libpng
	fix_la_file
	
	echo "libpng" >> $BUILD_STATUS_FILE
}

function build_libxml2()
{
	NAME="libxml2"
	ISBUILT_ARG=${NAME}
	isBuilt
	if [ $ISBUILT -eq 1 ] ; then
		return;
	fi

	pushd deps >/dev/null
	LIBXMLVER=`ls -d libxml2-* | sed 's/libxml2-//'`
	pushd libxml2-* >/dev/null
	
	if [ $? -ne 0 ] ; then
		echo "libxml2 dir missing, or duplicated?"
		exit 1
	fi

	createHgRepoAsNeeded
	
	#Libxml 2.8 and up doesn't need patching.
	# note that --compare-versions returns 0 on truth, and 1 on false
	dpkg --compare-versions  $LIBXMLVER lt 2.8
	if [ $? -eq 0 ] ; then
		echo "WARNING : Previous attempts to use libxml2 < 2.8 have failed."
		echo " it is recommended to manually obtain the .dsc,.orig.tar.gz and .debian.tar.gz from"
		echo " http://packages.debian.org/source/wheezy/libxml2 . download these, then replace the .dsc,.orig.tar.gz and .debian.tar.gz in deps/packages/"
		exit 1
	fi

	make clean

	#Modifications
	#	Disable python, because sys/select.h is not in mingw
	./configure --host=$HOST_VAL --without-lzma --without-python --without-html --without-http --without-ftp --without-push --without-writer --without-push --without-legacy --without-xpath --without-iconv  --enable-shared=yes --enable-static=no --prefix=/ || { echo "Libxml2 configure failed"; exit 1; } 

	make -j $NUM_PROCS || { echo "libxml2 build failed"; exit 1; } 
	
	make install DESTDIR="$BASE"|| { echo "libxml2 install failed"; exit 1; } 

	popd >/dev/null
	popd >/dev/null

	FIX_LA_FILE_ARG=libxml2
	fix_la_file

	#For compat, link libxml2/libxml to libxml/
	pushd include >/dev/null
	ln -s libxml2/libxml libxml
	popd >/dev/null

	#The dll gets installed into bin/, link it to lib/
	pushd lib > /dev/null
	ln -s ../bin/libxml2-[0-9]*.dll 
	popd > /dev/null


	echo ${NAME} >> $BUILD_STATUS_FILE
}

function build_libjpeg()
{
		
	NAME=$LIBJPEGNAME
	ISBUILT_ARG=${NAME}
	isBuilt
	if [ $ISBUILT -eq 1 ] ; then
		return;
	fi
	pushd deps >/dev/null
	pushd ${NAME}*[0-9]* >/dev/null
	
	if [ $? -ne 0 ] ; then
		echo "${NAME} dir missing, or duplicated?"
		exit 1
	fi

	createHgRepoAsNeeded

	make clean

	autoconf

	./configure --host=$HOST_VAL --enable-shared --disable-static --prefix=/ || { echo "$NAME configure failed"; exit 1; } 

	make -j $NUM_PROCS || { echo "$NAME build failed"; exit 1; } 
	
	make install DESTDIR="$BASE"|| { echo "$NAME install failed"; exit 1; } 
	
	#DLL needs to be copied into lib manually
	cp -p .libs/${NAME}-[0-9]*.dll $BASE/lib/ 

	popd >/dev/null
	popd >/dev/null

	FIX_LA_FILE_ARG=$NAME
	fix_la_file
	echo ${NAME}  >> $BUILD_STATUS_FILE
}

function build_libtiff()
{
	NAME="libtiff"
	ISBUILT_ARG=${NAME}
	isBuilt
	if [ $ISBUILT -eq 1 ] ; then
		return;
	fi
	pushd deps >/dev/null
	pushd tiff*[0-9]* >/dev/null
	
	if [ $? -ne 0 ] ; then
		echo "libtiff dir missing, or duplicated?"
		exit 1
	fi
	
	createHgRepoAsNeeded
	
	./configure --host=$HOST_VAL --enable-shared --disable-static --prefix=/ || { echo "Libtiff configure failed"; exit 1; } 

	make -j $NUM_PROCS || { echo "libtiff build failed"; exit 1; } 
	
	make install DESTDIR="$BASE"|| { echo "libtiff install failed"; exit 1; } 
	
	#DLL needs to be copied into lib manually
	cp -p .libs/${NAME}-[0-9]*.dll $BASE/lib/ 

	popd >/dev/null
	popd >/dev/null
	
	FIX_LA_FILE_ARG=libtiff
	fix_la_file
	echo ${NAME} >> $BUILD_STATUS_FILE
}


function build_qhull2015()
{
	NAME="libqhull_2015"
	ISBUILT_ARG=${NAME}
	isBuilt
	if [ $ISBUILT -eq 1 ] ; then
		return;
	fi
	
	echo Building $NAME
	pushd deps >/dev/null
	pushd qhull-* >/dev/null
	
	if [ $? -ne 0 ] ; then
		echo "qhull dir missing, or duplicated?"
		exit 1
	fi

	createHgRepoAsNeeded

	#delete the qhull directories that do NOT correspond to the version of qhull we want to build
	rm -rf src/libqhull src/libqhullcpp src/libqhullstatic*

#	APPLY_PATCH_ARG="$PATCHES_QHULL"
#	applyPatches
	make clean

	cp ../../patches/qhull2015-cmakefile-replacement CMakeLists.txt

	# TODO: Technically, deleting CMakeCache doesn't solve all caching problems. Cached data can remain elsewhere
	# This seems to stem from cmakes stubborn approach of "we only allow out-of-source-tree builds".
	# This is annoying, as in-tree building is very common, and natural for small projects. It also allows incremental building.
	#  IMHO, once I manually alter a cmakefile, the cache should fucking well work out that it is out-of-date, as I keep getting nasty suprises.
	# https://cmake.org/Bug/view.php?id=14820 .
	rm -f CMakeCache.txt
	cmake -DCMAKE_INSTALL_PREFIX="$BASE" -DCMAKE_TOOLCHAIN_FILE=../../patches/cmake-toolchain$BITS_VAL -DWITH_BOOST=0 -DWITH_FFTW=0 -DWITH_LEMON=0 


	#TODO: Better test (using c)
	#if using win64, then ensure that there is only the correct long long version
	# of the pointer
	if [ `egrep "typedef .* ptr_intT" src/libqhull_r/mem_r.h | wc -l` -ne 1 ] ; then
		echo "There appears to be multiple ptr_intT types in qhull's mem.h. Qhull normally picks the wrong one. Aborting- please fix"
		exit 1
	fi

	sed -i "s/ gcc$/${HOST_VAL}-gcc/" Makefile
	sed -i "s/ g++$/${HOST_VAL}-g++/" Makefile

	make SO="dll" -j $NUM_PROCS 
	find ./ -name \*dll -exec cp {} ${BASE}/bin/	
	make SO="dll" -j $NUM_PROCS || { echo "qhull build failed"; exit 1; } 
	make install DESTDIR="$BASE"|| { echo "qhull install failed"; exit 1; } 

	popd >/dev/null
	popd >/dev/null

	ln -s ${BASE}/include/libqhull ${BASE}/include/qhull



	FIX_LA_FILE_ARG=libqhull
	fix_la_file
	echo ${NAME} >> $BUILD_STATUS_FILE
}

#FIXME: This does not work. Qhull uses a strange combination of cmake
# and hand makefiles, so propagating correct cross-compiling 
# parameters is quite tricky
function build_qhull2015b()
{
	NAME="libqhull"
	ISBUILT_ARG=${NAME}
	isBuilt
	if [ $ISBUILT -eq 1 ] ; then
		return;
	fi
	
	echo Building $NAME
	pushd deps >/dev/null
	pushd qhull-* >/dev/null
	
	if [ $? -ne 0 ] ; then
		echo "qhull dir missing, or duplicated?"
		exit 1
	fi
	
	createHgRepoAsNeeded

	rm -f CMakeCache.txt
	cmake -DCMAKE_INSTALL_PREFIX="$BASE" -DCMAKE_TOOLCHAIN_FILE=../../patches/cmake-toolchain$BITS_VAL 

	make -j $NUM_PROCS || exit 1

	make install	

	popd >/dev/null
	popd >/dev/null

	echo ${NAME} >> $BUILD_STATUS_FILE
}

function build_expat()
{
	NAME="libexpat"
	ISBUILT_ARG=${NAME}
	isBuilt
	if [ $ISBUILT -eq 1 ] ; then
		return;
	fi
	echo Building $NAME
	pushd deps >/dev/null
	pushd expat-** >/dev/null
	
	if [ $? -ne 0 ] ; then
		echo "expat dir missing, or duplicated?"
		exit 1
	fi

	createHgRepoAsNeeded

	./configure --host=$HOST_VAL --enable-shared --disable-static --prefix=/ || { echo "$NAME configure failed"; exit 1; } 

	make -j $NUM_PROCS || { echo "$NAME build failed"; exit 1; } 
	
	make install DESTDIR="$BASE"|| { echo "$NAME install failed"; exit 1; } 

	#DLL needs to be copied into lib manually
	cp -p .libs/${NAME}-[0-9]*.dll $BASE/lib/ 

	popd >/dev/null
	popd >/dev/null



	FIX_LA_FILE_ARG=libexpat
	fix_la_file
	echo ${NAME} >> $BUILD_STATUS_FILE
}

function build_gsl()
{
	NAME="libgsl"
	ISBUILT_ARG=${NAME}
	isBuilt
	if [ $ISBUILT -eq 1 ] ; then
		return;
	fi
	
	echo Building $NAME
	pushd deps >/dev/null
	pushd gsl-* >/dev/null
		
	if [ $? -ne 0 ] ; then
		echo "gsl dir missing, or duplicated?"
		exit 1
	fi

	createHgRepoAsNeeded

	make clean

	mkdir -p doc
	echo "all:" > doc/Makefile.in 
	echo "install:" >> doc/Makefile.in
	echo "clean:" >> doc/Makefile.in

	APPLY_PATCH_ARG=$PATCHES_GSL
	applyPatches


	./configure --host=$HOST_VAL --enable-shared --disable-static --prefix=/ || { echo "gsl configure failed"; exit 1; } 

	make -j $NUM_PROCS || { echo "gsl build failed"; exit 1; } 
	
	make install DESTDIR="$BASE"|| { echo "gsl install failed"; exit 1; } 

	popd >/dev/null
	popd >/dev/null


	FIX_LA_FILE_ARG=libgsl
	fix_la_file
	echo ${NAME} >> $BUILD_STATUS_FILE
}


function build_libiconv()
{
	NAME="iconv"
	ISBUILT_ARG=${NAME}
	isBuilt
	if [ $ISBUILT -eq 1 ] ; then
		return;
	fi

	echo Building $NAME
	pushd deps >/dev/null
	pushd libiconv-* >/dev/null
		
	if [ $? -ne 0 ] ; then
		echo "libiconv dir missing, or duplicated?"
		exit 1
	fi

	APPLY_PATCH_ARG=$PATCHES_ICONV
	applyPatches
	
	createHgRepoAsNeeded

	make clean


	./configure --host=$HOST_VAL --enable-shared --disable-static --prefix=/ || { echo "libiconv configure failed"; exit 1; } 

	make -j $NUM_PROCS || { echo "libiconv build failed"; exit 1; } 
	
	make install DESTDIR="$BASE"|| { echo "libiconv install failed"; exit 1; } 

	popd >/dev/null
	popd >/dev/null
	
	FIX_LA_FILE_ARG=libiconv
	fix_la_file
	echo ${NAME} >> $BUILD_STATUS_FILE
}

function build_gettext()
{
	NAME="gettext"
	ISBUILT_ARG=${NAME}
	isBuilt
	if [ $ISBUILT -eq 1 ] ; then
		return;
	fi
	
	echo Building $NAME
	pushd deps >/dev/null
	pushd gettext-* >/dev/null
		
	if [ $? -ne 0 ] ; then
		echo "$NAME dir missing, or duplicated?"
		exit 1
	fi

	make clean

	APPLY_PATCH_ARG=$PATCHES_GETTEXT
	applyPatches
	automake
	
	createHgRepoAsNeeded

	#The CFlags/cxxflags thing is due to a really old gettext bug
	# (who is maintaining that these days? the fix is known for years!) 
	# http://savannah.gnu.org/bugs/?36443
	CFLAGS="$CFLAGS -O2" CXXFLAGS="$CXXFLAGS -O2" ./configure --host=$HOST_VAL --disable-threads --enable-shared --disable-static --prefix=/ || { echo "$NAME configure failed"; exit 1; } 

	make -j $NUM_PROCS || { echo "$NAME build failed"; exit 1; } 
	
	make install DESTDIR="$BASE"|| { echo "$NAME install failed"; exit 1; } 

	#FIXME: I had to copy the .lib, .la and .a files manually
	# I don't know why the makefile does not do this.
	CPSUCCESS=0
	for i in `ls gettext-runtime/intl/.libs/libintl*.{la,lib,a,dll}`
	do
		cp $i ${BASE}/lib/ 
		if [ $? -eq 0 ] ; then
			CPSUCCESS=1;
		fi
	done
	if [ $CPSUCCESS -eq 0 ] ; then	
		{  echo "semi-manual copy of libintl failed"; exit 1; } 
	fi
	popd >/dev/null
	popd >/dev/null

	#FIXME: libintl.h does notget installed properly

	FIX_LA_FILE_ARG=libintl
	fix_la_file
	FIX_LA_FILE_ARG=libcharset
	fix_la_file
	
	echo ${NAME} >> $BUILD_STATUS_FILE
}

function createDirLayout()
{
	NEEDED_DIRS="bin lib include deps/packages"
	for i in $NEEDED_DIRS
	do
		if [ ! -d $i ] ; then
			mkdir -p $i;
		fi
	done
}

function checkPatchesExist()
{	
	echo -n "checking patches exist..."
	if [ x"$PATCH_LIST" == x"" ] ; then
		return;
	fi

	if [ ! -d patches ] ; then
		echo "Patch directory does not exist, but patches are needed"
		echo "Need : " $PATCH_LIST
		exit 1
	fi

	for i in $PATCH_LIST
	do
		if [ ! -f "patches/$i" ]  ; then
			echo "patches/$i" does not exist
			exit 1;
		fi
	done
	echo "done"
}

#Ensure we are running under  a whitelisted host
function checkHost()
{
	echo -n "Host checks... "
	if [ x`which uname` == x"" ] ; then
		echo "Uname missing... Shouldn't be... Can't determine arch - bailing out."
		exit 1
	fi

	if [ x`uname -s` != x"Linux" ] ; then
		echo "This is only meant to run under linux.. and probably only debian-like systems."
		exit 1
	fi


	HOSTTYPE=`uname -m`

	if [ x"$HOSTTYPE" != x"x86_64" ] ; then
		echo "This only works on x86_64 builder machines - you'll need to modify the script to handle other arches. Sorry."
		exit 1
	fi

	BINARIES_NEEDED="apt-get sudo patch grep sed awk"
	
	for i in $BINARIES_NEEDED 
	do
		if [ x`which $i` == x"" ] ; then
			echo "Missing binary : $i  - aborting"
			exit 1
		fi
	done
	
	if [ x`which lsb_release` != x"" ] ; then
		#Possible results
		# Debian, LinuxMint,Ubuntu,(others)
		DIST_NAME=`lsb_release -i | awk -F: '{print $2}' | sed 's/\s//g'`
		if [ x$DIST_NAME != x"Debian" ] ; then
			echo "This is meant to target debian. I'm not sure if it will work on your system. You'll need to work that out."
			echo "Sleeping for 4 seconds."
			sleep 4
		fi
	fi
	
	echo "done"
}

#Check that we have a suitable host system
checkHost

#Check we have the patches we need
checkPatchesExist

#build the dirs we need
createDirLayout


#Install cross compiler
#---
case ${HOST_VAL}  in
	x86_64-w64-mingw32)
		if [ x"$DIST_NAME" == x"Ubuntu" ] || [ x"$DIST_NAME" == x"LinuxMint" ] ; then
			MINGW_PACKAGES="mingw-w64-dev g++-mingw-w64-x86-64"
		else
			MINGW_PACKAGES="mingw-w64-x86-64-dev g++-mingw-w64-x86-64"
		fi
		HOST_EXT="win64"
	;;
	i686-w64-mingw32)
		MINGW_PACKAGES="gcc-mingw-w64-i686 g++-mingw-w64-i686"
		HOST_EXT="win32"
	;;
	*)
		echo "Unknown host... please install deps manually,or alter script"
		exit 1	
	;;
esac

#install the compiler
install_mingw
#---

#Create the binaries we need
createBaseBinaries

#Obtain the needed dependencies
grabDeps

#set our needed environment variables
PATH=${BASE}/bin/:/usr/$HOST_VAL/bin/:$PATH
export CXX=${HOST_VAL}-g++
export CPP=${HOST_VAL}-cpp
export CC=${HOST_VAL}-gcc
export CPPFLAGS=-I${BASE}/include/
export CFLAGS=-I${BASE}/include/
export CXXFLAGS=-I${BASE}/include/
export LDFLAGS=-L${BASE}/lib/
export RANLIB=${HOST_VAL}-ranlib
export LIBS="-L${BASE}/lib/"
DESTDIR=${BASE}

build_libxml2
build_gsl
build_qhull2015

echo "BUILDING LIBATOMPROBE"
sleep 1

build_libatomprobe

