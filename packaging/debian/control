Source: libatomprobe
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: D Haley <mycae@gmx.com>
Section: science
Priority: optional
Standards-Version: 4.1.0
Vcs-Browser: https://salsa.debian.org/science-team/libatomprobe
Vcs-Git: https://salsa.debian.org/science-team/libatomprobe.git
Homepage: https://apttools.sourceforge.net/index.html

Package: libatomprobe0
Architecture: any
Build-Depends: debhelper (>= 11),
               dpkg-dev (>= 1.16.1~),
               libgl1-mesa-dev | libgl-dev,
               libpng-dev | libpng15-dev,
               libqhull-dev,
               libgsl-dev,
               libxml2-dev,
	       python3-dev,
	       python3,
	       libpython3-dev,
	       dh-python,
	       cmake
Depends: ${shlibs:Depends},
         ${misc:Depends},
	 ${python3:Depends}
Description: Library for processing Atom Probe Tomography (APT) data
 This provides a C++ library for the scientific analysis
 of real valued point data (x,y,z,value). This is primarily targeted
 towards Atom probe tomography applications, but may prove useful to
 other applications as well.
 .
 The package includes both its own C++ and SWIG based python interfaces

Package: libatomprobe-dev
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
	 ${python3:Depends}
Description: Development files for libatomprobe
 This package provides development files needed to write programs that
 use libatomprobe
