%module atomprobe

%include cstring.i
%include cpointer.i
%include typemaps.i
%include carrays.i

%apply unsigned int *INPUT {unsigned int *p};
%apply float &OUTPUT { float &lBound, float& uBound };
%apply float *OUTPUT { float &distance};
%typemap(memberin) const char *file = char *;


%{
#   define SWIG_PYTHON_EXTRA_NATIVE_CONTAINERS 
#include <vector>
#include "primitives/point3D.h"
#include "primitives/boundcube.h"
#include "primitives/ionHit.h"
#include "primitives/mesh.h"


#include "io/dataFiles.h"


#include "helper/misc.h"
#include "helper/aptAssert.h"
#include "helper/voxels.h"

#include "io/ranges.h"
#include "io/multiRange.h"

#include "algorithm/K3DTree-approx.h"
#include "algorithm/K3DTree-exact.h"
#include "algorithm/axialdf.h" //Needs K3DTree-exact.h
#include "algorithm/filter.h"
#include "algorithm/histogram.h"
#include "algorithm/massTool.h"
#include "algorithm/rangeCheck.h"
#include "algorithm/rotations.h"
#include "algorithm/isoSurface.h"

#include "reconstruction/reconstruction-simple.h"

#include "deconvolution/deconvolution.h"

#include "helper/progress.h"
#include "helper/composition.h"

#include "helper/maths/lfsr.h"
#include "helper/maths/quat.h"
#include "helper/maths/misc.h"

#include "isotopes/abundance.h"
#include "isotopes/overlaps.h"
#include "isotopes/massconvert.h"

#include "projection/projection.h"


#include "statistics/confidence.h"

%}

%inline %{
bool dummyKdCallback()
{
        return true;        
}
void forceKDInit(AtomProbe::K3DTreeExact &k) {
      k.setCallback(dummyKdCallback) ;
      }

%}
%include std_vector.i
%include std_pair.i
%include std_string.i
%include std_map.i
%include std_set.i


// Instantiate templates 
//=============
namespace std {
           %template(IntVector) vector<int>;
           %template(UIntVector) vector<unsigned int>;
           %template(SizetVector) vector<size_t>;
           %template(DoubleVector) vector<double>;
           %template(FloatVector) vector<float>;
           %template(BoolVector) vector<bool>;
           %template(StringVector) vector<std::string>;

           %template(EPOS_ENTRYVector) vector<AtomProbe::EPOS_ENTRY>;
           %template(IonHitVector) vector<AtomProbe::IonHit>;
           %template(Point3DVector) vector<AtomProbe::Point3D>;
           
           %template(ATOVector) vector<AtomProbe::ATO_ENTRY>;
           
           %template(SizetSizetPair) std::pair<size_t,size_t>;
           %template(FloatFloatPair) std::pair<float,float>;
          
           %template(FloatFloatVector) std::vector<std::pair<float,float> >;
           %template(SizetSizetVector) std::vector<std::pair<size_t,size_t> >;
           %template(UIntFloatPairVector) std::vector<std::pair<unsigned int,float> >;
           %template(UIntDoubleMap) std::map<unsigned int, double> ;

           %template(FloatVectorVector) std::vector< std::vector<float> >;
              
                
           %template(WeightVector) std::vector<AtomProbe::Weight > ;
           %template(WeightVectorVector) std::vector<std::vector<AtomProbe::Weight> > ;

           %template(MoleculeSet) std::set<AtomProbe::SIMPLE_SPECIES>;
           %template(MoleculeSetVector) std::vector<std::set<AtomProbe::SIMPLE_SPECIES> >;

           %template (StringSizetVector) std::vector<std::pair<std::string,size_t> >;
           %template (StringStringVector) std::vector<std::pair<std::string,std::string> >;


        %apply std::vector<std::pair<unsigned int,float> > &OUTPUT { std::vector<std::pair<unsigned int,float> > &decomposedHits};

        %template(ISOTOPE_ENTRYVector) std::vector<AtomProbe::ISOTOPE_ENTRY> ;
           %template(ISOTOPE_ENTRYVectorVector) std::vector<std::vector<AtomProbe::ISOTOPE_ENTRY> >;

}
        
//=============


//Map complex input types to python friendly things
//==========

%typemap(in,numinputs=0) unsigned int *res(unsigned int& p)
{
        $1=p;
}

%typemap(in,numinputs=0) (float& theta, float &phi)
{
        $1 = new float;
        $2 = new float;
}

%typemap(argout) (float& theta, float &phi) {
          %append_output(PyFloat_FromDouble(*$1));
          %append_output(PyFloat_FromDouble(*$2));
}


//Required for K3DTree
%typemap(in,numinputs=0) (std::vector<Point3D> &pts, bool clear)
{
        $1=pts;
        $2=clear;
}

%typemap(in,numinputs=0) (std::vector<IonHit> &pts, bool clear)
{
        $1=pts;
        $2=clear;
}
//==========
%cstring_bounded_output(char* symbol, 1024);


//Access functions
//=================
//Extra [] operator for IonHit
namespace AtomProbe{
%extend IonHit{
    const float __getitem__(unsigned int i) {
                        assert(i<4);
                        if(i<3)
                            return $self->getPos()[i];
                        else
                            return $self->getMassToCharge();
                         }
}
}

namespace AtomProbe{
%extend Point3DVector{
    void setVal(unsigned int j, Point3D i) {
            assert(j < $self->len);
            *($self)[j]=i;
                         }
}
}
//Extra [] operator for Point3D
namespace AtomProbe{
%extend Point3D {
    const float __getitem__(unsigned int i) {
                        assert(i<3);
                    return $self->getValue(i);
                         }
}
}

namespace AtomProbe{
// set exception handling for __setitem__
%extend TRIANGLE{
  const float __getitem__(unsigned int i){
          assert(i<3);
                  return $self->p[i];
  }
}
}

namespace AtomProbe{
%extend TRIANGLE{
  void __setitem__(unsigned int i, float j){
          assert(i<3);
          $self->p[i]=j;
  }
}

}

namespace AtomProbe{
template<class T1, class T2>
void assignVec(vector<T1>  &a, unsigned int i,T2 b)
{
        a[i]=b;
}
}

//=================


//SWIG Definitions : Note order can be important
//===

%include "primitives/point3D.h"
%include "primitives/boundcube.h"
%include "primitives/ionHit.h"
%include "primitives/mesh.h"

%include "helper/misc.h"
%include "helper/aptAssert.h"
%include "helper/voxels.h"

%include "io/ranges.h"
%include "io/dataFiles.h"
%include "io/multiRange.h"

%include "algorithm/K3DTree-approx.h"
%include "algorithm/K3DTree-exact.h"
%include "algorithm/axialdf.h" // Needs K3DTree-exact.h
//%include "algorithm/convexHull.h" //Has a callback func
%include "algorithm/filter.h"
%include "algorithm/histogram.h"
%include "algorithm/massTool.h"
%include "algorithm/rangeCheck.h"
%include "algorithm/rotations.h"
%include "algorithm/isoSurface.h"

%include "reconstruction/reconstruction-simple.h"

%include "deconvolution/deconvolution.h"

%include "isotopes/abundance.h"
%include "isotopes/overlaps.h"
%include "isotopes/massconvert.h"

%include "helper/progress.h"
%include "helper/composition.h"
%include "helper/maths/lfsr.h"
%include "helper/maths/quat.h"
%include "helper/maths/misc.h"

%include "algorithm/rangeCheck.h"

%include "projection/projection.h"

%include "statistics/confidence.h"
//===


%template(VoxelsFloat) AtomProbe::Voxels<float>;
%template(VoxelsUInt) AtomProbe::Voxels<unsigned int>;
%template(TriangleWithVertexNormVector) std::vector<AtomProbe::TriangleWithVertexNorm>;
%template(TRIANGLEVector) std::vector<AtomProbe::TRIANGLE>;
%array_class(unsigned int, uintArray);
