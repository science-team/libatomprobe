#include "atomprobe/atomprobe.h"

using namespace std;
using namespace AtomProbe;

#include <fstream>
#include <set>
#include <iomanip>
#include <algorithm>

//This code demonstrates creating a "DOT" [1] graph, consisting of a plot of all the
// overlaps present in a rangefile.
//
// The graph's edges and its output show which species and masses overlap
// Note that there are lots and lots of output edges at the moment,
// so you may need to use the strict keyword in the output dot file (text file)
// These plots were initially developed by London, 2016.
//
// [1] https://en.wikipedia.org/wiki/DOT_language

int main(int argc, char *argv[])
{
#ifdef HAVE_LIBXML2

	//Check input arguments
	if(argc < 2)
	{
		cerr << "USAGE : " << argv[0] << " (Rangefile| Ion list)" << endl;
		return 1;
	}

	RangeFile rng;
	if(argc == 2)
	{
		//Load the rangefile
		//---
		if(!rng.open(argv[1]))
		{
			cerr << "Failed to open rangefile :" << argv[1] << rng.getErrString() << endl;
			return 2;
		}
		cerr << "Rangefile has :" << rng.getNumIons() << " ranges, and " << rng.getNumIons() << " ions." << endl;
		//---
	}
	else
	{
		set<string> ionNames;
		//Fake a rangefile using the specified ions
		RGBf rgbf;
		for(unsigned int ui=1;ui<argc;ui++)
			ionNames.insert(string(argv[ui]));

		for(const auto & ionName : ionNames)
		{
			string s;
			s = ionName;

			rng.addIon(s.c_str(),s.c_str(),rgbf);
		}
	}

	//Open the table of masses of elements
	//---
	AbundanceData natTable;
	if(natTable.open("naturalAbundance.xml"))
	{
		cerr << "failed to open natural abundance file" << endl;
		return 3;
	}
	//---


	//Find the overlaps
	//---
	//Separation between overlap items
	const float MASS_DELTA=0.05;
	//Maximum charge to consider when evaluating overlaps
	const unsigned int MAX_CHARGE=3;
	vector<pair<size_t,size_t> > overlapIdx;
	vector<pair<float,float> > overlapMass;
	findOverlaps(natTable,rng,MASS_DELTA,MAX_CHARGE,overlapIdx,overlapMass);

	cerr << "Found :" << overlapIdx.size() << " overlaps, using max charge=" << MAX_CHARGE << " and mass delta=" << MASS_DELTA << endl;
	//---

	//Save the result into a DOT formatted file
	//---
	ofstream f("dotFile.txt");
	if(!f)
	{
		cerr << "Unable to open output file." <<endl;
		return 4;
	}

	// Get a list of all the nodes in the graph
	vector<pair<string, unsigned int> > nodeList;
	vector<string> nodeNames;
	for(unsigned int ui=0; ui<overlapIdx.size(); ui++)
	{
		// first entry of overlapIdx
		unsigned int rawId,id,charge;
		rawId =overlapIdx[ui].first;
		id = rawId/MAX_CHARGE;
		charge=rawId%MAX_CHARGE +1;
		nodeList.push_back( make_pair(rng.getName(id), charge) );

		// make a node label string for it
		// TODO: Add <SUB></SUB> around numbers for subscripts
		// TODO: replace only "1+" with "+", or replace "2+" with "++" etc.
		std::stringstream buffer;
		buffer << "\"" << rng.getName(id) << "|" << charge << "+\" ";
		buffer << " [ label = <" << rng.getName(id) << "<SUP>" << charge << "+</SUP>> ];" << endl;
		nodeNames.push_back(buffer.str());

		//second entry of overlapIdx
		rawId =overlapIdx[ui].second;
		id = rawId/MAX_CHARGE;
		charge=rawId%MAX_CHARGE+1;
		nodeList.push_back( make_pair(rng.getName(id), charge) );

		// make a node label string for 2nd node
		buffer.str(""); // clear buffer
		buffer << "\"" << rng.getName(id) << "|" << charge << "+\" ";
		buffer << " [ label = <" << rng.getName(id) << "<SUP>" << charge << "+</SUP>> ];" << endl;
		nodeNames.push_back(buffer.str());
	}

	// TODO: When two nodes share multiple edges (peaks), display them
	// as one edge with "peak1 \n peak2 \n peak3 etc." rather than
	// multiple edges.

	// start graph
	f << "strict graph overlap_map {" << endl;

	// Only print unique labels to cut down file size
	// Find unique strings using a set
	set <std::string> uniqueStrings;
	for (auto & nodeName : nodeNames)
	{
		uniqueStrings.insert(nodeName);
	}
	// Print the unique labels
	for (const auto & uniqueString : uniqueStrings)
	{
		f << uniqueString;
	}

	for(unsigned int ui=0; ui<overlapIdx.size(); ui++)
	{
		//Create graph edge
		f << "\"" << nodeList[ui*2].first << "|" << nodeList[ui*2].second << "+\" -- ";

		f << "\"" << nodeList[ui*2+1].first << "|" << nodeList[ui*2+1].second << "+\"";

		// average mass to label the edge with
		float avgMass = (overlapMass[ui].first + overlapMass[ui].second)/2.0;
		f << " [ label = \"" << std::fixed << std::setprecision( 1 ) << avgMass << "\" ];" << endl;
	}
	f << "}" << endl;
	//---

	cerr << "Wrote overlaps to dotFile.txt" << endl;
	return 0;
#else
	cerr << "libatomprobe built without XML support  : refusing to continue" << endl;
#endif
}
