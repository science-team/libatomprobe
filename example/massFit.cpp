/* massFit.cpp :  Example showing how to perform mass knapsack search
 * Copyright (C) 2017  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <atomprobe/atomprobe.h>


#include <iostream>
#include <cstdlib>
#include <vector>
#include <fstream>


using std::cout;
using std::endl;
using std::vector;
using std::string;

using namespace AtomProbe;

//Display human-readable output
void printSolution(const vector<Weight> &solnComponents, 
				const vector<string> &labels)
{
	double cumulativeMass = 0.0f;
	for(unsigned int ui=0;ui<solnComponents.size();ui++)
	{
		Weight w;
		w = solnComponents[ui];
		cout << labels[w.uniqueId];
		if(ui != solnComponents.size()-1)
			cout << ":";

		cumulativeMass +=w.mass;
	}
	cout << "\t" << cumulativeMass <<endl;

}

int main(int argc, const char *argv[])
{
	vector<string> names;
	names.push_back("A"); // weight "A", mass 1
	names.push_back("B"); // Weight "B", mass 2
	names.push_back("C"); // Weight "C", mass 4

	vector<Weight> inputWeights;

	//Identifier (2nd argument) is optional
	inputWeights.push_back(Weight(1.005f,0));
	inputWeights.push_back(Weight(2.003f,1));
	inputWeights.push_back(Weight(4.003f,2));

	//Explain problem to user
	const float TARGET=3.001f;
	const float TOLERANCE=0.05f;
	const unsigned int MAX_COMBINE=3;

	cout << "Target weight is :" << TARGET << " with tolerance :" << TOLERANCE << endl;
	cout << "Searching up to :" << MAX_COMBINE << " allowed items per solution" << endl;
	cout << endl;
	cout << "Input masses:" << endl;
	cout << "-------------" << endl;
	for(auto ui=0;ui<inputWeights.size();ui++)
	{
		cout << names[ui] << " -> mass is :  " << inputWeights[ui].mass << endl;
	}
	cout << "-------------" << endl;

	cout << endl;
	
	//Provide brute-force list of weights
	// note that the brute-forcer may drop weights from calculation if it thinks they are not needed
	vector<vector<Weight> > solutions;
	MassTool::bruteKnapsack(inputWeights,TARGET,TOLERANCE,MAX_COMBINE,solutions);


	//Loop over all solutions
	for(auto & solution : solutions)
		printSolution(solution,names);

}
