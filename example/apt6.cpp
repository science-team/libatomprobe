/* countEpos.cpp : Sample program to count entries in an epos file
 * Copyright (C) 2015  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "atomprobe/atomprobe.h"

using namespace std;
using namespace AtomProbe;

int main( int argc, char *argv[])
{
#ifdef EXPERIMENTAL
	std::string filename,posName;
	if(argc ==1)
	{
		cout << "Enter input file name" << endl;

		cin >> filename;
		cout << "Enter output pos file name" << endl;

		cin >> posName;
	}
	else if(argc ==3)
	{
		filename=argv[1];
		posName=argv[2];
	}
	else
	{
		cerr<< "USAGE: " << argv[0] << " [FILENAME POSFILEOUT]" << endl;
		return 1;
	}

	vector<IonHit> ionHits;
	size_t errCode;
	if(errCode=loadAPTSuite6Format(filename.c_str(),ionHits))
	{
		cerr << "failed to load file" << errCode << endl;
		cerr << " i.e. " << getAPT6ErrorString(errCode) << endl;
	}
	else
	{
		cerr << "File loaded successfully" << endl;
	}

	cerr <<"Loaded :" << ionHits.size() << " Ions" << endl;

	//Save to pos
	savePosFile(ionHits,posName.c_str());
#else
        cerr << "Experimental features must be enabled at build time. Example not built" << endl;
#endif

}
