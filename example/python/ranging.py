#!/usr/bin/python3
import sys
import atomprobe

#Optional - allow continuing past errors, rather than terminating
# only recommended for debuggin
atomprobe.setHardAssert(False)

range_file = atomprobe.RangeFile()

filename = input("Enter range file to load:")

result=range_file.open(filename)

if result == False:
	print("Failed to load rangefile\n")
	print ("Reason :" + range_file.getErrString())
	sys.exit(1)

print(f"Loaded a rangefile with {range_file.getNumIons()} Ions, and  {range_file.getNumRanges()} ranges")

print("Ions")
print("====")
for i in range(range_file.getNumIons()):
	print(range_file.getName(i))
print("====")


print("Ranges")
print("====")
for i in range(range_file.getNumRanges()):
	#Get the range name - ranges have an ionID that identifies the owning parent ion
	parentName = range_file.getName(range_file.getIonID(i))
	print( str(range_file.getRange(i)) + "\t" + parentName)
print("====")

#Create a multi-range
ad=atomprobe.AbundanceData()
if ad.open("../../data/naturalAbundance.xml"):
	print("Failed to open natural abundance file")
	sys.exit(1)

multi_range=atomprobe.MultiRange(range_file,ad)

print("Created a multi-range with {} ions and {} ranges".format(multi_range.getNumIons() , multi_range.getNumRanges()))

if(multi_range.isSelfConsistent()):
	print("Multi range is self-consistent")
	multi_range.write("test_multi_out.mrng")
	print("Wrote out multi-range test file, to test_multi_out.mrng")
else:
	print("Multi-range is not right...")


