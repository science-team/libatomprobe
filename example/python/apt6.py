#!/usr/bin/python3
import atomprobe
import sys 

#For this example, we won't do error checking
# as it is not the purpose of the example
filename = input("Enter apt6 file to load:")

v=atomprobe.IonHitVector()
errCode = loadAPTSuite6Format(filename,v)

if errCode :
    print(f"failed to load file {errCode}")
    print("i.e." + str(getAPT6ErrorString(errCode)) )
    sys.exit(1)
else:
    print("Loaded " + str(len(v)) + " Ions ")


filename = input("Enter pos file to output:")
savePosFile(v,filename);

