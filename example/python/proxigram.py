#!/usr/bin/python3
import atomprobe
import numpy
import random

ions = atomprobe.IonHitVector()
#filename = input("Enter POS file to load:")
filename='/home/pcuser/Desktop/test-ellipsoid.pos'
result=atomprobe.loadPosFile(ions,filename)



#voxelise the data
vox=atomprobe.VoxelsFloat()
vox.resize(50,50,50)

#Obtain the bounding cube, and set the voxel grid bounds
bc=atomprobe.BoundCube()
bc.setBounds(ions)
vox.setBounds(bc)

#Count the number of ions
vox.countIons(ions)

#obtain min/max for dataset
maxVox=vox.max()
minVox=vox.min()

print(f"Min/max voxel data ({minVox},{maxVox})")

triVec=atomprobe.TriangleWithVertexNormVector()

isoValue=1.0
atomprobe.marchingCubes(vox,isoValue,triVec)

nTris=triVec.size()
print(f"Generated : {nTris} triangles as isosurface")

#Generate a mesh object
mesh=atomprobe.Mesh()
mesh.nodes.resize(nTris*3)
mesh.triangles.resize(nTris)

vnodes=atomprobe.Point3DVector()
vnodes.resize(nTris*3)

for i in range(nTris):
	for j in range(3):
		#copy each triangle vertex
		mesh.nodes[3*i+j] = triVec[i].getPoint(j)
		mesh.triangles[i].setP(j,3*i+j)

#small tolerance value
eps = numpy.finfo(float).eps

#Fuse into a single mesh object with shared vertices
#This is not strictly needed, but we do it anyway
# it is however, slow.
#print('Merging vertices....')
#mesh.mergeDuplicateVertices(eps**0.5)
#print('Done')


#Note you cannot use signed distance alone, due to
# incorrect sign flipping. The atom probe paper on proxigrams is wrong

NUM_PTS=1000

#Generate a random cube of points
# across data bounds
randomPts = atomprobe.Point3DVector()
randomPts.resize(NUM_PTS)
minP=bc.min()
maxP=bc.max()
tmpP=atomprobe.Point3D()
for i in range(NUM_PTS):
	for j in range(3):
		tmpP.setValue(j,random.uniform(minP[j],maxP[j]))

	randomPts[i]=tmpP

#Check sign
print('Performing sign check...',end='')
inside=atomprobe.BoolVector()
mesh.pointsInside(randomPts,inside)
print('Done')

print('Obtaining distances..',end='')
distances=list()
dist=float()
for i in range(len(inside)):

	idx,dist=mesh.getNearestTri(randomPts[i])
	if(inside[i]):
		dist=-abs(dist)
	else:
		dist=abs(dist)

	distances.append(dist)
print('Done')


#Now we have the distances, they need to be coverted into a histogram
y,x = numpy.histogram(distances,bins='auto')

for i in range(len(y)):
	xCentre=0.5*(x[i]+x[i+1])
	print( str(xCentre) + "\t\t" + str(y[i]))

#Inside == true if inside,
