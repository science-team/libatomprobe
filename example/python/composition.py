#!/usr/bin/python3
import atomprobe

#For this example, we won't do error checking
# as it is not the purpose of the example
filename = input("Enter range file to load:")
range_file = atomprobe.RangeFile()
range_file.open(filename)
print(f"Loaded a rangefile with {range_file.getNumIons()} Ions, and  {range_file.getNumRanges()} ranges")

filename = input("Enter POS file to load:")
v = atomprobe.IonHitVector()
result=atomprobe.loadPosFile(v,filename)

range_file.range(v)

#Compute counts
nIons = len(v)

ionCounts=dict()
for ion in v:
	key=range_file.getName(ion)
	ionCounts[key] = ionCounts.get(key,0)+1

print(ionCounts)

#Add poisson error bounds
print("Poisson confidence limits, alpha=0.95")
for key,value in ionCounts.items():
	#Find the 95% confidence limits, assuming poisson error
	lBound,uBound = atomprobe.poissonConfidenceObservation(value,0.95)
	print(f" {key} [ {lBound:.1f}, {uBound:.1f}]")

