#!/usr/bin/python3
import sys
import atomprobe

v = atomprobe.IonHitVector()

filename = input("Enter POS file to load:")

result=atomprobe.loadPosFile(v,filename)

if result != 0:
	print("Failed loading pos file!\n")
	print("Reason :"+ atomprobe.getPosFileErrString(result))
	sys.exit(1)

print(f"The first ion (x,y,z,m) is ({v[0][0]},{v[0][1]},{v[0][2]},{v[0][3]})")
