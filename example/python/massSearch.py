#!/usr/bin/python3

import atomprobe
v= atomprobe.WeightVector()
w = atomprobe.Weight()

#Create an allowed weight
w.mass=0.5
w.uniqueId=1

#Add it to the list of weights we can use
v.push_back(w)

solns=atomprobe.WeightVectorVector()


atomprobe.MassTool_bruteKnapsack(v,1.0,0.05,3,solns)

print('Input trim size')
print(len(v))

s_len=len(solns)
print(f'There are {s_len} solutions')


for i in range(s_len):
	print('== Solution ==')
	print(f'Solution of size: {len(solns[i])}')
	#Caution : python may attempt to garbage collect solns[] if you
	# don't explicitly create this object.
	sThis=solns[i]
	for j in range(len(sThis)):
		print(f'i:{i} j:{j}')
		print(sThis[j].mass)

	print('==============')
	print('')
