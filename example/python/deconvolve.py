#!/usr/bin/python3
import sys
import atomprobe

mrng = atomprobe.MultiRange()

#Add Fe as an ion
rgbF= atomprobe.RGBf()
rgbF.green=rgbF.red=rgbF.blue=0

ss=atomprobe.SIMPLE_SPECIES()
ss.atomicNumber=26
ss.count=1
ionIdFe=mrng.addIon(ss,"Fe",rgbF)

#Add Ni as an ion
ss.atomicNumber=28
ss.count=1
ionIdNi=mrng.addIon(ss,"Ni",rgbF)

#Open abundance data
natTable=atomprobe.AbundanceData()
if natTable.open("../../data/naturalAbundance.xml"):
	print("error opening natural abundance table")
	sys.exit(1)

#get the natural masses and abundances of iron
ironIdx = natTable.symbolIndex("Fe")[0]
nickelIdx = natTable.symbolIndex("Ni")[0]

entriesFe = natTable.isotopes(int(ironIdx))
entriesNi = natTable.isotopes(int(nickelIdx))


#Add ranges
accum=0.0
TOL=0.1

TOTALFE=1000
TOTALNI=500

countsNi=countsFe=[]
rangeGroupings=atomprobe.UIntVector()
for entry in entriesFe:
	mrng.addRange(entry.mass-TOL, entry.mass+TOL,ionIdFe)
	rangeGroupings.push_back(0)
	countsFe.append(TOTALFE*entry.abundance)

for entry in entriesNi:
	mrng.addRange(entry.mass-TOL, entry.mass+TOL,ionIdNi)
	rangeGroupings.push_back(1)
	countsNi.append(TOTALNI*entry.abundance)


#This tells the MRNG which ranges are linked
# this is used as e.g. Fe2+ and Fe+ both are Fe, but
# have different intensities in the mass spectrum
#	here it is a bit trivial
mrng.setRangeGroups(rangeGroupings)

#Here the deconvolve interface could be better.
# it currently takes in ions, but could be set to take
# either a list of masses, or counts
ionVec=atomprobe.IonHitVector()
ionhit = atomprobe.IonHit(0,0,0,0)

#Make some hit data with correct abundances
#Fe
print('Input Fe counts')
print('----')
for i in range(len(entriesFe)):
	print( str(entriesFe[i].mass) + "|" + str(int(countsFe[i])) )
	ionhit.setMassToCharge(entriesFe[i].mass)
	for j in range(int(countsFe[i])):
		ionVec.push_back(ionhit)
print('----')

#Ni
for i in range(len(entriesNi)):
	print( str(entriesNi[i].mass) + "|" + str(int(countsNi[i])) )
	ionhit.setMassToCharge(entriesNi[i].mass)
	for j in range(int(countsNi[i])):
		ionVec.push_back(ionhit)

#Problem is now set up, lets solve!
res,results= atomprobe.leastSquaresOverlapSolve(mrng,natTable,ionVec,None)
if not res:
	print('Solver failed!')
	sys.exit(1)


print('Deconvolve Complete')
for i in results:
	print( str(i[0]) + " | " +  str(i[1]))


