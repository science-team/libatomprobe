#!/usr/bin/python3
import atomprobe

#Compute the rotation matrix between two vector sets using
# the triad algorithm


#Original input vectors, which are orthogonal
v1=atomprobe.Point3D(0,0,1)
v2=atomprobe.Point3D(0,1,0)

#Rotated vectors, which are orthogonal
rv1=atomprobe.Point3D(1,0,0)
rv2=atomprobe.Point3D(0,0,1)

#normalise
v1.normalise()
v2.normalise()
rv1.normalise()
rv2.normalise()

#pre-declare result container
matrix=atomprobe.FloatVectorVector()

#Get rotation matrix
atomprobe.computeRotationMatrix(v1,v2,rv1,rv2,matrix)

#Print output
for i in range(3):
	for j in range(3):
		print(matrix[i][j],end=" ")
	print("")


