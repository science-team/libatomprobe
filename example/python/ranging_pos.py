#!/usr/bin/python3
import sys
import atomprobe

#Load Pos file
filename = input("Enter POS file to load:")
v = atomprobe.IonHitVector()
result=atomprobe.loadPosFile(v,filename)
if result != 0:
	print("Failed loading pos file!\n")
	print("Reason :"+ atomprobe.getPosFileErrString(result))
	sys.exit(1)

#Load Range file
range_file = atomprobe.RangeFile()
filename = input("Enter range file to load:")
result=range_file.open(filename)
if result == False:
	print("Failed to load rangefile\n")
	print ("Reason :" + range_file.getErrString())
	sys.exit(1)

print("Before ranging there are :" + str(len(v)) + "Ions")
#Range data from pos file  (in-place)
range_file.range(v)
print("After ranging there are :" + str(len(v)) + "Ions")

