#!/usr/bin/python3
import atomprobe


ions = atomprobe.IonHitVector()
#filename = input("Enter POS file to load:")
filename='/home/pcuser/Desktop/test-ellipsoid.pos'
result=atomprobe.loadPosFile(ions,filename)



#voxelise the data
vox=atomprobe.VoxelsFloat()
vox.resize(50,50,50)

#Obtain the bounding cube, and set the voxel grid bounds
bc=atomprobe.BoundCube()
bc.setBounds(ions)
vox.setBounds(bc)

#Count the number of ions
vox.countIons(ions)

#obtain min/max for dataset
maxVox=vox.max()
minVox=vox.min()

print(f"Min/max voxel data ({minVox},{maxVox})")

triVec=atomprobe.TriangleWithVertexNormVector()

isoValue=1.0
atomprobe.marchingCubes(vox,isoValue,triVec)

nTris=triVec.size()
print(f"Generated : {nTris} triangles as isosurface")

