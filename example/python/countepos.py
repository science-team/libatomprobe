import atomprobe
import sys
import ctypes 

filename="myfile.epos"

eV = atomprobe.EPOS_ENTRYVector()

if( atomprobe.loadEposFile(eV,filename)) :
	print(str("File load failed") + filename)
	sys.exit(1);

print(str("There are:") + str(len(eV)) + str("ions"))
singleData=[]
multiData=[]
for i in range(len(eV)):
	val=eV[i].getHitMultiplicity()
	pos=eV[i].getIonHit()
	if(val== 1):
		singleData.append(pos)
	elif(val > 1 ):
		multiData.append(pos)

print(str("Number singles:") + str(len(singleData)) )
print(str("Number multiple:") + str(len(multiData)) )

