#Demonstrate how to perform rotations
import atomprobe
import random

NPTS=10

#Generate random points
v=atomprobe.Point3DVector()
for i in range(NPTS):
	x=atomprobe.Point3D(random.random(),random.random(),random.random())
	x.normalise()
	v.push_back(x)

#Random rotation vector and angle (Radians)
rot_vec = atomprobe.Point3D(random.random(),random.random(),random.random())
rot_vec.normalise()
rot_angle=0.241 

#Obtain the rotation matrix that corresponds to the axis-angle rotation above
# (this step is not strictly needed, but we will use it to compare to our recovered matrix)
R=atomprobe.getRotationMatrix(rot_vec,angle)

#For each random point, make a rotated version
vRotated=atomprobe.Point3DVector()
for i in range(len(v)):
	rotated=atomprobe.Point3D()
	rotated=v[i]
	atomprobe.quat_rot(rotated,rot_vec,rot_angle)
	vRotated.push_back(rotated)


#Use unitary weighting for each paired set of vectors
# this is essentially the inverse error (confidence in direction)
vWeights=atomprobe.FloatVector()
vWeights.resize(NPTS,1.0)

#This is tricky - atomprobe.gsl_matrix doesn't exist, but we need to pass a named nullptr
matrix=atomprobe.gsl_matrix(0)
atomprobe.getRotationMatrixWahba(v,vRotated,vWeights,matrix)




for i in range(3):
	print("")
	for j in range(3):
		print(matrix.data[i




