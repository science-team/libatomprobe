/* countEpos.cpp : Sample program to count entries in an epos file
 * Copyright (C) 2015  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "atomprobe/atomprobe.h"

using namespace std;
using namespace AtomProbe;

int main( int argc, char *argv[])
{
	
	std::vector<EPOS_ENTRY> outData;



	std::string filename;
	if(argc ==1)
	{
		cout << "Enter file name" << endl;

		cin >> filename;
	}
	else if(argc ==2)
		filename=argv[1];
	else
	{
		cerr<< "USAGE: " << argv[0] << " [FILENAME]" << endl;
		return 1;
	}
	
	if(loadEposFile(outData, filename.c_str()))
	{
		cerr << "Epos file load failed. File is :" << filename << endl;
		return 1;
	}


	size_t numHits=0;
	for(size_t ui=0;ui<outData.size();ui++)
	{
		if(outData[ui].hitMultiplicity > 1)
			numHits++;
	}
	cerr << "There are:" << outData.size() << "Hits" << endl;

	cerr << "Number of multiples is :" << numHits << endl;


	//Work out the duration of the experiment, in pulses
	unsigned long long int ull=0;
	for(auto v : outData)
		ull+=v.deltaPulse;

	cerr << "Max pulse value :" << ull << endl;
	unsigned long long nEmptyPulse=0;
	unsigned long long nSinglePulse=0;
	unsigned long long nMultiPulse=0;

	unsigned long long nMultiIons=0;
	unsigned long long nSingleIons=0;
	for(auto v : outData)
	{
		//Compute pulse statistics
		//===
	
		if(v.deltaPulse)	
			nEmptyPulse+=v.deltaPulse -1;

		if(v.deltaPulse ==1)
		{
			nSinglePulse++;
		}

		if(v.deltaPulse >1 && v.hitMultiplicity >1)
			nMultiPulse++;
		//===

		//Compute ion statistics

		//Hits with either a "0" or ">1" in hitMultiplicity field are mutliple hits.
		// Don't need to add the zeros however.
		if(v.hitMultiplicity > 1)
			nMultiIons+=v.hitMultiplicity;

		//Single hits
		if(v.hitMultiplicity ==1)
			nSingleIons++;

	}

	unsigned long long hits=nSingleIons + nMultiIons;


	cerr << "No-hit fraction (%,pulse basis):" << nEmptyPulse/(float)ull*100 << endl;
	cerr << "Single-hit fraction (%, pulse basis):" << nSinglePulse/(float)ull*100<< endl;
	cerr << "Multi-fraction (%,pulse basis):" << nMultiPulse/(float)ull*100 << endl;

	cerr << "Single hit fraction (%, ion basis) :" << nSingleIons/(float)hits*100 << endl;
	cerr << "Mult-hit fraction (%, ion basis) :" << nMultiIons/(float)hits*100 << endl;


}
