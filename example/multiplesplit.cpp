#include "atomprobe/atomprobe.h"

using namespace std;
using namespace AtomProbe;

int main(int argc ,char *argv[])
{

	if((argc !=4 && argc !=3))
	{
		cerr << "USAGE: EPOSFILE POSFILE_OUT_SINGLE [POSFILE_OUT_MULT]" << endl;
		return 1;
	}

	bool singlesOnly = (argc ==3);

	std::vector<EPOS_ENTRY> outData;

	//Load the EPOS file
	if(loadEposFile(outData, argv[1]))
	{
		cerr << "Epos file load failed. File is :" << argv[2] << endl;
		return 1;
	}

	vector<IonHit> singleData, multipleData;

	for(auto &hit : outData)
	{
		if(hit.hitMultiplicity == 1)
			singleData.push_back(hit.getIonHit());
		else
			multipleData.push_back(hit.getIonHit());
	}	
	outData.clear();
	outData.shrink_to_fit();
	

	cerr << "Number of singles is :" << singleData.size()<< endl;
	cerr << "Number of multiples is :" << multipleData.size()<< endl;

	if(savePosFile(singleData,argv[2]))
	{
		cerr << "Error saving multiple hits to :" << argv[2] << endl;
		return 1;
	}

	if(!singlesOnly)
	{
		if(savePosFile(multipleData,argv[3]))
		{
			cerr << "Error saving single hits to :" << argv[3] << endl;
			return 1;
		}
	}

}
