/* correlationhist.cpp : Sample program to create a cross-correlation histogram
 * Copyright (C) 2015  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//This example creates an ion cross-correlation histogram
// as described by Saxey et al.
// DOI : 10.1016/j.ultramic.2010.11.021


#include <iostream>

#include "atomprobe/atomprobe.h"

using namespace std;
using namespace AtomProbe;


void dumpHistogram(std::vector<std::vector< unsigned int> > &hist, float step)
{

	//write the mass headings
	for(unsigned int ui=0;ui<hist.size();ui++)
	{
		cout << step*ui << "\t";

		for(unsigned int uj=0;uj<hist.size();uj++)
		{
			cout << hist[ui][uj] << "\t";
		}		

		cout << endl;
	}
}

int main(int argc, char *argv[]) 
{
	//Check for correct program usage
	if(argc != 3)
	{
		//print error message, and exit
		cerr << "USAGE : " << argv[0] << " EPOSFILE MASS_STEP" << endl;
		return 1;
	}

	//Get the user-specified histogram step size
	float stepSize;
	if(stream_cast(stepSize,argv[2]))
	{
		cerr << "Unable to understand stepsize :" << argv[2] << endl;
		return 1;
	}

	if(stepSize <=0)
	{
		cerr << "Stepsize must be nonzero and positive " << endl;
		return 2;
	}

	//Load the file 
	std::string filename = argv[1];
	cerr << "Processing file :" << filename << endl;

	vector<vector<unsigned int> > histogram;
	unsigned int offset=0;
	unsigned int entriesLeft;
	const float END_MASS=300;

	ProgressBar pb;
	pb.setLength(50);
	pb.init();

	while(true)
	{

		const unsigned int CHUNK_SIZE=1e4;
		vector<EPOS_ENTRY> eposIons;
		unsigned int errCode;
		if((errCode=chunkLoadEposFile(eposIons,filename.c_str(),CHUNK_SIZE,
			offset,entriesLeft)))
		{
			pb.finish();
			cerr << "Unable to load file :" << filename << endl;
			cerr << " reason :" << errCode << endl;
			return 2;
		}

		offset++;

		//Update progress
		pb.update(100.0f*(float)offset/(float)(offset+entriesLeft/CHUNK_SIZE));

		if(!entriesLeft)
			break;


		if(!accumulateCorrelationHistogram
			(eposIons,histogram,stepSize,END_MASS))
		{
			cerr << "Correlation histogram generation failed. Are ions sequenced correctly??" << endl;
			return 2;
			
		}
	}




	dumpHistogram(histogram,stepSize);

}



