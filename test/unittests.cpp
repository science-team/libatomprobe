/* unittests.cpp : runs all internal unint tests from the library
 * Copyright (C) 2014  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <cassert>

#include "atomprobe/atomprobe.h"
#include "atomprobe/apt_config.h"

#ifndef ASSERT
	#define ASSERT(f) assert( (f) );
#endif

#ifdef TEST
#undef TEST
#define TEST(f) { if(!(f))  return false; }
#endif

using namespace std;
using namespace AtomProbe;

vector<string> rangePaths()
{
	//TODO: Replace with proper system to obtain
	// the actual range path, relative to this folder
	// e.g. via a glob
	vector<string> paths;
	paths.push_back("samples/ranges/test10.rng");
	paths.push_back("samples/ranges/test11.rng");
	paths.push_back("samples/ranges/test12.rng");
	paths.push_back("samples/ranges/test1.env");
	paths.push_back("samples/ranges/test1.rng");
	paths.push_back("samples/ranges/test1.rrng");
	paths.push_back("samples/ranges/test2.rng");
	paths.push_back("samples/ranges/test2.rrng");
	paths.push_back("samples/ranges/test3.rng");
	paths.push_back("samples/ranges/test3.rrng");
	paths.push_back("samples/ranges/test4.rng");
	paths.push_back("samples/ranges/test4.rrng");
	paths.push_back("samples/ranges/test5.rng");
	paths.push_back("samples/ranges/test5.rrng");
	paths.push_back("samples/ranges/test6.rrng");
	paths.push_back("samples/ranges/test7.rng");
	paths.push_back("samples/ranges/test8.rng");
	paths.push_back("samples/ranges/test9.rng");

	return paths;
}

#ifdef DEBUG
void gsl_catch_errors(const char *reason, const char *file, int line, int gsl_errno)
{
	cerr << "Failed because :" << reason << " in file " << file << " line" << line << " error number :" << gsl_errno << endl;
	ASSERT(false);
}
#endif

bool runTests()
{
#ifdef DEBUG
	//Disable the GSL error handler.
	//It uses abort() which destroys the stack trace.
	// painful for debugging
	gsl_set_error_handler(gsl_catch_errors);

	bool haveAbundance=false;
	AbundanceData abundance;
#ifdef HAVE_LIBXML2
	if(!abundance.open("naturalAbundance.xml"))
		haveAbundance=true;
#endif

	cerr << "Testing overlap...";
	TEST(testDeconvolve());
	cerr << "\tDone" << endl;
	
	cerr << "Testing rotation...";
	TEST(testRotationAlgorithms());
	cerr << "\tDone" << endl;

//Qhull crashes under cygwin
#ifdef HAVE_QHULL
#ifndef __CYGWIN__
	cerr << "Testing Convex Hull...";
	TEST(testConvexHull());
	cerr << "\tDone" << endl;
#endif
#endif

	cerr << "Testing progress bar...";
	TEST(testProgressBar()); //No need for "Done",its built into progress bar.

	cerr << "KD tree...";
	TEST(K3DTreeExact::runUnitTests());
	TEST(K3DTreeApprox::test());
	cerr << "\tDone" << endl;

	cerr << "Mesh tests...";
	TEST(meshTests());
	cerr << "\tDone" << endl;

	cerr << "Testing sampling...";
	TEST(testSampling())
	cerr << "\tDone" << endl;

	cerr << "Isotope tests..." ;
	TEST(AbundanceData::runUnitTests())
	cerr << "\tDone" << endl;
	
	cerr << "Knapsack tests..." ;
	TEST(MassTool::runUnitTests())
	cerr << "\tDone" << endl;

	cerr << "Overlap tests..." ;
	TEST(testOverlapDetect());
	cerr << "\tDone" << endl;

	cerr << "Projection tests..." ;
	TEST(testProjection());
	cerr << "\tDone" << endl;
	
	cerr << "Miller index tests...";
	TEST(millerTest());
	cerr << "\tDone" << endl;
	
	cerr << "Reconstruction tests...";
	TEST(AtomProbe::reconstructTest());
	cerr << "\tDone" << endl;
	
	cerr << "Isotope Filter tests...";
	TEST(AtomProbe::isotopeFilterTests());
	cerr << "\tDone" << endl;

	//Allow IO tests to soft-fail
	cerr << "IO Tests...";
	if(!AtomProbe::testIO())
		cerr << "Failed IO tests - are test files in ../testdata/?" << endl;
	else
		cerr << "\tDone" << endl;


	vector<string> rangeFilePaths;
	rangeFilePaths =rangePaths();
	
	cerr << "Range tests...";
	for(auto ui=0; ui < rangeFilePaths.size();ui++)
	{
		RangeFile rng;
		TEST(rng.open(rangeFilePaths[ui].c_str()));

		TEST(rng.isSelfConsistent());
	}
	TEST(RangeFile::test());
	TEST(MultiRange::test());
        TEST(testComponentAnalysis());
	//To test the splitting code n 
	if(haveAbundance)
	{
		cerr << "(+Multi-split)...";
		TEST(MultiRange::testSplit(abundance));
	}
	cerr << "\tDone" << endl;
	
	cerr << "Range check tests...";
	TEST(AtomProbe::testRangeChecking());
	cerr << "\tDone" << endl;

	cerr << "Generation tests....";
	TEST(AtomProbe::runGenerateTests());
	cerr << "\tDone" << endl;
	
	cerr << "Mass<>mol conversion...";
	TEST(AtomProbe::testMassMolConversion());
	cerr << "\tDone" << endl;
	
	cerr << "Composition tests....";
	TEST(AtomProbe::testComposition());
	cerr << "\tDone" << endl;
	
	cerr << "Statistics tests....";
	TEST(AtomProbe::runConfidenceTests());
	cerr << "\tDone" << endl;

	cerr << "Spectrum tests....";
	TEST(AtomProbe::testBackgroundFitMaths());
	cerr << "\tDone" << endl;
	cerr << "Voxel tests....";
	TEST(AtomProbe::runVoxelTests());
	cerr << "\t\tDone" << endl;

	cerr << "Isosurface tests...";
	TEST(AtomProbe::testIsoSurface());
	cerr << "\tDone" << endl; 

	cerr << "Misc. Maths tests....";
	TEST(AtomProbe::runMiscMathsTests());
	TEST(AtomProbe::testPoint3D());
	TEST(AtomProbe::testIonHit());
	cerr << "\tDone" << endl;


#ifdef HAVE_HDF5
#ifdef EXPERIMENTAL
	cerr << "HDF tests...";
	TEST(AtomProbe::runAPTHDF5Tests());
	cerr << "\t\tDone" << endl;
#endif
#endif

	if(haveAbundance)
	{
		cerr << "Ion Adjacency tests...";
		TEST(AtomProbe::runMiscMathsTests());
		cerr << "\tDone" << endl;
	}
	else
		cerr << "Skipping Ion Adjacency ; no abundance" << endl;

#else
	cerr << "Tests not enabled in release mode. Please recompile in debug mode" << endl;
#endif
	return true;
}

int main()
{
	cerr << "Running test program " << endl;
	if(!runTests())
	{
		cerr << "unit tests failed" << endl;
		return 1;
	}

	return 0;	
}
