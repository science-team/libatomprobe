#ifndef ATOMPROBE_TEST_H
#define ATOMPROBE_TEST_H

#ifndef TEST
#define TEST(f,g) { if(!(f)) { cerr << "Test failed " << __FILE__ << ":" << __LINE__ << g << endl ; } } ;
#endif

#endif
