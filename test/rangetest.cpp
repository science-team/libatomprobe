
#include "atomprobe/atomprobe.h"

using namespace std;
using namespace AtomProbe;

#include "test.h"

int main()
{

	RangeFile rng;
	RGBf c;
	c.red=c.green=c.blue=1.0f;
	unsigned int ionId;
	ionId=rng.addIon("FeO","FeO",c);

	//add a range
	unsigned int rangeId;
	rangeId = rng.addRange(55.9+16.0,56.3+16.0,ionId);

	TEST(rng.isSelfConsistent(),"check self consistent");

	//Now, guess the charge state

#ifdef HAVE_LIBXML2

	AbundanceData massTable;
	if(massTable.open("../data/naturalAbundance.xml"))
	{
		cerr << "unable to execute test, could not find abundance data" << endl;
		return 1;
	}

	unsigned int charge;
	if(!rng.guessChargeState(rangeId,massTable,charge))
	{
		cerr << "Unable to compute charge state, thats not right" << endl;
		TEST(false,"charge guess failed");
	}

	TEST(charge ==1,"Check charge state guess");

	MultiRange m(rng,massTable);
#else
	cerr << "XML support was not compiled into library. Isotope based tests will not work" << endl;
#endif

}
