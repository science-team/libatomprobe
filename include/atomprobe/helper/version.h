/* version.h : version tracking information
 * Copyright (C) 2020  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef LIBATOMPROBE_VERSION_H
#define LIBATOMPROBE_VERSION_H

#include "stringFuncs.h"


#define LIBATOMPROBE_MAJOR 0
#define LIBATOMPROBE_MINOR 0
#define LIBATOMPROBE_REVISION 1

#include <string>
#include <cstdlib>
#include <iostream>

namespace AtomProbe
{

//!Class to hold the library version data
class LibVersion
{
	bool isDebug;
	public:
	LibVersion() { 
	#ifdef DEBUG
		std::cerr << "libatomprobe built with DEBUG enabled. This will be slow!" <<std::endl;
		std::cerr << "Using libatomprobe " << LIBATOMPROBE_MAJOR << "." << LIBATOMPROBE_MINOR << "." << LIBATOMPROBE_REVISION << std::endl;
		isDebug=true;
	#else
		isDebug=false;
	#endif
		
		checkDebug();

	}

	void checkDebug();
	static unsigned int getMajor() { return LIBATOMPROBE_MAJOR;}
	static unsigned int getMinor() { return LIBATOMPROBE_MINOR;}
	static unsigned int getRevision() { return LIBATOMPROBE_REVISION;}

	//!Obtain the version of the program as a string
	static std::string getVersionStr() { 
		std::string tmp1,tmp2;
		stream_cast(tmp1,getMajor());
		tmp1+=".";
		stream_cast(tmp2,getMinor());
		tmp1+=tmp2 +".";
		stream_cast(tmp2,getRevision());
		tmp1+=tmp2;
		return tmp1;
	};

};

}

#endif
