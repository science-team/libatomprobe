/* 
 * Copyright (C) 2013  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * 
 */
#ifndef ATOMPROBE_XMLHELPER_H
#define ATOMPROBE_XMLHELPER_H

#include "atomprobe/apt_config.h"

#ifdef HAVE_LIBXML2

#include "atomprobe/helper/stringFuncs.h"  

#include <libxml/xmlreader.h>
#include <string>
#include <vector>

#ifdef DEBUG
#include <iostream>
#endif


namespace AtomProbe {

enum
{
	PROP_PARSE_ERR = 1,
	PROP_BAD_ATT
};

/* Cheat codes for XMLHelpNextType
Enum xmlElementType
{
    XML_ELEMENT_NODE = 1,
    XML_ATTRIBUTE_NODE = 2,
    XML_TEXT_NODE = 3,
    XML_CDATA_SECTION_NODE = 4,
    XML_ENTITY_REF_NODE = 5,
    XML_ENTITY_NODE = 6,
    XML_PI_NODE = 7,
    XML_COMMENT_NODE = 8,
    XML_DOCUMENT_NODE = 9,
    XML_DOCUMENT_TYPE_NODE = 10,
    XML_DOCUMENT_FRAG_NODE = 11,
    XML_NOTATION_NODE = 12,
    XML_HTML_DOCUMENT_NODE = 13,
    XML_DTD_NODE = 14,
    XML_ELEMENT_DECL = 15,
    XML_ATTRIBUTE_DECL = 16,
    XML_ENTITY_DECL = 17,
    XML_NAMESPACE_DECL = 18,
    XML_XINCLUDE_START = 19,
    XML_XINCLUDE_END = 20,
    XML_DOCB_DOCUMENT_NODE = 21
}    
*/


//These functions return nonzero on failure,
//zero on success
//be warned that the node WILL be modified.
unsigned int XMLHelpNextType(xmlNodePtr &node,int);
//Jump forward to the element matching "nodename"
unsigned int XMLHelpFwdToElem(xmlNodePtr &node,  const char *nodeName);
//Jump forward to an element that dooesn't match "nodeName"
unsigned int XMLHelpFwdNotElem(xmlNodePtr &node,const char *nodeName);
//this will match the node name against a list, stopping at the first matching
unsigned int XMLHelpFwdToList(xmlNodePtr &node, const std::vector<std::string> &nodeList);
//Get the text associated with this node (node->content as a string)
std::string XMLHelpGetText(xmlNodePtr &node);

//!Free a xmlDoc pointer. For use in conjunction with std::unique_ptr for auto-deallocation 
void XMLFreeDoc(void* data);

//Returns 0 if successful, non zero if there is a property failure, or if the property is empty
template<class T> unsigned int XMLHelpGetProp(T &prop,xmlNodePtr node, std::string propName)
{
	xmlChar *xmlString;

	//grab the xml property
	xmlString = xmlGetProp(node,(const xmlChar *)propName.c_str());

	//Check string contents	
	if(!xmlString)
		return PROP_PARSE_ERR;

	if(stream_cast(prop,xmlString))
	{
		xmlFree(xmlString);
		return PROP_BAD_ATT;
	}
			
	xmlFree(xmlString);

	return 0;
}


//!Grab the specified attribute from the next element, then stream_cast() it into the passed-in object. Returns true on success, false on error
template<class T> 
bool XMLGetNextElemAttrib(xmlNodePtr &nodePtr, T &v, const char *nodeName, const char *attrib)
{
	std::string tmpStr;
	xmlChar *xmlString;
	//====
	if(XMLHelpFwdToElem(nodePtr,nodeName))
		return false;

	xmlString=xmlGetProp(nodePtr,(const xmlChar *)attrib);
	if(!xmlString)
		return false;
	tmpStr=(char *)xmlString;

	if(stream_cast(v,tmpStr))
	{
		xmlFree(xmlString);
		return false;
	}

	xmlFree(xmlString);

	return true;
}

/* Defined in the bowels of the xmlLib2 library
 * Enum xmlElementType {
 *	XML_ELEMENT_NODE = 1
 *	XML_ATTRIBUTE_NODE = 2
 *	XML_TEXT_NODE = 3
 *	XML_CDATA_SECTION_NODE = 4
 *	XML_ENTITY_REF_NODE = 5
 *	XML_ENTITY_NODE = 6
 *	XML_PI_NODE = 7
 *	XML_COMMENT_NODE = 8
 *	XML_DOCUMENT_NODE = 9
 *	XML_DOCUMENT_TYPE_NODE = 10
 *	XML_DOCUMENT_FRAG_NODE = 11
 *	XML_NOTATION_NODE = 12
 *	XML_HTML_DOCUMENT_NODE = 13
 *	XML_DTD_NODE = 14
 *	XML_ELEMENT_DECL = 15
 *	XML_ATTRIBUTE_DECL = 16
 *	XML_ENTITY_DECL = 17
 *	XML_NAMESPACE_DECL = 18
 *	XML_XINCLUDE_START = 19
 *	XML_XINCLUDE_END = 20
 *	XML_DOCB_DOCUMENT_NODE = 21
 *	}
 */
};
#endif
#endif
		
