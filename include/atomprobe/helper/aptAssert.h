/* 
 * Copyright (C) 2014  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APTASSERT_H
#define APTASSERT_H
#include <cassert>
#include <iostream>

namespace AtomProbe
{
	//!Do assertions cause a straight up crash (enabled), or write a mesage to stderr (disabled)? By default, hard crash
	void setHardAssert(bool enabled); 

	//!Either abort program, or ask the user what to do for an assertion. depending on hardAssert setting 
	void askAssert(const char *, unsigned int);
};

#ifdef DEBUG
#define ASSERT(f) {if(!(f)){AtomProbe::askAssert(__FILE__,__LINE__);}}

#ifndef WARN
#define WARN(f,g) { if(!(f)) { std::cerr << "WARNING: " << __FILE__ << ":" <<__LINE__<< g << std::endl;}}
#endif

#ifndef TEST
#define TEST(f,g) {if(!(f)) { std::cerr << "Test fail :" << __FILE__ << ":" << __LINE__ << "\t"<< g << std::endl;return false;}}
//Quick/quiet test
#define TEST_Q(f) {if(!(f)) { std::cerr << "Test fail :" << __FILE__ << ":" << __LINE__ << std::endl;return false;}}
#endif


#else
	#define ASSERT(f)
	#define WARN(f,g) 
	#define TEST(f,g)
#endif

#endif
