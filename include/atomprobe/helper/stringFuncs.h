/*
 *	helper/stringFuncs.h - String manipulation header 
 *	Copyright (C) 2014, D Haley 

 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.

 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.

 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ATOMPROBE_STRINGFUNCS_H
#define ATOMPROBE_STRINGFUNCS_H

#include <string>
#include <sstream>
#include <vector>

namespace AtomProbe 
{


//!Template function to cast and object to another by the stringstream
//IO operator
template<class T1, class T2> bool stream_cast(T1 &result, const T2 &obj)
{
    std::stringstream ss;
    ss << obj;
    ss >> result;
    return ss.fail();
}

template<class T>
void strAppend(std::string s, const T &a)
{
	std::string tmp;
	stream_cast(tmp,a);
	s+=tmp;
}

//Convert an RGBA 8-bit/channel quadruplet into its hexadecimal colour string

void genColString(unsigned char r, unsigned char g, 
			unsigned char b, unsigned char a, std::string &s);
//Convert an RGB 8-bit/channel triplet into its hexadecimal colour string
void genColString(unsigned char r, unsigned char g, 
			unsigned char b, std::string &s);

//!Parse a colour  string containing rgb[a]; hex for with leading #
bool parseColString(const std::string &str,
	unsigned char &r, unsigned char &g, unsigned char &b, unsigned char &a);

//!Generate a string with leading digits up to maxDigit (eg, if maxDigit is 424, and thisDigit is 1
//leading digit will generate the string 001
std::string digitString(unsigned int thisDigit, unsigned int maxDigit);

//Strip given whitespace (\f,\n,\r,\t,\ )from a string
std::string stripWhite(const std::string &str);
//Strip specified chars from a string
std::string stripChars(const std::string &Str, const char *chars);
//!Return a lowercase version for a given string
std::string lowercase(std::string s);
//!Return a uppercase version for a given string
std::string uppercase(std::string s);

//Drop empty entries from a string of vector
void stripZeroEntries(std::vector<std::string> &s);

//!Strip whitespace, (eg tab,space) from either side of a string
std::string stripWhite(const std::string &str);

//!Split string references using a single delimiter.
void splitStrsRef(const char *cpStr, const char delim,std::vector<std::string> &v );

//!Split string references using any of a given string of delimiters
void splitStrsRef(const char *cpStr, const char *delim,std::vector<std::string> &v );

//!Return only the filename component
std::string onlyFilename( const std::string& path );
//!Return only  the directory name component of the full path 
// - do not use with UNC windows paths
std::string onlyDir( const std::string& path );

//!Convert a path format into a native path from unix format
// - do not use with UNC windows paths
std::string convertFileStringToNative(const std::string &s);

//!Convert a path format into a unix path from native format
std::string convertFileStringToCanonical(const std::string &s);

//Print N tabs to a string
inline std::string tabs(unsigned int nTabs)
{
	std::string s;
	s.resize(nTabs);
	std::fill(s.begin(),s.end(),'\t');
	return s;
}

//Brute force convert a wide STL str to a normal stl str
inline std::string stlWStrToStlStr(const std::wstring& s)
{
	std::string temp(s.length(),' ');
	std::copy(s.begin(), s.end(), temp.begin());
	return temp; 
}
//Brute force convert an stlStr to an stl wide str
inline std::wstring stlStrToStlWStr(const std::string& s)
{
	std::wstring temp(s.length(),L' ');
	std::copy(s.begin(), s.end(), temp.begin());
	return temp;
}

//Set a given token to null, to trim a string
void nullifyMarker(char *buffer, char marker);

//!Count the number of code points in a given unicode (8-bit char *) string
// - Must be null terminated. 
// - number of code points is output by "count"
// - Return value is false if malformed
bool countCodePoints(const uint8_t* s, size_t* count) ;
}
#endif
