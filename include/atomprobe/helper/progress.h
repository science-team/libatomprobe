/* 
 * progress.h: Simple progress bar
 * Copyright (C) 2015  D. Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_PROGRESSBAR_H
#define ATOMPROBE_PROGRESSBAR_H


namespace AtomProbe {


class ProgressBar
{
	private:
		//!Length of the progress bar in characters
		unsigned int length;
		//!Last progress value that was used
		unsigned int lastProg;
		//!Do we want to keep printing the bar? 
		// Useful if e.g. error conditions arise
		bool printEnd;
		float accumulatedTicks;
	public:
		ProgressBar() { reset();}
		~ProgressBar(); 
		//!Set the number of markers in the progress bar
		void setLength(unsigned int l){ length=l;}

		//!Draw the initial progress bar.
		void init();

		//!reset the progress bar internals, in case we want to re-use it
		void reset();
		
		//!Draw the progress bar as needed, using the given progress value [0,100]
		void update(unsigned int newProgress);
	
		//!Finalise the progress bar. It is not necessary for the progress to be set to 100%,  this is done for you.	
		void finish();

		//!Abort drawing the progress 
		void abort(){printEnd=false;};
		
};

#ifdef DEBUG
bool testProgressBar();
#endif

}

#endif
