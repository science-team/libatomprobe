/*
 *	helper/convert.h - Conversion functions between differing datatypes
 *	Copyright (C) 2015, D Haley 

 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.

 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.

 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ATOMPROBE_CONVERT_H
#define ATOMPROBE_CONVERT_H
#include <vector>

#include "atomprobe/primitives/ionHit.h"
#include "atomprobe/atomprobe.h"


namespace AtomProbe 
{

	//!Convert an incoming entry of EPOS files to pos, and the append this to the pos vector given
	void convertEPOStoPos(const std::vector<EPOS_ENTRY> &eposEntry, 
						std::vector<IonHit> &posFile);
}

#endif
