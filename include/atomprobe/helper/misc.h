/*
 * misc.h - Miscellaneous helper functions
 * Copyright (C) 2015  D Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATOMPROBE_MISC_H
#define ATOMPROBE_MISC_H
#include "stringFuncs.h"

namespace AtomProbe
{
//!Data holder for colour as float
class RGBf
{
	public:
	float red;
	float green;
	float blue;

	//!Convert to hex-256 representation #rrggbb
	std::string toHex() const {
			std:: string  s; 
			genColString(red*255,green*255,blue*255,s);
			return s;
	};

	void fromHex(const std::string &s)
	{
		unsigned char c[4];
		parseColString(s,c[0],c[1],c[2],c[3]);

		red=c[0]/255.0f;
		green=c[1]/255.0f;
		blue=c[2]/255.0f;
	}

	inline bool operator==(const RGBf & oth) const
	{
		return (oth.red == red && oth.green == green && oth.blue==blue);
	}

};

//!Test for equality within tolerance f (||a-b|| < f)
template<class T>
bool tolEqual(const T &a,const T &b, const T &f)
{
	return abs(a-b) < f;
}


//!Obtain the elements at positions indicies in the input vector, copy to output
template<class T>
void selectElements(const std::vector<T> &in, const std::vector<unsigned int> &indices, std::vector<T>& out)
{
	out.resize(indices.size());
#pragma omp parallel for
	for(auto i=0u; i<indices.size(); ++i)
		out[i]=in[indices[i]];
}


//!Perform an out-of-place tranposition of a given vector.
/*!The nested vector may be ragged (ie not a rectangular array)
  This will temporarily double memory use due to copy
*/
template<class T>
void transposeVector(std::vector<std::vector<T> > &v)
{
	if(!v.size())
		return;

	std::vector<std::vector<T> > transposed;
	transposed.resize(v[0].size());

	for(size_t ui=0;ui<transposed.size();ui++)
	{
		transposed[ui].resize(v.size());
	}

#pragma omp parallel for
	for(size_t ui=0;ui<v.size();ui++)
	{	
		for(size_t uj=0;uj<v[ui].size();uj++)
		{
			transposed[uj][ui]=v[ui][uj];
		}
	}

	v.swap(transposed);
}

//!Remove elements from the vector, without preserving order.
/* The pattern of removal will be unique with a given kill pattern.
 the vectors must be the same size, and items for which the boolean vector 
 is true will be removed
*/
template<class T>
void vectorMultiErase(std::vector<T> &vec, const std::vector<bool> &wantKill)
{
	if(!vec.size())
		return;
	size_t shift=0;	
	for(size_t ui=0;ui<vec.size();ui++)
	{
		if(wantKill[ui])
			shift++;
		else if(shift)
			vec[ui-shift] = vec[ui];
	}
	vec.resize(vec.size()-shift);
}

#ifdef DEBUG
//Return true on success
bool runMiscMathsTests();
#endif
}

#endif
