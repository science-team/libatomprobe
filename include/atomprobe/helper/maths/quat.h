/*
 *	mathfuncs.h - General mathematic functions header
 *	Copyright (C) 2015, D Haley 

 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.

 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.

 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ATOMPROBE_MATHFUNCS_H
#define ATOMPROBE_MATHFUNCS_H

#include <cmath>
#include <limits>
#include <iostream>
#include <vector>
#include <algorithm>

#include <gsl/gsl_matrix.h>

#include "atomprobe/primitives/point3D.h"

namespace AtomProbe
{

//!Data storage structure for quaternions
typedef struct 
{
	float a; //Real component
	float b;
	float c;
	float d;
} Quaternion;

//TODO: Merge with point3D?
//!Data storage structure for points
typedef struct
{
	float fx;
	float fy;
	float fz;
} Point3f;

//Uses quaternion mathematics to perform a rotation around your favourite axis
//IMPORTANT: rotVec must be normalised before passing to this function 
//failure to do so will have weird results
//Note result is stored in  point passed as argument
//angle is in radians.

//Inefficient-sh Point3D version
//!Rotate a point around a given rotation axis by a specified angle
/*!This is not efficient for large numbers of points, as this will
 * recompute the quaternion each time
 */
void quat_rot(Point3D &p, const Point3D &r, float angle);

//!Rotate a point around a given vector, with specified angle
/*!This is not efficient for large numbers of points, as this will
 * recompute the quaternion each time.
 * angle (radians) is anti-clockwise as viewed from positive side of vector (right-handed)
 */
void quat_rot(Point3f *point, const Point3f *rotVec, float angle);

//!Rotate each point in array of size n around a given vector, with specified angle
/*! rotVec is the rotation, angle is the anti-clockwise angle as viewed from positive side of vector
 * ( right-handed)
 * rotation of points is in-place
 */
void quat_rot_array(Point3f *point, unsigned int n, const Point3f *rotVec, float angle);

//!Rotate each point in array of size n around a given vector, with specified angle
/*! rotVec is the rotation, angle is the anti-clockwise angle as viewed from positive side of vector
 * ( right-handed).
 * rotation of points is in-place
 */
void quat_rot_array(Point3D *point, unsigned int n, const Point3f *rotVec, float angle);


//!Compute the quaternion  for specified rotation. 
/*! angle is in radians.
 *  pass to quat_rot_apply_quat to compute rotation
 */
void quat_get_rot_quat(const Point3f *rotVec, float angle,  Quaternion *rotQuat);

//!Use previously generated quats from quat_get_rot_quats to rotate a point
/*! Rotation is in-place, on single point
  */
void quat_rot_apply_quat(Point3f *point, const Quaternion *rotQuat);


#ifdef DEBUG
bool testMathfuncs();
#endif

}	
#endif
