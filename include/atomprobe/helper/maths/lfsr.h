/*
 *	helper/maths/lfsr.h - Linear shift feedback register
 *	Copyright (C) 2015, D Haley 

 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.

 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.

 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdlib>

namespace AtomProbe
{

//!This class implements a Linear Feedback Shift Register (in software) 
/*!This is a mathematical construct based upon polynomials over closed natural numbers (N mod p).
This will generate a weakly random digit string, but with guaranteed no duplicates, using O(1)
memory and O(n) calls. The no duplicate guarantee is weak-ish, only guaranteeing no repetition in the
shift register for 2^n-1 iterations. n can be set by setMaskPeriod. After this the sequence will repeat
*/
class LinearFeedbackShiftReg
{
	size_t lfsr;
	size_t maskVal;
	size_t totalMask;
	public:
		//!Get a value from the shift register, and advance
		size_t clock();
		//!Set the internal lfsr state. Note 0 is the lock-up state.
		void setState(size_t newState) { lfsr=newState;};
		//!Set the mask to use such that the period is 2^n-1. 3 is minimum 60 is maximum
		void setMaskPeriod(unsigned int newMask);

		//!Check the validity of the table
		bool verifyTable(size_t maxLen=0);
};
}
