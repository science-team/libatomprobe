#ifndef ATOMPROBE_FITTING_H
#define ATOMPROBE_FITTING_H


namespace AtomProbe
{

//!Fit a Voigt function to the given X/Y values. Internally, a function minimiser is used
/*!	Voigt functions are used in spectroscopy as a general curve for fitting spectra.
  	 they are symmetric, and won't fit "tailed" curves well

	autoinit : If true, then initial values are chosen automatically
		and heuristically, if false, input parameters must be given, and are
		used to seed the minimiser
	outputs are sigma/gamma (voigt parameters), mu (x shift) and amp (y-scale)
*/
bool fitVoigt(const std::vector<double> &x, const std::vector<double> &y, 
	double &sigma, double &gamma, double &mu, double &amp, bool autoInit=true);


//!Generate a shifted voigt profile.
void voigtProfile(const std::vector<double> &x, double sigma, 
	double gamma, double mu, double amp, std::vector<double> &y);

//!Fit a Doniach-Sunjic curve
bool fitDoniachSunjic(const std::vector<double> &x, const std::vector<double> &y, 
	double &a, double &f, double &mu, double &amp, bool autoInit=true);

//!generate a Doniach-Sunjic profile
void doniachSunjic(const std::vector<double> &x, double &a, double &F, double &mu,double &amp,
				std::vector<double> &y);

//!Fit a smoothed log-gaussian curve (arxiv:0711.4449)
bool fitLikeLogGaussian(const std::vector<double> &x, const std::vector<double> &y, 
	double &lambda, double &sigma, double &xp, double &amp, double &h,bool autoInit=true);

//!Generate  a smoothed log-gaussian curve
/*! xp : positioning
   lambda :asymmetry 
   h : FWHM (>0)
  amp : vertical rescale
  sigma : width
*/
void likeLogGaussian(const std::vector<double> &x, double &xp, double &sigma,
				double &lambda,double &amp, double &h,
				std::vector<double> &y);

//!Fit a smoothed log-gaussian curve (arxiv:0711.4449)
bool fitExpNorm(const std::vector<double> &x, const std::vector<double> &y, 
	double &K,  double &mu, double &sigma,double &amp, bool autoInit=true);

//! Exponentially decaying normal distribution
/* mu - shift.
   sigma - squeeze
  K - asymmetry
  amp - amplitude
*/
void expNorm(const std::vector<double> &x, double &K, double &mu, double&sigma, double &amp, std::vector<double> &y);

double lsq(const std::vector<double>  &y, const std::vector<double> &yFit);
}
#endif
