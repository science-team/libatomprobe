/* processing.h : Mass spectrum processing functions
 * Copyright (C) 2018  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_PROCESSING_H
#define ATOMPROBE_PROCESSING_H
namespace AtomProbe
{
//!Background fitting modes
enum
{
	BACK_FIT_MODE_CONST_TOF,
	BACK_FIT_MODE_ENUM_END,
};

struct BACKGROUND_PARAMS
{
	enum
	{
		FIT_FAIL_MIN_REQ_BINS=1,
		FIT_FAIL_AVG_COUNTS,
		FIT_FAIL_INSUFF_DATA,
		FIT_FAIL_DATA_NON_GAUSSIAN,
		FIT_FAIL_END
	};
	//background fitting mode to use
	unsigned int mode;
	//-- start/end window for const tof background fit
	float massStart, massEnd;
	//step size in bins for fitting histogram
	float binWidth;

	//result parameters
	float intensity,stdev; 

};


//!Perform a background fit, assuming constant TOF noise, from 0->inf time.
/*!	- Background params has the input and output data for the fit.
	- dataIn requires raw mass data, not histogram counts.
	- returns zero on success, nonzero on error
 Return value is stored in params.intensity, as counts/bin.
 Use createMassBackground to re-generate the histogram
*/
unsigned int doFitBackground(const std::vector<float> &massData, BACKGROUND_PARAMS &params) ;

//!Obtain a human readable error, which has arison when using doFitBackground
//! Input is the error code from doFitBackground
std::string getFitErrorMsg(unsigned int errCode); 

//!Build a histogram of the background counts
/*!- Inputs : Start and end mass for background window, and bin count 
   - tofBackIntensity is the intensity level per unit time (proportional
     to sqrt mass) in the background, as obtained by doFitBackground.
   
   Note : You may need to divide by bin width, depending on your application
*/
void createMassBackground(float massStart, float massEnd, unsigned int nBinsMass,
			float tofBackIntensity, std::vector<float> &histogram);

//!Simple peak-finding algorithm. 
// for your application
/*! 
	Adapted from Nathaniel Yoder's:
		https://uk.mathworks.com/matlabcentral/fileexchange/25500-peakfinder-x0-sel-thresh-extrema-includeendpoints-interpolate
		which is BSD licenced.

	The performance of this function may or may not be suitable for
	your application, and your histogram may require pre-processing
	to optimise false postive/false-negative performance
	
	- x0 : input mass histogram, must be > 2 elements
	- sel : Selectivity for peaks - larger is more selective
	- autosel : whether to determine sel automatically, or not
	- includeEndpoints : allow edge values to be peaks
*/
void findPeaks(const std::vector<float> &x0, std::vector<unsigned int>& peakInds, float sel, 
		bool autoSel=true,bool includeEndpoints=true);

#ifdef DEBUG
bool testBackgroundFitMaths();
#endif
}
#endif
