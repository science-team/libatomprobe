
/* axialdf.h : Axial distribution function routines
 * Copyright (C) 2014  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_DECONVOLUTION_H
#define ATOMPROBE_DECONVOLUTION_H
#include <vector>
#include <utility>

#include "atomprobe/io/multiRange.h"
#include "atomprobe/isotopes/abundance.h"

namespace AtomProbe
{

	//!Solve overlaps using least squares for given multi-range. 
	// Returns false if overlap cannot be uniquely solved (e.g. rank deficient/multiple solutions)
	// return vector contains pairs of ionID, and number of hits.
	// - Background estimation provided by the background estimator
	//   function. This will subtract the estimated counts from each range
	//   given in the supplied function (startMass,endMass). If the
	//   function given is null, then background  estimation will not be used.
	bool leastSquaresOverlapSolve(const AtomProbe::MultiRange &rangeData, const AtomProbe::AbundanceData &abundance, 
			const std::vector<IonHit> &hits, float (*backgroundEstimator)(float,float),
			std::vector<std::pair<unsigned int,float> > &decomposedHits, float rangeTolerance=0);


#ifdef DEBUG
	bool testDeconvolve();
#endif

}

#endif
