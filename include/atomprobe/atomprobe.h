/* atomprobe.h : Atom probe library base header - use "include" in your program to use libatomprobe
 * Copyright (C) 2014  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_H
#define ATOMPROBE_H

/*! \mainpage Libatomprobe
 *
 * \section intro_sec Introduction
 *
 * Libatomprobe is a library for Atom Probe Tomography (APT)
 * computations. This includes a wide range of algorithms for 
 * which can be accessed from multiple languages, such as ,
 * for example python and C++.
 *
 * \section install_sec Installation
 * To install libatomprobe, see the instructions in the doc/README.txt folder
 *
 * \section reporting_sec Support
 * If you notice a problem within the library, or are having trouble using it,
 * please contact us ( mycae -at- gmx.com) ; ( andy.london -at- ukaea.uk) for support.
 * We aim to fix problems quickly, and help get your analysis up and running!
 */

//CMake configure-time header,
// this tells us what was enabled when library was built
#include "apt_config.h"

#include "io/ranges.h"
#include "io/multiRange.h"
#include "io/dataFiles.h"

#include "algorithm/K3DTree-approx.h"
#include "algorithm/K3DTree-exact.h"
#include "algorithm/rotations.h"
//Bug : qhull crashes under cygwin. Disable this functionality 
#ifndef __CYGWIN__
#include "algorithm/convexHull.h"
#endif
#include "algorithm/axialdf.h"
#include "algorithm/histogram.h"
#include "algorithm/massTool.h"
#include "algorithm/filter.h"
#include "algorithm/rangeCheck.h"
#include "algorithm/componentAnalysis.h"

#include "helper/progress.h"
#include "helper/misc.h"
#include "helper/stringFuncs.h"
#include "helper/sampling.h"
#include "helper/composition.h"
#include "helper/convert.h"
#include "helper/maths/quat.h"
#include "helper/maths/misc.h"
#include "helper/version.h"

#include "isotopes/abundance.h"
#include "isotopes/overlaps.h"
#include "isotopes/massconvert.h"

#include "projection/projection.h"

#include "latticeplanes/millerIndex.h"
#include "latticeplanes/generate.h"

#include "reconstruction/reconstruction-simple.h"

#include "spectrum/processing.h"

#include "primitives/mesh.h"

#include "statistics/confidence.h"

#include "deconvolution/deconvolution.h"

#include "helper/voxels.h"

#include "algorithm/isoSurface.h"

#include "io/multiRange.h"
#include "io/apthdf5.h"
#include <gsl/gsl_rng.h>


#ifdef EXPERIMENTAL
	#include <cstdlib>
	#include "spectrum/fitting.h"

#endif

namespace AtomProbe
{


//! Library-wide singleton random number source. Uses GSL as backend
//! Defaults to marsenne twister 
class RandNumGen
{
	private:
		//TODO: Remove GSL, use std library?
		gsl_rng *rng;
	public:
		RandNumGen();
		~RandNumGen();
		//!Obtain a GSL random number generator
		gsl_rng* getRng() const { return rng;};
};

extern LibVersion libVersion;
extern RandNumGen randGen;

}

#endif
