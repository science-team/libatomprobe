/* ion.h : atomic and molecular ion primitive
 * Copyright (C) 2014  Daniel Haley
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_ION_H
#define ATOMPROBE_ION_H

#include <iostream>
#include <vector>
#include <string>
#include <utility>

namespace AtomProbe{

/*! \brief A molecular ion data class
 *
 * A molecular ion class that has storage
 * for a name, charge state and isotopic fingerprint
 * as well as functions for converting to/from
 * text.
 */

class Ion
{
        private:
                //!Charge state value, zero for unassigned
                unsigned int _chargeState;

		//! "friendly" name: "TiO2"
		std::string _ionName;

		//! molecular formula "TiO2"
		std::string _ionFormula;

		//! Elemental fragments (('Ti',2),('O',1), ...)
		std::vector<std::pair<std::string,size_t> > _fragments;

		//! Isotopic distribution or fingerprint ((mass1, abundance1), (mass2, abundance2), ...)
		std::vector<std::pair<float,float> > _isotopicFingerprint;

        public:
		//!Constructor with no initialisation
		Ion();

		//!Constructor initialising values from name
		Ion(const std::string &strName);

		//!Constructor initialising values from name and charge state
		Ion(const std::string &strName, const unsigned int chargeState);

		//! Turn a ion string name e.g. "TiO" into a vector-pair fragments list
		//! like (("Ti",1) ("O",1))
		bool ionStr2ionFragments(const std::string &name,
		                        std::vector<std::pair<std::string,size_t> > &fragments);

		//! Take a vector of ion fragments and make an ion name
		bool ionFragments2ionStr(const std::vector<std::pair<std::string,size_t> > &fragments,
		                              std::string &name);

		//! Should have a formatted name as well for LaTeX and HTML sytle names

		//! Set the ion fragments to a vector of pairs(string, unsigned int)
		void setIonFragments(std::vector<std::pair<std::string, size_t> > ionFragments) {_fragments = ionFragments;}

		//! Get the ion fragments as a vector of pairs(string, unsigned int)
		std::vector<std::pair<std::string, size_t> > getIonFragments(void) {_fragments;}

		//! Set charge state
		void setChargeState(const unsigned int chargeState) {_chargeState = chargeState;}


};
}
#endif
