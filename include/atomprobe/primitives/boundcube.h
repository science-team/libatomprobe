/* boundcube.h : Bounding cube class
 * Copyright (C) 2014  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_BOUNDCUBE_H
#define ATOMPROBE_BOUNDCUBE_H

#include "atomprobe/primitives/point3D.h"

#include <vector>

namespace AtomProbe{

class IonHit;

//!Helper class to define a bounding cube
class BoundCube
{
	//!bounding values (x,y,z) (lower,upper)
	float bounds[3][2];
#ifdef DEBUG
	//!Is the cube set?
	bool valid[3][2];
#endif
public:

	BoundCube()
	{
#ifdef DEBUG
		setInvalid();
#endif
	}

	BoundCube(const std::vector<Point3D> &pts)
	{
		setBounds(pts);
	}

	BoundCube(const std::vector<IonHit> &pts)
	{
		setBounds(pts);
	}


	BoundCube(const Point3D &p1, const Point3D &p2);
	
	bool operator==(const BoundCube &other) const;
	//!Set the bounds by passing in minima and maxima of each dimension
	void setBounds(float xMin,float yMin,float zMin,
			float xMax,float yMax,float zMax);

	//!Set an individual bound
	/*! bound : dimension of bound, 0=x,1=y,2=z	
	* minMax : 0 for lower bound, 1 for upper bound
	*/
	void setBound(unsigned int bound, unsigned int minMax, float value) ;

	//!Set the bounding cube to be the same as a different cube
	void setBounds(const BoundCube &b);
	//!Obtain bounds from an array of Point3Ds
	void setBounds( const Point3D *ptArray, unsigned int nPoints);
	//!Use two points to set bounds -- does not need to be high,low. This is worked out
	void setBounds( const Point3D &p, const Point3D &q);
	//!Obtain bounds from an array of Point3Ds
	void setBounds(const std::vector<Point3D> &ptArray);
	//!Obtain bounds from an array of IonHits 
	void setBounds(const std::vector<IonHit> &ptArray);
	//!Set the bounds using a single point and radius. 
	/*!Bounds will be minimal sphere enclosing,
	 where the sphere is defined by the (p,radius) pair
	 */
	void setBounds(const Point3D &p, float radius);
#ifdef DEBUG
	//! Returns true if all bounds are valid
	bool isValid() const;
	//!Returns true if valid on the specified dimension
	bool isValid(unsigned int dim) const;

	//!Set the bounding cube to be invalid
	void setInvalid();
#endif
	//!Set the cube to be "inside out" at the limits of numeric results;
	void setInverseLimits();

	//!Return the bound on the given dimension
	/*! bound - the dimension [x=0,y=1,z=2]
	*   minMax - 0 for lower bound, 1 for upper bound
	*/
	float getBound(unsigned int bound, unsigned int minMax) const ;
	//!Return the centroid
	Point3D getCentroid() const;

	//!Get the bounds of the cube as two points, the lower left (minimum bound) and upper right corner (max bound)
	void getBounds(Point3D &low, Point3D &high) const ;

	//Return the lower left (min) bound 
	Point3D min() const;
	
	//Return the lower left (min) bound 
	Point3D max() const;

	//!Return the size of the side parallel to the given dimension
	float getSize(unsigned int dim) const;


	//! Returns true if any bound is of null thickness
	bool isFlat() const;

	//!Checks if a point intersects a sphere of centre Pt, radius^2 sqrRad
	bool intersects(const Point3D &pt, float sqrRad) const;

	//!Check to see if this box intersects another
	bool intersects(const BoundCube &b) const;


    	//!Does this bounding box entirely contain another
    	bool contains(const BoundCube &b) const;
	//!Check to see if the point is contained in, or part of the walls of the cube
	bool containsPt(const Point3D &pt) const;

	//!Returns true if this box is contained by the sphere, represented by the origin+radius 
	bool containedInSphere(const Point3D &origin, float radius) const;
    
    	//!Create the box containing a union of two bounding cubes
 	BoundCube makeUnion(const BoundCube &b) const;
    	
	//!Create the box which is the intersection of two bounding cubes this is either null, or a box
 	BoundCube makeIntersection(const BoundCube &b) const;

	//!Returns maximum distance to box corners (which is an upper bound on max box distance).
	float getMaxDistanceToBox(const Point3D &pt) const;

	//!Get the largest dimension of the bound cube
	float getLargestDim() const;

	//!Obtain the volume of the cube
	float getVolume() const;

	//!Set min-max bounds to fp min/max
	void setLimits();
	//!Return a triplet to indicate a spatial partition value lies in
	/*!Returns which segment (lower:0, inside:1, higher:2) the given slice
	 coordinate is at the specified dimension (eg, for a box [-1,1]
	 in y, segmentTriple(1,-2) would return 0.
	*/
    	unsigned int segmentTriple(unsigned int dim, float slice) const;

	BoundCube operator=(const BoundCube &);
	//!Expand (as needed) volume such that the argument bounding cube is enclosed by this one
	void expand(const BoundCube &b);
	//!Expand such that point is contained in this volume. Existing volume must be valid
	void expand(const Point3D &p);

	//!Expand the bounding cube by this value in each dimension (upper and lower)
	void expand(float f);

	friend  std::ostream &operator<<(std::ostream &stream, const BoundCube& b);

	friend class K3DTreeExact;
	friend class K3DTreeApprox;
};
}
#endif
