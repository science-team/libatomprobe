/*
 * ionhit.h - Ion event data class
 * Copyright (C) 2014  D Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATOMPROBE_IONHIT_H
#define ATOMPROBE_IONHIT_H

#include <vector>

#include "atomprobe/helper/endianTest.h"

#include "point3D.h"
#include "boundcube.h"

namespace AtomProbe {


//!This is a data holding class for POS file ions, from
/* Pos ions are typically obtained via reconstructed apt detector hits
 * and are of form (x,y,z mass/charge)
 */
class IonHit
{
	private:
		float massToCharge; // mass to charge ratio in Atomic Mass Units per (charge on electron)
		Point3D pos; //position (xyz) in nm
	public:
		IonHit();
		IonHit(float *);
		//copy constructor
		IonHit(const IonHit &);
		IonHit(const Point3D &p, float massToCharge);
		IonHit(float x, float y, float z, float mass);

		//Set the data from a 4 byte float array. 
		void setHit(float *arr) { pos.setValueArr(arr); massToCharge=arr[3];};
		//Set only the mass/charge value
		void setMassToCharge(float newMassToCharge);
		//Set the position of the event
		void setPos(const Point3D &pos);

		//Set the xth (0,1,2) coordinate of the data
		void setPos(unsigned int idx, float pos);
		//Set the XYZ coords of the data
		void setPos(float fX, float fY, float fZ)
			{ pos.setValue(fX,fY,fZ);};

		//Retrieve  the XYZ coordinate for this data
		Point3D getPos() const;
		//obtain the XYZ coordinates as a constant ref.
		inline const Point3D &getPosRef() const {return pos;};
		//returns true if any of the 4 data pts are NaN
		bool hasNaN();

#ifdef __LITTLE_ENDIAN__		
		void switchEndian();
#endif

		//obtain the mass-to-charge for the event
		float getMassToCharge() const;


		//Helper functions
		//--
		//get the points from a vector of of ionhits
		static void getPoints(const std::vector<IonHit> &ions, std::vector<Point3D> &pts);

		//Get the bounding cube from a vector of ionhits
		static void getBoundCube(const std::vector<IonHit> &p, BoundCube &b);

		//Get the centroid from a vector of ion hits
		static void getCentroid(const std::vector<IonHit> &points, Point3D &centroid);
		
		//---

		//assignment operator
		const IonHit &operator=(const IonHit &obj);
		//equality operator
		bool operator==(const IonHit &obj) const;
		//array get operator (0,1,2,3 valid -> x/y/z/m)
		float operator[](unsigned int ui) const ;	

		//array get operator (0,1,2,3 valid -> x/y/z/m)
		float &operator[](unsigned int ui) ;       
		IonHit operator+(const Point3D &obj);

		//!Output streaming operator. Users (x,y,z,m) as format for output
		friend std::ostream &operator<<(std::ostream &stream, const IonHit &);

		static BoundCube getIonDataLimits(const std::vector<IonHit> &points);


};


//This is an entry in the epos "format".
// There is, to best knowledge, no official specification
// There are unofficial specifications
class EPOS_ENTRY
{
	public:
		//!Reconstruction coordinates in natural units (probably nm),
		//! and mass to charge information in amu/charge
		float x,y,z,massToCharge;
		//! timeOfFlight in ns. Correction status does not appear to be specified
		/*!voltDC is the standing voltage on the specimen for the detected pulse 
		 voltage pulse is the pulse intensity (V) 
                 */
		float timeOfFlight,voltDC,voltPulse;
		//!X and Y coordinates on the detector in mm, nominally uncorrected
		float xDetector,yDetector;
		// !Hit multiplicity is the number of hits, for the *FIRST MULTIPLE ONLY*
		/*!where a multiple hit is one where multiple ions arrive on the same pulse
		 Otherwise, zero.
		Delta pulse is the number of pulses before the last ion evaporation
		 in cases of multiple hits, the non-initial events have a zero value*/
		int32_t deltaPulse, hitMultiplicity;

	IonHit getIonHit() const ; 
	void getIonHit(IonHit &h) const ;

        //!Python convenience functions to avoid int32_t types
        int getDeltaPulse() { return deltaPulse;}
        int getHitMultiplicity() { return hitMultiplicity;}

	//equality operator
	bool operator==(const EPOS_ENTRY &obj) const;
};

const size_t EPOS_RECORD_SIZE = 11*4;

#ifdef DEBUG
bool testIonHit();
#endif

};

#endif
