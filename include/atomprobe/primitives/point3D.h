/* point3D.h : 3D point primitive
 * Copyright (C) 2014  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_POINT3D_H
#define ATOMPROBE_POINT3D_H

#include <iostream>
#include <cmath>
#include <vector>

#include <gsl/gsl_matrix.h>

#include "atomprobe/helper/endianTest.h"
#include "atomprobe/helper/stringFuncs.h"


//FIXME: This is not the right place for this
#include <gsl/gsl_linalg.h>
namespace AtomProbe{

//!A 3D point data class storage
/* A  3D point data class
 * contains operator overloads and some basic
 * mathematical functions
 */
class Point3D
{
        private:
		//!Value data
		float value[3];
	public:
		//!Constructor with no initialisation
		Point3D() {};
		
		//!Constructor initialising values from an array of length 4*(sizeof(float))
		Point3D(const float *f) {setValueArr(f);};
		
		//!Constructor with initialising values, X, Y and Z
		inline Point3D(float x,float y,float z) 
			{ value[0] = x, value[1] = y, value[2] = z;}
		
		
		//!Set value \a val of the \a ui-th dimension, \a ui in  (0,1,2)
		inline void setValue(unsigned int ui, float val){value[ui]=val;};
		
		//!Set the XYZ value fo the point	
		inline void setValue(float fX,float fY, float fZ)
			{value[0]=fX; value[1]=fY; value[2]=fZ;}

		//!Set from string representation
		/*!
		* From a string representation of a 3D point, 
		* parse and store the data in a Point3D.
		* 
		* Must contain 3 entries.
		* 
		* Values can be separated by whitespace or any 
		* of ,;|_
		* 
		* Polar notation is indicated by < ... > 
		* 
		* Polar notation assumes <radius,theta,phi> in 
		* degrees (they will be converted to radians internally) and then 
		* converted into cartesian coordinates (X,Y,Z)
		* 
		*     Point3D mypoint;
		*     mypoint.parse("<1,45,90>");
		*     cout << mypoint << endl;
		*
		* Returns:
		*
		*     (-3.09086e-08,0.707107,0.707107)
		*/
		bool parse(const std::string &s);
		
		//!Set value by pointer (X, Y, Z from array of len 3)
		inline void setValueArr(const float *val)
			{
				value[0]=*val;
				value[1]=*(val+1);
				value[2]=*(val+2);
			};

		//!Get value of ith dimension (0, 1, 2)
		inline float getValue(unsigned int ui) const {return value[ui];};
		
		//!Obtain a pointer to internal array (3 floats)
		inline const float *getValueArr() const { return value;};

		//!Copy data from internal into target array of length 3 : Pointer MUST be allocated
		/*!
		* As a quick example, you can copy into a buffer you own
		*     Point3D  p(1,2,3);
		*     float x[3];
		*     p.copyValueArr(x[0]);
		*/
		void copyValueArr(float *value) const;
		
		//!Add a point to this, without generating a return value
		/*! 
		Add a \a Point3D set of values to this Point3D.
		
		This is different to +=, because it generates no return value.
		*/
		void add(const Point3D &obj);
		
		//!Extend the vector by the specified distance
		/*! 
		* Assumes Point3D is a vector from the origin, and adds on 
		* a vector parallel to Point3D of length \a distance.
		*/
		void extend(float distance);

		//!Returns true if any of the 3 data pts are NaN
		bool hasNaN() const { return (std::isnan(value[0]) || 
					std::isnan(value[1]) || std::isnan(value[2]));};
		
		//!Equality operator
		bool operator==(const Point3D &pt) const;
		
		//!Assignment operator
		const Point3D &operator=(const Point3D &pt);
		
		//!+= operator
		const Point3D &operator+=(const Point3D &pt);
        
		//!-= operator
		const Point3D &operator-=(const Point3D &pt);
		
		//!*= operator, multiplies the whole Point3D by a scalar
		Point3D operator*=(const float scale);	
		
		//!Addition of Point3D objects (X1+X2, Y1+Y2, Z1+Z2)
		const Point3D operator+(const Point3D &pt) const;
		
		//!Addition of a scalar to a Point3D object (X+f, Y+f, Z+f)
		const Point3D operator+(float f) const;
		
		//!Scalar multiplication (X*f, Y*f, Z*f)
		const Point3D operator*(float scale) const;
		
		//!Elemental multiplication of two Point3D objects (X1*X2, Y1*Y2, Z1*Z2)
		const Point3D operator*(const Point3D &pt) const;
		
		//!Scalar division of Point3D by \a scale (X/scale, Y/scale, Z/scale)
		const Point3D operator/(float scale) const;
		
		//!Elemental division of two Point3D objects (X1/X2, Y1/Y2, Z1/Z2)
		const Point3D operator/(const Point3D &pt) const;
		
		//!Subtraction of two Point3D objects (X1-X2, Y1-Y2, Z1-Z2)
		const Point3D operator-(const Point3D &pt) const;
		
		//!Negation, Returns a Point3D object with the negative of the previous value
		const Point3D operator-() const;
		
		//!Output streaming operator. Streams values in the text format (x,y,z)
		friend std::ostream &operator<<(std::ostream &stream, const Point3D &);
		
		//!Make point unit magnitude, maintaining direction
		void normalise();	
		
		//!Return point unit magnitude, maintianing direction
		Point3D normal() const;	
		
		//!Returns the square of distance to another Point3D 
		/*! (X1-X2)^2 + (Y1-Y2)^2 ... */
		float sqrDist(const Point3D &pt) const;
		
		//!Calculate the dot product of this and another point 
		/*! (X1*X2 + Y1*Y2 ...) */
		float dotProd(const Point3D &pt) const;
		
		//!Calculate the cross product of this and another point
		Point3D crossProd(const Point3D &pt) const;

		//!Calculate the angle between two position vectors in radians
		float angle(const Point3D &pt) const;
		
		//!Returns magnitude^2, taking this as a position vector
		float sqrMag() const;

		//!Magnitude of position vector
		float mag() const { return sqrtf(sqrMag());}
	
		//!Array indexing operator, Point3D[1] returns the value of the ui-th dim
		float operator[](unsigned int ui) const;
		
		//!Array reference operator, Point3D[1] 
		/*!returns reference to data in ui-th dim */
		float &operator[](unsigned int ui); 
	
		//!Is this point inside a box bounded by orign and \a farPoint?	
		/*!Returns true if this point is located inside (0,0,0)	-> \a farPoint
		*
		* Assuming box shape (non zero edges return false)
		*
		* \a farPoint must be positive in all dim
		*/
		bool insideBox(const Point3D &farPoint) const;
		
		//!Is this point inside a box bounded by \a lowPt and \a hiPt?	
		/*!Returns true if this point is located inside \a lowPt -> \a hiPt
		*
		* Assuming box shape.
		*/
		bool insideBox(const Point3D &lowPt, const Point3D &hiPt) const;
		
		//!Makes each value negative of old value (-X, -Y, -Z)
		void negate();

		//!Makes each value its reciprocal (1/X, 1/Y, 1/Z)
		void reciprocal();
		
		//!Perform a 3x3 matrix transformation using a GSL matrix 
		void transform3x3(const gsl_matrix *matrix);

		//!Perform a cross-product based orthogonalisation with the specified vector
		bool orthogonalise(const Point3D &p);


		//!find the centroid of a set of points
		static void getCentroid(const std::vector<Point3D> &pts, Point3D &p);

		//!Assign the vector using spherical coordinates. 
		/*! theta - inclination (angle from +z). phi - azimuth (angle from +x), r - radius
		*
		* The value of the point is stored in cartesian coords.
		*/ 
		void setISOSpherical(float radius, float theta, float phi );
		
		//!Get the Point3D value as spherical coordinates (ISO 30-11)
		/*!Internal cartesian representation converted to ISO 30-11 and 
		* returned in the pointers provided as input.
		*
		* theta - inclination (angle from +z). phi - azimuth (angle from +x), r - radius
		*/
		void getISOSpherical(float &radius, float &theta, float &phi ) const;


#ifdef __LITTLE_ENDIAN__
		//!Flip the endian state for data stored in this point
		void switchEndian();
#endif
};
#ifdef DEBUG
bool testPoint3D();
#endif
}
#endif
