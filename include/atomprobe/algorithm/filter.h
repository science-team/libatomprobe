/* filter.h: mass spectrum candidate filtering
 * Copyright (C) 2020  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_MASS__FILTER_H
#define ATOMPROBE_MASS__FILTER_H

#include <vector>
#include <utility>

#include "atomprobe/isotopes/abundance.h"
#include "atomprobe/algorithm/massTool.h"

namespace AtomProbe
{

//! Remove peaks for which an experimental set of observed peaks do not show
//! a peak at a position which is expected from the peak distribution for this
//! element. Only works with single-atom peaks
/*!- Solutions are the input solutions, 
 * - peakData is a list of peak mass positions 
 * - tolerance is the error allowed to fit a peak (+/-)
 * - solutionCharge - charge state for the input isotope entries 
*/
void filterPeakNeedBiggerObs(const AtomProbe::AbundanceData &massTable,
				const std::vector<float> &peakData, float tolerance,
				size_t solutionCharge, std::vector<std::vector<AtomProbe::ISOTOPE_ENTRY> > &solutions);


//!Use the maximum possible PPM for each isotopic combination to filter possible solutions
/*!For example, 17^O:17^O:17^O in a naturally abundant mix has a  
*  maximum possible concentration of 5e-11 at. fraction.  
* Isotope group tolerance is used to  group the abundance distribution
* of nearby isotopes
* 
* massTable - a table of natural abundances
* minPpm - Minimum PPM below which to discard solutions
* solutions - list of solutions, which will be culled. 
*/
void filterBySolutionPPM(const AtomProbe::AbundanceData &massTable, 
		float minPpm, std::vector<std::vector<AtomProbe::ISOTOPE_ENTRY> > &solutions);

//!Compute the fraction of the data that has been explained, using the natural abundance information, and intensity data
/*!Specifically, this returns the maximum possible fraction of this peak 
 * that is explained by the isotopic fingerprint for a given solution set, and the count data given.
 * This is returned as a vector with a value for each proposed solution
 * Note the solutions do not need to sum to 1
 *
 * massData - list of location and intensities for peaks in dataset
 * peakMass - peak we wish to query. Must be present (within tolerance) in peak list (massData)
 * massWidth - tolerance width (+-) when locating peaks
 * solutions - list of candidate solutions we want to find the explained fraction for
 * massDistTol - tolerance to use when checking for peaks in mass distribution
 * solutionCharge - charge state to apply to isotope distribuion
 */
std::vector<float> maxExplainedFraction(const std::vector<std::pair<float,float> > &massData, 
	float peakMass, float massWidth, const std::vector<std::vector<AtomProbe::ISOTOPE_ENTRY> > &solutions, 
			const AtomProbe::AbundanceData &massTable, float massDistTol, unsigned int solutionCharge);
#ifdef DEBUG
	bool isotopeFilterTests();
#endif
}
#endif
