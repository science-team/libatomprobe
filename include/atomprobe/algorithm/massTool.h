/* 
 * massTool.h : Copyright (C) 2017  D Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef ATOMPROBE_MASSTOOL_H
#define ATOMPROBE_MASSTOOL_H

#include <cstdlib>
#include <vector>
#include <map>
#include <utility>
namespace AtomProbe {

//!Placeholder class for containing input weights for MassTool
class Weight
{
	public:
		Weight(){};
		Weight(float m, size_t uniqId=0) { mass=m; uniqueId = uniqId;}

		float mass; //The effective mass (might be normalised by charge state) for the element we wish to search for
		size_t uniqueId; //Optional unique identifier, to allow disambiguation of objects with same masses (but maybe later different solution types)

		bool operator==(const Weight &b) const { return (mass == b.mass && uniqueId == b.uniqueId);}
		bool operator<(const Weight &b) const { return (mass < b.mass);} 
};

//!Class that brute-force solves the knapsack problem
class MassTool
{
	private:
		//!Pre-process weight std::vector to remove any weights that cannot form
		// part of the solution. Speed-up only
		static void preprocessKnapsack(std::vector<Weight> &weights, 
			float totalWeight, float tolerance, unsigned int maxCombine);
	public:


		//!Depth-First-Search based brute force solve the knapsack problem,
		/*!Record all solutions within "tolerance" of the value "targetWeight".
		 * Solutions are reported as std::vectors of weights. All solutions are
		 * ensured unique.
		 * 
		 * "weights" vector should contain the masses of the input object. Note that the elemIdx and isotopeIdx values in the Weight
		 * structure are used for tracking purposes only. Elements may be removed from this input vector by MassTool.
		 *
		 * targetWeight is the total mass you want to find out what combination of weights matches.
		 * tolerance is the mass error from targetweight (+-tolerance) you are willing to accept
		 * maxObjects is the maximum number of weights you can combine to get the solution
		 */
		static void bruteKnapsack(std::vector<Weight> &weights, 
				float targetWeight, float tolerance, 
				unsigned int maxObjects, std::vector<std::vector<Weight> > &solutions);


		//!Multiple-target version of brute knapsack. 
		/* ! This version has parameters the same as the single-target bruteKnapsack algorithm
		 * with the exception of targetWeight, which is now a vector of targets
		 *
		 * The solutions will be returned all together, and the caller must separate them.
		 *
		 * The efficiency (speed) gains of this function are
		 * not fully clear, as internal pre-processing  will be
		 * less aggressive. However, only a single tree search
		 * will be performed.
		 */
		static void bruteKnapsack(std::vector<Weight> &weights, 
				const std::vector<float> &targetWeight, float tolerance, 
				unsigned int maxObjects, std::vector<std::vector<Weight> > &solutions);

#ifdef DEBUG
		static bool runUnitTests();
#endif
};

}
#endif
