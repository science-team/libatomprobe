/* axialdf.h : Axial distribution function routines
 * Copyright (C) 2014  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_AXIALDF_H
#define ATOMPROBE_AXIALDF_H


#include "atomprobe/algorithm/K3DTree-exact.h"
#include "atomprobe/helper/progress.h"

namespace AtomProbe
{

//!Generate a 1D axial distribution function, 
/*! This generates a histogram of point-point distances, from the points
 in the point list to the points in the tree, within a specified radius of
 the source point. 

 This is variously called a 1D "Spatial Distribution
 Map (SDM)" "Atom Vicinity" or similar 

 - pointList : the source points for the distribution function
 - tree : a pre-generated KD tree with the target points
 - distMax : radius of the points around which to search
 - histogram : a vector with the number of bins that are required for output. Does not need to be zero filled
 */
unsigned int generate1DAxialDistHist(const std::vector<Point3D> &pointList, K3DTreeExact &tree,
		const Point3D &axisDir, float distMax, std::vector<unsigned int> &histogram) ;


//FIXME: Internal code seems to be a bit unhelpful.
//!Generate a series of 1D distribution functions, one per pixel in a 2D grid of spherical coordinate directions
/*!Using a stepped spherical coordinates in equal delta-Theta, delta-Phi increments
* pointList : the source points from which to search
* tree : K3D Tree of target points
* distMax : maximum distance around which to search
* dTheta : step size in theta (radiians).
* dPhi : step size in phi (radiians)
* prog : Progress bar
* histogram : resultant data as a 2D vector, one bin per angular entry. Each vector will have the same size. The inner most vector contains the analysis data, over +-distMax
*/
unsigned int generate1DAxialDistHistSweep(const std::vector<Point3D> &pointList, K3DTreeExact &tree,
			float distMax, float dTheta, float dPhi, AtomProbe::ProgressBar &prog,
			std::vector<std::vector<std::vector<unsigned int> > > &histogram);
}
#endif
