/*
 * componentAnalysis.h : Analysis of atomstic components (?)
 * Copyright (C) 2018  D Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "atomprobe/io/multiRange.h"

namespace AtomProbe
{

struct OVERLAP_PROBLEM_SETTINGS {
	//!Tolerance in Da, above which  we consider problems to be 
	// linked to one another 
	float massTolerance;

	//!The minimal natural abundance for which we consider the ions
	// in ionic fraction
	float intensityTolerance;

	//!Maximum charge state to consider 
	unsigned int maxDefaultCharge;
};

//TODO: An adjacency matrix still needs to be traversed to generate a
// connectivity path between the nodes.

//!Calculate the adjacency matrix for the ions in a multirange
/*! Uses tolerance and other data in settings.  Range data is NOT used.
* Vectors and matrices should not be allocated, but must be freed using
* gsl_matrix_free and gsl_vector_free after use.
* Return values are a 0/1 adjacency matrix, and two vectors, one
* describing the owner index of each ion and the other the mass.
* return matrix will be square, and vectors will have same size as matrix
*/ 
void computeIonDistAdjacency(const MultiRange &mrf, const AbundanceData &abundance, const OVERLAP_PROBLEM_SETTINGS &settings, gsl_matrix* &m,
	gsl_vector* &vOwnership, gsl_vector* &vMass);


//!Calculate the adjacency matrix for the ranges 
//void computeRange

#ifdef DEBUG	
bool testComponentAnalysis();

#endif
}
