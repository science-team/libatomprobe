/* convexHull.cpp: Wrapper for convex hull calculations
 * Copyright (C) 2020  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_CONVEX_HULL_H
#define ATOMPROBE_CONVEX_HULL_H

#include <vector>

#include "atomprobe/apt_config.h"
#include "atomprobe/primitives/ionHit.h"

#ifdef HAVE_QHULL

namespace AtomProbe{
//Error codes
enum
{
	HULL_SURFREDUCE_NEGATIVE_SCALE_FACT=1,
	HULL_ERR_USER_ABORT,
	HULL_ERR_NO_MEM,
};


//!Obtain a set of points that are a subset of points the convex hull that are at least "reductionDim" away from the hull itself
/*Returns 0 on success,  nonzero on error, with error codes in above enum

  FIXME: Currently the algorithm is over-agressive and reduces by more
  than neccesary, some times a lot.  It should be replaced with a more
  correct version
*/
unsigned int GetReducedHullPts(const std::vector<IonHit> &points, float reductionDim,  
		unsigned int *progress, bool (*callback)(bool), std::vector<IonHit> &pointResult);

//!Obtain the convex hull of a set of ions. 
/*!An existing set of points on the convex hull, "curHull", can be given
 * as a starting set, optionally. For the last call to this routine ,
 * set "freeHull" to true.  
 * Returns 0 on success,  nonzero on error,
 * with error codes in above enum
*/
unsigned int computeConvexHull(const std::vector<IonHit> &data, unsigned int *progress,
			bool (*callback)(bool),std::vector<Point3D> &curHull, bool freeHull);


#ifdef DEBUG
bool testConvexHull();
#endif

}

#endif

#endif
