/* 
 * histogram.h : atom probe data histogramming header
 * Copyright (C) 2015  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATOMPROBE_HISTOGRAM_H
#define ATOMPROBE_HISTOGRAM_H
#include <vector>

#include "atomprobe/primitives/ionHit.h"
#include "atomprobe/helper/aptAssert.h"

#include <gsl/gsl_histogram.h>
#include <limits>

namespace AtomProbe {

//!Generates an ion "correlation histogram" from a vector of EPOS ions. The input vector *MUST*
// be in the correct ordering, usually as-loaded from file, (ie hitMultiplicity must be set correctly).
/*!	The nature of the plot is described in the following paper (Saxey et al, 2011): 
	DOI : 10.1016/j.ultramic.2010.11.021, see e.g, Figure 3a.

	* The histogram will be allocated for you - do not pre-allocate

	* As the plot is symmetric around the m1=m2 line, the lowerTriangular setting
	controls if mirroring of the calculation is done. Setting this to true will
	ensure that only the lower half of the data field is calculated (faster).

	* StepMass > 0
	* EndMass > 0

	in both cases, the histogram will use the full rectangular memory space
	(arrays will not be ragged)
*/
bool correlationHistogram(const std::vector<EPOS_ENTRY> &eposIons,
		std::vector<std::vector<unsigned int> > &histogram,float stepMass,
		float endMass, bool lowerTriangular=false);	

//!Increments a correlation histogram, as per correlationHistogram,
// using an incomplete vector of EPOS entries. Note that EPOS entries
// that lead in with a zero deltapulse will be ignored.
/*!
	For input parameters, see correlationHistogram(...). This
	version will have lower peak memory input, as eposIons can be chunked.
	- The input histogram must be zero sized, or have the correct size when
	  used. If zero sized, it will be resized and zeroed appropriately to contents

	- The default here is that the lower triangular is true, unlike
	  in correlationHistogram(...) so when performing the final
	  accumulation, you may wish to set this to false, to ensure
	  that histogram_ij = histogram_ji.

	- Do not change endMass or stepMass between calls on the same histogram
*/
bool accumulateCorrelationHistogram(const std::vector<EPOS_ENTRY> &eposIons,
		std::vector<std::vector<unsigned int> > &histogram, float stepMass,
		float endMass, bool lowerTriangular=true);

//!Make a linearly spaced histogram with the given spacings
/*! Takes a vector of floats as input, \param data,
 * histVals will be sized as needed
*/
template<class T>
void linearHistogram(const std::vector<T> &data, T start,
		    T end, T step, std::vector<T> &histVals)
{
    ASSERT(start < end);
    ASSERT(step > std::numeric_limits<T>::epsilon());

    gsl_histogram *h = gsl_histogram_alloc((end-start)/step);
    gsl_histogram_set_ranges_uniform(h,start,end);

    for(size_t ui=0; ui<data.size();ui++)
        gsl_histogram_increment(h,data[ui]);

    //Copy out data
    histVals.resize(h->n);
#pragma omp parallel for
    for(size_t ui=0;ui<h->n; ui++)
        histVals[ui]=h->bin[ui];

    gsl_histogram_free(h);
}

}

#endif
