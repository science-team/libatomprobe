/* rangeCheck.h : Rangefile correctness checking routines 
 * Copyright (C) 2014  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_RANGECHECK_H
#define ATOMPROBE_RANGECHECK_H 

#include "atomprobe/isotopes/abundance.h"
#include "atomprobe/io/ranges.h"

#include <vector>

namespace AtomProbe
{

//!Ensure that each mass given spans a peak that should exist
/*! Each entry in badRanges is true if an inconsistent range is detected.
 There will be one entry per range in the parent rangefile
 Some ranges may be skipped if they cannot be understood
 maxComponents will skip checks for ions with more than this many
  atoms in the ionic molecule. Eg H2 would not be checked if maxComponents=1
*/
void checkMassRangingCorrectness(const AtomProbe::RangeFile &rng,AtomProbe::AbundanceData &massTable,
		float massTolerance, unsigned int maxChargeState, 
		unsigned int maxComponents,std::vector<bool> &badRanges);

#ifdef DEBUG
bool testRangeChecking();
#endif
}


#endif
