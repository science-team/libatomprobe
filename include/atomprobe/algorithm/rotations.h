/* spatial.h:  Common spatial manipulation algorithms
 * Copyright (C) 2015  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_SPATIAL_H
#define ATOMPROBE_SPATIAL_H

#include "atomprobe/primitives/point3D.h"

#include <gsl/gsl_matrix.h>
#include <vector>

namespace AtomProbe
{
//!Given two orthogonal and normal basis vectors, r1 and r2, and two matching rotated vectors
//!ur1 and ur2, compute the rotation matrix that will transform between them.
/*! i.e. r = M*ur, vector pairs (ur1,ur2) and (r1,r2) should be orthogonal and normalised
 matrix m must be pre-allocated as a 3x3 matrix, using e.g gsl_matrix_alloc(3,3)
 */
void computeRotationMatrix(const Point3D &ur1, const Point3D &ur2,
	const Point3D &r1, const Point3D &r2, gsl_matrix *m);

//!Convenience wrapper for conputeRotationMatrix, which uses vector containers. 
/*! output vectors will be 3x3, indexed vector[row][column]. Presizing is not required
*/ 
void computeRotationMatrix(const Point3D &ur1, const Point3D &ur2,
	const Point3D &r1, const Point3D &r2, std::vector<std:: vector<float> > &m);

//!Given a series of paired observations of directions, compute the least squares rotation matrix between them
/*! This computes the least-squares rotation matrix that transforms the
    unrotated points to  the rotated ones.
  * rotated points and unrotated points must be the same size, must
  have >=2 entries, and must not be degenerate.  Each point must be
  normalised. 
  * Weights is a series of weighting factor (e.g. inverse
  error) for each point.  
  * If R is nullptr on input, then it will be replaced with a matrix
  allocated by gsl_matrix_alloc(). The caller should deallocate this
  with gsl_matrix_free() after use.
*/
unsigned int computeRotationMatrixWahba(const std::vector<Point3D> &unrotated,const std::vector<Point3D> &rotated, 
		 const std::vector<float> &weights,gsl_matrix* &R);
#ifdef DEBUG

//Run internal unit tests for rotation algorithms
bool testRotationAlgorithms();
#endif

}
#endif

