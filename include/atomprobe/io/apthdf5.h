#ifndef LIBATOMPROBE_APT_HDF5
#define LIBATOMPROBE_APT_HDF5

#include "atomprobe/apt_config.h"

//Only enable if HDF5 was made available at compile time
#ifdef HAVE_HDF5
#ifdef EXPERIMENTAL

#include <hdf5/serial/H5Cpp.h>
#include <hdf5/serial/H5LTpublic.h>

#include <vector>
#include <string>
#include <utility>
#include <cassert>
#include <cinttypes>

//Allow std::variant access, if supported
#if __cplusplus >=201703L
#include <variant>
#endif

namespace AtomProbe {

#if __cplusplus >=201703L
typedef std::variant<double,std::string,uint64_t> APT_H5_VARIANT;
#endif

//error codes
enum
{
	ERR_HDF_EXCEPTION=1, // Some unspecified exception occured
	ERR_NOT_HDF_FILE, //File is not a valid HDF5 file
};

//This class is a wrapper for HDF5 reading/writing without 
// any streaming capacity; this abstracts away
// several of the data retrieval operations, as well as providing
// some simple validity checks specific to the APTHDF5 standard
// advanced users may wish to use H5File directly.
class APTHDF5
{
	private:
		//!Backend accessor object
		H5::H5File *file;

		//!Block based data writing utility
		template<class T>
		void writeDataToGroup(const std::string &datasetName, const T* data,
                        unsigned int rows, unsigned int cols, bool compression=true);

		//!Obtain the internal HDF predicate type  
		H5::PredType getPredType(const double *) const { return H5::PredType::IEEE_F64LE;}
		H5::PredType getPredType(const unsigned int *) const { return H5::PredType::STD_U64LE;}

		//!Check field sizes from each of the given dataset paths match, returns false on fail, with mismatch identified by index
		bool validateFieldSizesMatch(const std::vector<std::string> &paths, unsigned int &firstMismatch);

		//!Validate the contents of the given fields, e.g. by data type. Returns false on failure
		bool validateFieldContents(std::vector<std::string> &datasets) ;
		//!Fetch data from the given dataset chunk-wise, up to n items. Returns false on failure
		bool fetch1DSelection(H5::DataSet &ds, unsigned int offset, 
			unsigned int n, std::vector<double> &data);

		//!Get dataset parameters, up to 2D. count should be of size 2
		/*! dimsData, slabspace, memspace count and dims are output parameters
		 */
		bool getDatasetParams( const char *groupName, unsigned int &dimsData,H5::DataSet &ds,
				H5::DataSpace &slabSpace, H5::DataSpace* &memspace, hsize_t* &count, hsize_t* &dims) const;

	public:
		APTHDF5();
		~APTHDF5();
		//!Open a file, given a filename. Optional flag is H5F_ACC access pattern flag
		/* Valid flags include H5F_ACC_CREAT, H5F_ACC_TRUNC, H5F_ACC_RDONLY, H5F_ACC_RDWR; see H5FPublic.h
		 *  in HDF library for full details
		 */
		unsigned int open(const char *filename, unsigned int flag);

		//!Return the internal HDF5 file object, life is liked to class lifetime
		H5::H5File *getFilePtr();

		//!Return true if the file is a valid APTHDF5 file
		/*!version number should be  e.g. 2020101, for (Month 10 : Year 20 : Revision 1 - if no revision, use 0);
		 Note : this is best effort ; validation should not be taken as a true measure of standards compliance
		 however, the code will perform multiple checks
		 */
		bool isValid(unsigned int minVersion=2020010);

		//!Return true if a given group is available or not; 
		/*!Needs to specify full path
		 * using forward slash as separator, e.g. "/some_group/sub/whateverData"
		 * (unsigned int)-1 : error
		 * 0 : dataset not found
		 * 1 : dataset found
		 */
		unsigned int hasDataset(const char *datasetName) const;

		//!Enumerate the datasets present in the file
		/*!First entry in pair is name of group, second is data type
		 */
		bool enumerateDatasets(std::vector<std::pair<std::string, unsigned int> > &dataSets) const;

		//!Obtain the datatype of a given group
		/*!returned value is HDF type (dataset identifier), eg H5T_INTEGER, H5T_FLOAT or H5T_STRING
		 (unsigned int)-1 if group does not exist, or an error was encountered
		 */
		unsigned int getGroupDataType(const char *groupName) const;

		//!Obtain the dimensions of a given group
		std::vector<unsigned int> getDimensions(const char *groupName) const;

		//!Obtain string data from a group; assumption is that there is a single string (1x1) in the dataset
		bool getStringData(const char *groupName, std::string &data) const;

		//!Obtain real data from a group, up to 2D
		bool getRealData(const char *groupName,std::vector<std::vector<float> > &data) const;

		//!Obtain unsigned integer data from a group, up to 2D; 64-bit
		bool getUIntegerData(const char *groupName,std::vector<std::vector<uint64_t> > &data) const;

		//!Obtain integer data from a group, up to 2D, 64-bit
		bool getIntegerData(const char *groupName,std::vector<std::vector<int64_t> > &data) const;

#if __cplusplus >=201703L
		//!Obtain unsigned integer data from a group
		unsigned int getUIntegerData(const char *groupName, std::vector<std::vector<APT_H5_VARIANT> > &data ) const;

		//!Obtain data associated with a given group, up to 2D, using a variant datatype
		unsigned int getData(const char *datasetName,std::vector<std::vector<APT_H5_VARIANT> > ) const;
#endif
		//!Clear and write data to the specified group (1D array, mx1)
		/* Note that the APTHDF5 instance must be in read/write mode, not readonly
		 */
		template<class T>
		bool writeData(const std::vector<T> &data, const char *groupName);

		//!Write single string to file, under the given group name
		bool writeData(const std::string &data, const char *groupName);


};

// Various notes: closing the HDF file doesn't close it if all objects on the file were not closed
// This can be tuned via H5Pset_fclose_degree, to make it more sensible (close objects on file close)

template<class T>
void APTHDF5::writeDataToGroup(const std::string &datasetName, const T* data,
                        unsigned int rows, unsigned int cols, bool compression)
{
	assert(file);

        hsize_t dSize[] ={
                rows,cols };
	H5::DataSpace dataSpace(2,dSize);


	H5::DataSet *dData;
	hid_t predTypeValue;
	
        if(!compression)
	{
		dData= new H5::DataSet;
		*dData=file->createDataSet(datasetName.c_str(),getPredType(data),dataSpace);
	}
        else
        {
                //Set up the compression settings
                hsize_t chunkSize[2] = {
                        100,100};
                chunkSize[0]=std::min(chunkSize[0],(hsize_t)rows);
                chunkSize[1]=std::min(chunkSize[1],(hsize_t)cols);

		H5::DSetCreatPropList pList;
                pList.setChunk( 2, chunkSize);
                pList.setDeflate( 6 );

                dData= new H5::DataSet;
		*dData=	file->createDataSet(datasetName.c_str(),getPredType(data),dataSpace,pList);

        }


	hid_t dataType;
	if(typeid(T) == typeid(unsigned int))
	{
		dataType = H5T_NATIVE_UINT;
		
	}
	else if (typeid(T) == typeid(float))
	{
		dataType = H5T_NATIVE_FLOAT;
	}
	else if (typeid(T) == typeid(double))
	{
		dataType = H5T_NATIVE_DOUBLE;
	}
	else
	{
		//Unsupported
		assert(false);
	}
	H5Dwrite(dData->getId(), dataType,
			dataSpace.getId(), H5S_ALL, H5P_DEFAULT, data);

	delete dData;
}

template<class T>
bool APTHDF5::writeData(const std::vector<T> &data, const char *groupName)
{
	if(!data.size() || !groupName)
		return false;

	
	try
	{
		writeDataToGroup(groupName,&(data[0]),data.size(),1,false);
	}
	catch(std::exception &e)
	{
		return false;
	}

	return true;
}

#ifdef DEBUG
bool runAPTHDF5Tests();
#endif

}
#endif //EXPERIMENTAL

#endif //HAVE_HDF5

#endif //INCLUDE_GUARD
