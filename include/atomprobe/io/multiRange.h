/*
 * multiRange.h - Atom probe rangefile class 
 * Copyright (C) 2014  D Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATOMPROBE_MULTIRANGE_H
#define ATOMPROBE_MULTIRANGE_H

#include <vector>
#include <set>
#include <string>
#include <map>

#include "atomprobe/apt_config.h"

#include "atomprobe/io/ranges.h"

#include "atomprobe/helper/misc.h"
#include "atomprobe/primitives/ionHit.h"
#include "atomprobe/isotopes/abundance.h"

namespace AtomProbe
{
struct SIMPLE_SPECIES
{
	//!Number of protons (element number)
	unsigned int atomicNumber;

	//!Number of copies of this isotope
	unsigned int count;

	bool operator==(const SIMPLE_SPECIES &oth) const;
};

//!Structure that allows for the multirange data to be mapped into
// a 1D mass axis
struct FLATTENED_RANGE
{
	//Start and end of the "flattened" range
	float startMass,endMass;
	//The ionIDs from parent multirange that are contained between start and end mass
	std::vector<unsigned int> containedIonIDs, containedRangeIDs;
};

bool operator<(const SIMPLE_SPECIES &a, const SIMPLE_SPECIES &b);

enum
{
	MULTIRANGE_FORMAT_XML,
};

//!Data storage and retrieval class for "ranging" a spectra, where overlapping ranges are permitted
class MultiRange
{
	private:
		//!The human-readable names of each species
		std::vector<std::string> ionNames;

		//!The ionic data for each species (atomic number, elemental number)
		/*!Internal vector, vector<SIMPLE_SPECIES>, is the ionic molecule
		* internal vector can be empty for unknown molecule
		*/
		std::vector<std::set<SIMPLE_SPECIES> > moleculeData;	

		//!A colour that describes each species
		std::vector<RGBf> colours;
	

		//!This holds the min and max masses for the range, one per range
		std::vector<std::pair<float,float> > ranges;
		
		//!If present (optional), the groupings for the ranges (these ranges are all group 1, etc). 
		// Typically ions that are linked by isotope fingerprint share a grouping identifier.
		// This might be used e.g. for decomposition/deconvolution
		std::vector<unsigned int> rangeGrouping;
		
		//!The ion ID number for each range 
		std::vector<unsigned int> ionIDs;
		

		//!Error state of last operation
		unsigned int errState;
	

	public:
		MultiRange();

		//!Do our best to initialise from a rangefile
		/*! ions and their child ranges may be dropped if they cannot be identified
		 */
		MultiRange(const AtomProbe::RangeFile &rng, const AtomProbe::AbundanceData &natData);

		//!Check for equality with other multirange
		bool operator==(const MultiRange &oth) const;
	
		//!Add the ion to the database returns ion ID if successful, -1 otherwise
		unsigned int addIon(const std::set<SIMPLE_SPECIES> &molecule, 
				 const std::string &name, const RGBf &ionCol);

	
		//!Add a simple ion to the database -  returns ion ID if successful, -1 otherwise
		unsigned int addIon(const SIMPLE_SPECIES &molecule, 
				 const std::string &name, const RGBf &ionCol);

                //!Remove an ion from the multirange - also removes all associated ranges
                void removeIon(unsigned int ionID);


                //!Remove multiple ions from the multirange - also removes all associated ranges
                /*! Each element must be unique, and must not exceed maximum ionID.
                 * Vector will be sorted
                 */
                void removeIons(std::vector<unsigned int > &v);
		
		//!Add a range to the rangefile. Returns ID number of added range if adding successful, (unsigned int)-1 otherwise.
		/*!Note that ranges are, unlike RangeFile, allowed to
		   overlap, and still be self consistent.
		 */
		unsigned int addRange(float start, float end, unsigned int ionID);
		
		//!Convenience wrapper for addRange(float,float, unsigned int)
		unsigned int addRange(const std::pair<float, float> &rng, unsigned int ionID);

		//!Set the groupings for the ranges : this is used when splitting overlapping ranges
		/*! When splitting overlapping ranges, "grouped" ranges are considered to be part of the same 
		* split grouping. This is typically used for example, to group isotopes that belong to an ion, 
		* but of differing charge state. As the intensities for charge state in an experiment are not linked
		* There is no need to link these in the range group.
		* Each range with a shared integer belongs to the same group
		* -  the input vector must match range size, or be empty.
		* - range size cannot be changed without changing grouping vector
		*/ 
		void setRangeGroups(const std::vector<unsigned int> &groups);

#ifdef HAVE_LIBXML2

		//!Save the structure to a file. format specifies output file format type.
		/*!Returns true if successful, otherwise false
		*/
		bool write(const char *fileName, unsigned int format=MULTIRANGE_FORMAT_XML) const;

		//!read the contents of the file into class
		/*!returns true if successful, otherwise false
		* - if not successful then the errState will contain the 
		*  error information, and the calling object (rangefile) will be in an invalid state
		*/ 
		bool open(const char *fileName, unsigned int format=-1);

#endif

		//!Get the ion's ID from a specified mass
		/*! Returns the ions ID if there exists a range that 
		 * contains this mass. Otherwise (unsigned int)-1 is returned
		 */
		unsigned int getIonID(unsigned int rangeId) const;
		
		//!Set the ion ID for a given range
		void setIonID(unsigned int range, unsigned int newIonId);

                //!Obtain the ionIDs that match a given SIMPLE_SPECIES
                void getIonIDs(const std::set<SIMPLE_SPECIES> &s, std::vector<unsigned int> &ionIDs) const;

		//!Obtain the colour for a given ion
		RGBf getColour(unsigned int ionID) const;
		//!Set the colour using the ion ID
		void setColour(unsigned int ionID, const RGBf &r);

		//!Retrieve the human-readable error associated with the current range file state
		std::string getErrString() const;
		
		//!Get the name of a specified ionID
		std::string getIonName(unsigned int ionID) const;

                //!Set the ion name
                void setIonName(unsigned int ionID, const std::string &s);
	
		//!Get the number of unique ranges
		unsigned int getNumRanges() const;

		//!Get the number of ranges for a given ion ID
		unsigned int getNumRanges(unsigned int ionID) const;

		//!Get the number of unique ions
		unsigned int getNumIons() const;

		//!Return the molecule that is associated with this ion
		std::set<SIMPLE_SPECIES> getMolecule(unsigned int ionID) const;
		
                //!Return the molecule that is associated with this ion
		void setMolecule(unsigned int ionID,const std::set<SIMPLE_SPECIES>&s );

                //!Convert the molecule into a string representation, with vertical bar separator e.g "Fe1" or "Fe1|O|H2"
                static std::string moleculeString(const std::set<SIMPLE_SPECIES> &mol, const AtomProbe::AbundanceData &natTable,bool &ok);

                //!Convert a molecule string (e.g. "Fe1|O2|H2" into its molecular form)
                static std::set<SIMPLE_SPECIES> moleculeFromString(const std::string &s, const AtomProbe::AbundanceData &natTable, bool &ok);

                //!Convert a molecule string into a somewhat more human readable form, e.g. "Fe1|O2|H2" would become "FeO2H2"
                static std::string nameFromMoleculeString(const std::string &s);

		//!Retrieve the start and end of a given range as a pair(start,end)
		std::pair<float,float> getRange(unsigned int rangeID) const;

		//!Retrieve all of the ranges that specify the given mass
		std::vector<unsigned int> getRangesFromMass(float mass) const;
		
                //!Retrieve all of the ranges that specify the given mass
		std::vector<unsigned int> getIonRanges(unsigned int ionID) const;

                //!Change a given range's values
                void setRange(unsigned int rangeId, float lowMass, float highMass);

                //!Change ranges mass values. If inverted, (first<second), then this will be flipped
                void setRange(unsigned int rangeId, const std::pair<float,float> &massPair);

		//!Set the lower bound of the given range
		bool setRangeStart(unsigned int rangeID, float v);

		//!Set the upper bound of the given range
		bool setRangeEnd(unsigned int rangeID, float v);

                //!Remove a range from the multirange;
                /*! This causes all rangeIDs to be moved around a bit, so any
                 * rangeIDs stored externally will be no longer mapped correctly
                 */
                void removeRange(unsigned int rangeId);

                //!remove multiple ranges from multirange
                /*! rangeIDs will be moved around, so any external rangeIds are invalidated
                 * - Max value of input must be < number of ranges
                 */
                void removeRanges(const std::vector<unsigned int> &ranges);
	
		//!Returns true if a specified mass is ranged
		bool isRanged(float mass) const;
		//! Returns true if an ion is ranged
		bool isRanged(const IonHit &) const;

		//!Clips out ions that are not inside the rangefile's ranges
		void range(std::vector<IonHit> &ionHits) const;

		//!Check to see if data structure is internally consistent
		bool isSelfConsistent() const;

		//!Erase the contents of the rangefile
		void clear();


		//!Obtain a projection onto the mass axis of ranges that do not touch one another
		// - each ion that is in the range is listed
		// - Tolerance is the tolerance in both the + and - directions to extend ranges when considering 
		//   the existence of overlaps
		void flattenToMassAxis(std::vector<FLATTENED_RANGE> &ionMapping,float tolerance=0) const;

                //!Generate a rangefile from the multirange, uses flattenToMassAxis internally
                // - returns false if it is not possble to to this
                bool flattenToRangeFile(const AtomProbe::AbundanceData &natTable,
                            RangeFile &range, float tolernace=0) const;

		//!Separate out the non-interacting parts of the multi-ranges into their own multiRange entries
		//!The vector of maps converts the ionids of the child MultiRange to the parent
		// This will only consider the ranges that exist in the multirange file
		// massTolerance : used to determine overlapping groups, and is +- the range bounds 
		void splitOverlapping(std::vector<MultiRange> &decomposedRanges, float massTolerance=0) const;
		
		
		//!Copy range from a different multirange into this one, including all dependant data
		// such as parent ion, molecule formation data, representation etc.
		// Does not copy group data, as this makes no sense, instead range group data is erased if present
		void copyDataFromRange(const MultiRange &src,unsigned int srcRngId);

		//!Guess the charge state for the ion that corresponds to the given range's midpoint
		// returns false if unable to guess the charge state within tolerances
		bool guessChargeState(unsigned int rangeId,
				      const AtomProbe::AbundanceData &massTable, unsigned int &charge,
				      float tolerance=0.5f) const;


                //!For any ranges that do not have molecule information, attempt to fill it in from the range information
                // this is entirely heuristic, and may do the wrong thing
                void guessMoleculeData(const AtomProbe::AbundanceData &abundance);
#ifdef DEBUG
		//!Test the overlap-splitting code, requires abundance information
		static bool testSplit(const AtomProbe::AbundanceData &abundance);
	
		static bool test();
#endif
};

}
#endif
