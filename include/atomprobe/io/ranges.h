/*
 * ranges.h - Atom probe rangefile class 
 * Copyright (C) 2018  D Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATOMPROBE_APTRANGES_H
#define ATOMPROBE_APTRANGES_H

#include <vector>
#include <string>
#include <map>

#include "atomprobe/primitives/ionHit.h"
#include "atomprobe/isotopes/abundance.h"
#include "atomprobe/helper/misc.h"

namespace AtomProbe
{


//Number of elements stored in the table
const unsigned int NUM_ELEMENTS=119;

enum{ RANGE_FORMAT_ORNL,
	RANGE_FORMAT_DBL_ORNL,
	RANGE_FORMAT_ENV,
	RANGE_FORMAT_RRNG,
	RANGE_FORMAT_END_OF_ENUM //not a format, just end of enumeration.
};

enum
{
	RANGE_ERR_FORMAT=1,
	RANGE_ERR_FORMAT_HEADER,
	RANGE_ERR_EMPTY,
	RANGE_ERR_FORMAT_LONGNAME,
	RANGE_ERR_FORMAT_SHORTNAME,
	RANGE_ERR_FORMAT_COLOUR,
	RANGE_ERR_FORMAT_TABLESEPARATOR,
	RANGE_ERR_FORMAT_RANGE_DUMMYCHARS,
	RANGE_ERR_FORMAT_MASS_PAIR,
	RANGE_ERR_FORMAT_TABLE_ENTRY,
	PARSE_RANGE_FORMAT_GENERIC_FAIL,
	RANGE_ERR_DATA_TOO_MANY_USELESS_RANGES,
	RANGE_ERR_DATA_FLIPPED,
	RANGE_ERR_DATA_INCONSISTENT,
	RANGE_ERR_DATA_NOMAPPED_IONNAME,
	RANGE_ERR_OPEN,	
	RANGE_ERR_FORMAT_RANGETABLE,	
	RANGE_ERR_FORMAT_TABLEHEADER_NUMIONS,
	RANGE_ERR_NONUNIQUE_POLYATOMIC,
	RANGE_ERR_TOO_LARGE,
	RANGE_ERR_DASHHEADER,
	RANGE_ERR_IONBLOCK_CONTENT,
	RANGE_ERR_NUMIONS,
	RANGE_ERR_NUMIONS_DUPLICATED,
	RANGE_ERR_TOO_MANYIONS,
	RANGE_ERR_ION_BLOCK_NOT_PRESENT,
	RANGE_ERR_DUPLICATE_NUMRANGES,
	RANGE_ERR_NUMRANGE_PARSE,
	RANGE_ERR_RRNG_IONS_TOOSHORT,
	RANGE_ERR_RRNG_COLON_SEPARATOR,
	RANGE_ERR_BADCOLOUR,
	RANGE_ERR_ION_NOT_MAPPED,
	RANGE_ERR_BADSTART,
	RANGE_ERR_BADEND,
	RANGE_ERR_NAME_EMPTY,
	RANGE_ERR_BAD_LINE_RANGEBLOCK,
	RANGE_ERR_MISSING_IONBLOCK,
	RANGE_ERR_NO_RANGES,
	RANGE_ERR_NO_BASIC_IONS,
	RANGE_ERR_MISMATCHED_NUMRANGES,
	RANGE_ERR_RANGEBLOCK_FORMAT,
	RANGE_ERR_BAD_MULTIPLICITY,
	RANGE_ERR_VOLUME_PARSE,
	RANGE_ERR_FORMAT_EMPTY_RANGEROW,
	RANGE_ERR_FILESIZE,
	RANGE_ERR_ENUM_END
};

//!Data storage and retrieval class for various range files
class RangeFile
{
	private:
		//!The names of each ion
		/*! The first element is the shortname for the ion
		  and the second is the full name !*/
		std::vector<std::pair<std::string,std::string> > ionNames;


		//! Ion formula (element count) information
		/*! This field stores formatted (element, count) formula pairs.
		 * For example the ion definition for H2O, the formula would be:
		 * {{H, 2}, {O, 1}}
		 * The order of the elements is alphabetic and each element is unique.
		 * !*/
		std::vector< std::map<std::string,size_t> > ionFormulas;


		//!This holds the colours for the ions, one per ion
		/*! Colours are defined by the RBGf class. \see{RBGf}*/
		std::vector<RGBf> colours;


		//!This holds the min and max masses for the range, one per range
		/*! Ranges define start and end points where any ions with a mass-to-charge
		 * that fall within this window are assigned a particular identity. The
		 * identity is determined by the corresponding ionIDs.
		 * Ranges are inclusive boundaries, defined by: lower <= X <= upper
		 * Retrive ranges using getRange.
		 * \see {RangeFile::ionIDs,RangeFile::getRange}
		!*/
		std::vector<std::pair<float,float> > ranges;


		//!The ion ID number for each range
		std::vector<unsigned int> ionIDs;


		//!volume information for range. Can either be empty (no information) or can contain zeros (no information, or no volume)
		std::vector<float> rangeVolumes;


		//!Error state of last operation
		/* Can get human readable error string with getErrString
		 * \see {RangeFile::getErrString(), RangeFile::getErrState()}
		*/
		unsigned int errState;


		//!Warning messages, used when loading rangefiles
		/* Vector of strings detailing potential problems.        */
		std::vector<std::string> warnMessages;




		//!Load an ORNL formatted "RNG" rangefile
		// caller must supply and release file pointer
		unsigned int openRNG(FILE *fp);


		//!Read the header section of an RNG file
		static unsigned int readRNGHeader(FILE *fpRange,
						  std::vector<std::pair<std::string,std::string> > &strNames,
						  std::vector<RGBf> &fileColours, unsigned int &numRanges,
						  unsigned int &numIons);


		//!Read the range frequency table
		static unsigned int readRNGFreqTable(FILE *fpRange, char *inBuffer,
						     const unsigned int numIons, const unsigned int numRanges,
						     const std::vector<std::pair<std::string,std::string> > &names,
						     std::vector<std::string> &colHeaders,
						     std::vector<unsigned int > &tableEntries,
						     std::vector<std::pair<float,float> > &massData,
						     std::vector<std::string> &warnings);

		unsigned int openDoubleRNG(FILE *fp);


		//!Load an RRNG file
		// caller must supply and release file pointer
		unsigned int openRRNG(FILE *fp);
		//!Load an ENV file
		// caller must supply and release file pointer
		unsigned int openENV(FILE *fp);

		//!Strip charge state from ENV ion names
		static std::string envDropChargeState(const std::string &strName);

		//!Should we enforce range consistency?
		static bool enforceConsistency;

	public:
		RangeFile();

		const RangeFile& operator=(const RangeFile &other);

                //!Erase the contents of the rangefile
		void clear();
		
                //!Open a specified range file - returns true on success
		bool open(const char *rangeFile);

		//!Open a specified range file using a given file format. Returns nonzero on failure.
		// - Can get human readable error string with getErrString
		unsigned int openFormat(const char *rangeFile, unsigned int format);

		//!is the extension string the same as that for a range file? I don't advocate this method, but it is convenient in a pinch.
		static bool extensionIsRange(const char *ext);
		//!Grab a vector that contains all the extensions that are valid for range files
		static void getAllExts(std::vector<std::string> &exts);

		//!Attempt to detect the file format of an unknown rangefile.
		/*! Returns one of RANGE_FORMAT_ enum value on success,
		    or RANGE_FORMAT_END_OF_ENUM on failure */
		static unsigned int detectFileType(const char *file);

		//!Return the human readable name for the given RANGE_FORMAT value
		static std::string rangeTypeString(unsigned int rangeType);

		//!Set whether the class will attempt to enfore consistency when running
		static void setEnforceConsistent(bool shouldEnforce=true) {
			enforceConsistency=shouldEnforce;
		}

		//!Performs checks for self consistency.
		/* Checks for ranges of zero width, overlapping ranges and duplicate ranges.
		 * Can be fixed with makeSelfConsistent.
		 * */
		bool isSelfConsistent() const;

		//!Performs checks for self consistency,  whilst providing information on the inconsistent components.
		bool isSelfConsistent(std::vector<std::string> &consistencyMessages) const;

		//!Modify range file to ensure self-consistency, by arbitrary heuristics.
		/*! Returns true if self consistant after corrections. false otherwise
		 */
		bool makeSelfConsistent();

		//!Retrieve the translated error associated with the current range file state
		std::string getErrString() const;

		//!Obtain the error state last set for this rangefile
		unsigned int getErrState() const { return errState; }

		//!Get the number of unique ranges
		unsigned int getNumRanges() const;
		//!Get the number of ranges for a given ion ID
		unsigned int getNumRanges(unsigned int ionID) const;
		//!Get the number of unique ions
		unsigned int getNumIons() const;

		//!Retrieve the start and end of a given range as a pair(start,end)
		std::pair<float,float> getRange(unsigned int ) const;

		//!Retrieve the start and end of a given range as a pair(start,end)
		std::pair<float,float> &getRangeByRef(unsigned int );

		//!Return true if we have range volume data, false otherwise
		bool haveRangeVolumes() const;

		//!Obtain an ions volume, if we have it. Zero may indicate no ion volume specfied for that range
		float getRangeVolume(unsigned int rangeId) const;
		//!Set the ion volume for a given range. If ion volumes are not tracked, these will be created
		void setRangeVolume(unsigned int rangeId, float newVolume) ;

		//!Retrieve a given colour from the ion ID
		RGBf getColour(unsigned int) const;
		//!Set the colour using the ion ID
		void setColour(unsigned int, const RGBf &r);


		//!Get the ion's ID from a specified mass
		/*! Returns the ion's ID if there exists a range that
		 * contains this mass. Otherwise (unsigned int)-1 is returned
		 */
		unsigned int getIonID(float mass) const;

		//!Get the ion's ID from a specified mass, using an IonHit
		/*! Returns the ion's ID if there exists a range that
		 * contains this mass. Otherwise (unsigned int)-1 is returned
		 */
		unsigned int getIonID(const IonHit &hit) const;
		//!Get the ion ID from a given range ID
		/*! No validation checks are performed outside debug mode. Ion
		 range *must* exist*/
		unsigned int getIonID(unsigned int range) const;

		//!Get the ion ID from its short or long name, returns -1 if name does not exist. Case must match
		unsigned int getIonID(const char *name, bool useShortName=true) const;
		unsigned int getIonID(const std::string &name) const {return getIonID(name.c_str());};

		//!Set the ion ID for a given range
		void setIonID(unsigned int range, unsigned int newIonId);



		// Ion formula get/set
		//! Get ion formula by ion ID
		/*! Returns the ion formula if there exists a range that
		 * contains this mass. If no range contains the ion hit then an empty map is returned:
		 * std::map<std::string, size_t>>
		 * The ion ID can be found using the getIonID method by mass, IonHit, name or range ID
		 !*/
		std::map<std::string,size_t> getIonFormula(unsigned int ionID) const;

		//! Set ion formula using ion short or long name (if it is already set)
		/*! The ion to change will be looked up using its short name (useShortName=true) or long name otherwise.
		 * The ion formula is automatically set using the short name of an ion
		 * when it is added by the addIon method. This method can be used to redefine the ion formulae.
		 * !*/
		void setIonFormula(const std::string &name, const std::map<std::string,size_t> &formula, bool useShortName=true);
		//! Set ion formula using ion ID
		/*! The ionID must be smaller than number of ions (e.g. from getNumIons() ) !*/
		void setIonFormula(const unsigned int ionID,const std::map<std::string,size_t> &formula);

		//! Use heuristics to guess missing formula data from the shortname
		/*! To overwrite existing formula data, set overwrite to true*/
		void guessFormulas(bool overwrite=false); 
		
		//!Erase any formula data
		void clearFormulas() { ionFormulas.clear();}; 

		//!Returns true if a specified mass is ranged
		bool isRanged(float mass) const;

		//! Returns true if an ion is ranged
		bool isRanged(const IonHit &) const;
		//!Clips out ions that are not inside the rangefile's ranges
		void range(std::vector<IonHit> &ionHits) const;

		//!Clips out ions that are inside the rangefile's ranges, if invert is false. If true, then reverse the set
		void rangeInvertable(std::vector<IonHit> &ionHits, bool invert);

		//!Clips out ions that don't match the specified ion name
		/*! Returns false if the ion name given doesn't match
		 *  any in the rangefile (case sensitive)
		 */
		bool range(std::vector<IonHit> &ionHits,
			   const std::string &shortIonName);

		//!Clips out ions that don't lie in the specified range number
		/*! Returns false if the range does not exist
		 *  any in the rangefile (case sensitive)
		 */
		bool rangeByID(std::vector<IonHit> &ionHits,
			       unsigned int range);


		//!Range the given ions, allowing only ions in the specified range. Input data will be modified to return result
		void rangeByRangeID(std::vector<IonHit> &ionHits,
				    unsigned int rangeID);

		//!Extract ion hit events in the specified ion ID values, from the input ion ionhit sequence.
		void extractIons(const std::vector<IonHit> &ionHits,
				 const std::vector<unsigned int> &ionIDs, std::vector<IonHit> &hits) const;
		//!Get the short name or long name of a specified ionID
		/*! Pass shortname=false to retrieve the long name
		 * ionID passed in must exist. No checking outside debug mode
		 */
		std::string getName(unsigned int ionID,bool shortName=true) const;

		//!Obtain the short name for a given IonHit. If not ranged, an empty string will be returned
		std::string getName(const IonHit &ion, bool shortName=true) const;

		//!set the short name for a given ion
		/*! Short and long names may not contain white space. !*/
		void setIonShortName(unsigned int ionID, const std::string &newName);

		//!Set the long name for a given ion
		/*! Short and long names may not contain white space. !*/
		void setIonLongName(unsigned int ionID, const std::string &newName);

		//!Check to see if an atom is ranged
		/*! Returns true if rangefile holds at least one range with shortname
		 * corresponding input value. Case sensitivity search is default
		 */
		bool isRanged(std::string shortName, bool caseSensitive=true);

		//!Write the rangefile to the specified output stream (default ORNL format)
		// RANGE_FORMAT_DBL_ORNL is not supported for write
		unsigned int write(std::ostream &o,size_t format=RANGE_FORMAT_ORNL) const;
		//!Write the rangefile to a file (ORNL format)
		unsigned int write(const char *datafile, size_t format=RANGE_FORMAT_ORNL) const;


		//!Get a range ID from mass to charge
		unsigned int getRangeID(float mass) const;

		//!Swap a range file with this one
		void swap(RangeFile &rng);

		//!Move a range's mass to a new location
		bool moveRange(unsigned int range, bool limit, float newMass);
		//!Move both of a range's masses to a new location
		bool moveBothRanges(unsigned int range, float newLow, float newHigh);

		//!Add a range to the rangefile. Returns ID number of added range
		/*!if adding successful, (unsigned int)-1 otherwise.
		 If enforceConsistency is true, this will disallow addition of ranges that
		 collide with existing ranges
		*/
		unsigned int addRange(float start, float end, unsigned int ionID);

		//!Add the ion to the database returns ion ID if successful, -1 otherwise
		/*! Short and long names may not contain white space. !*/
		unsigned int addIon(const std::string &shortName, const std::string &longName, const RGBf &ionCol);


		//!Set the lower bound of the given range
		bool setRangeStart(unsigned int rangeID, float v);

		//!Set the upper bound of the given range
		bool setRangeEnd(unsigned int rangeID, float v);

		//!Erase given range - this will reorder rangeIDs, so any ids you had previously will not longer be valid
		void eraseRange(unsigned int rangeId) ;

		//!erase given ions and associated ranges)
		void eraseIon(unsigned int ionId);

		//!Break a given string down into a series of substring-count pairs depicting basic ionic components
		//! returns false if the name cannot be broken down. E.g. H2O will be converted to [ {H,2}, {O,1}]
		//! Each string in the vector will be unique
		static bool decomposeIonNames(const std::string &name,

					      std::vector<std::pair<std::string,size_t> > &fragments);

		//!Perform decomposeIonNames(...) for a given ionID.
		bool decomposeIonById(unsigned int ionId,
				      std::vector<std::pair<std::string,size_t> > &fragments) const;

		//! Break a given string down into a chemical formula with a count of each element
		/*! If the name cannot be decomposed then an empty map is placed in the formula argument.
		 * !*/
		static void decomposeIonNameToFormula(const std::string &name, std::map<std::string,size_t> &formula);

		//!Guess the charge state for the ion that corresponds to the given range's midpoint
		// returns false if unable to guess the charge state within tolerances
		bool guessChargeState(unsigned int rangeId,
				      const AtomProbe::AbundanceData &massTable, unsigned int &charge,
				      float tolerance=0.5f) const;

#ifdef DEBUG
		//run unit tests, return true on success
		static bool test();
#endif
};



}

#endif
