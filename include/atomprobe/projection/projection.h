/* projection.cpp :  Stereographic and related projection computations
 * Copyright (C) 2020  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATOMPROBE_PROJECTION_H
#define ATOMPROBE_PROJECTION_H 

#include "atomprobe/primitives/point3D.h"

namespace AtomProbe
{

//Class that allows for conversion of coordinate systems from
// a spherical to a planar surface, touching the sphere apex.
// non-unit solutions can be obtained by appropriate scaling transforms
// off-axis projections can be obtained by a pre-rotation

// The coordinate system is a unit sphere, at centre=(0,0,0)
// the tip axis lies parallel to z
// the plane is located at z=1.
// The spherical coordinate system is ISO31-11 (aka ISO: 80000-2)
// See https://en.wikipedia.org/wiki/ISO_31-11#Coordinate_systems 
// also https://upload.wikimedia.org/wikipedia/commons/b/ba/Comparison_azimuthal_projections.svg
class SphericPlaneProjection
{
	public:
		//!Convert from plane coordinates to projection coords. 
		/*! -  Returns false if transform is not possible
		*/
		virtual bool toAzimuthal(float fx,float fy, float &theta, float &phi) const  = 0;
		//!Convert from spherical planar coordinates to projection coords
		virtual bool toPlanar(float theta ,float phi, float &fx, float &fy)  const = 0;

		//!Convert from actual detector postion (eg. mm) and flight path to scaled-down transform
		virtual void scaleDown(float flightLength,float detX,float detY,
					float &scaledX,float &scaledY) const =0;
		
		//!Convert from scaled-down coordinates to actual size
		virtual void scaleUp(float flightLength,float scaledX, float scaledY,
						float &realX, float &realY) const=0;

		virtual float thetaToEta(float theta) const = 0;
};

//TODO: In theory we could make this a single projection class,
// and have the operator switch modes. What is best?

//Pure gnomonic projection, where projection
// focus is at the sphere centre
class GnomonicProjection : public SphericPlaneProjection 
{
	public:
		//!Convert from plane coordinates to spherical coords. 
		//  Returns false if transform is not possible
		virtual bool toAzimuthal(float fx,float fy, float &theta, float &phi) const ;
		//!Convert from spherical coordinates to plane
		// note that there is a degeneracy in theta at fx=fy=0
		virtual bool toPlanar(float theta ,float phi, float &fx, float &fy) const ;


		//!Convert from actual detector postion (eg. mm) and flight path to scaled-down transform
		virtual void scaleDown(float flightLength,float detX,float detY,
						float &scaledX,float &scaledY) const;
		
		//!Convert from scaled-down radius to actual radius
		virtual void scaleUp(float flightLength,float scaledX,float scaledY,
						float &realX, float &realY) const;

		float thetaToEta(float theta) const {return  theta;}

};

//Pure stereographic projection, where projection focus is at rear of sphere
// (z=-1)
// In this transform, theta no longer follows ISO31-11 coordinate systems
// theta has range [0,PI/2). See docs/ folder for SVG diagram
class StereographicProjection : public SphericPlaneProjection 
{
	public:
		//!Convert from plane coordinates to stereographic (NOT SPHERICAL) coords. 
		//  Returns false if transform is not possible
		virtual bool toAzimuthal(float fx,float fy, float &theta, float &phi) const ;
		//!Convert from stereographic (NOT SPHERICAL) coordinates to plane
		virtual bool toPlanar(float theta ,float phi, float &fx, float &fy) const ;
			
		//!Convert from actual detector postion (eg. mm) and flight path to scaled-down transform
		virtual void scaleDown(float flightLength,float detX,float detY,
						float &scaledX,float &scaledY) const;
		
		//!Convert from scaled-down radius to actual radius
		virtual void scaleUp(float flightLength,float scaledX, float scaledY,
						float &realX, float &realY) const;

		//!Convert azimuthal angle to spherical. See etaToTheta for description
		/*! -input range is limited to [0,90)
		*/
		float thetaToEta(float theta) const;

};

//This is a spheric projection that allows for the focus to be moved
// a focus of 0 corresponds to gnomonic, a focus of 1 corresponds to stereographic,
// >1 is a "Far side general perspective" projection
class ModifiedFocusSphericProjection : public SphericPlaneProjection
{
	private:
		//Distance from sphere centre, away from plane (towards "south pole") that the focus is located at
		float focusDist;
		
		//!Return the point of projection onto a plane for a given
		// theta, given rotational symmetrical reduction to 2D problem
		bool getProjectionPt(float theta,float &f) const;
		
		
	public:
		//!Create a Spheric projection, with a focal point that is moved behind the sphere
		ModifiedFocusSphericProjection(float focus);

		//!Set the focus position of the sphere. 0= sphere centre, 1=sphere end, >1 is outside sphere
		void setFocus(float focus);
		//!Convert the spherical angle to an azimuthal angle. See docs/figures for examples
		/* !theta is the azimuthal angle is from focal point, x axis and projection vector
		 eta is the spherical angle is from sphere centre to projection point with x axis.
		 Note that using eta will place projected points correctly on the sphere, but not on the projected plane
		 valid inputs are in range [0,2*PI] */
		float etaToTheta(float eta) const;

		//!Convert azimuthal angle to spherical. See etaToTheta for description
		/*! input range is limited to [0,getMaxFOV()]
		*/
		float thetaToEta(float theta) const;

		//!Convert from plane coordinates to stereographic (NOT SPHERICAL) coords. 
		/*  Returns false if transform is not possible
		*/
		virtual bool toAzimuthal(float fx,float fy, float &theta, float &phi) const ;
		//!Convert from stereographic (NOT SPHERICAL) coordinates to plane
		/* note that there is a degeneracy in theta at fx=fy=0
		*/
		virtual bool toPlanar(float theta ,float phi, float &fx, float &fy) const ;

		//!Convert from actual detector postion (eg. mm) and flight path to scaled-down transform
		virtual void scaleDown(float flightLength,float detX,float detY,
						float &scaledX,float &scaledY) const;
		
		//!Convert from scaled-down to actual dimension
		virtual void scaleUp(float flightLength,float scaledX, float scaledY,
						float &realX, float &realY) const;
		

		//!Obtain angle spherical angle, eta, (i.e. angle from point of sphere, sphere centre and x axis) of the line that is tangent to the sphere
		/*! Beyond this there is no intersection, and no solution.
		*/
		float getMaxFOV() const;

		//!Obtain the radius in the XY plane of the sphere-angle (eta)  that gives us a given FOV
		float getFOVRadius(float eta) const;


};

#ifdef DEBUG
bool testProjection();
#endif
}
#endif
