/* reconstruction-simple.h : Sphere-on-cone reconstruction model 
 * Copyright (C) 2017  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_RECONSTRUCT_SIMPLE_H
#define ATOMPROBE_RECONSTRUCT_SIMPLE_H

#include "atomprobe/primitives/point3D.h"
#include "atomprobe/primitives/ionHit.h"
#include "atomprobe/projection/projection.h"

#include <vector>

namespace AtomProbe{

//enum that states supported shank evolution mode
enum
{
	EVOLUTION_MODE_SHANKANGLE,
};

//Convert a 2D series of detector positions to a 3D reconstructed point cloud
class ReconstructionSphereOnCone
{
	private:
		//The plane projection model (converts detector coordinates to angular ones)
                AtomProbe::SphericPlaneProjection *projectionModel;

		//!How to evolve the sphere-on-cone data, this should be set to an EVOLUTION_MODE_ value
		unsigned int evolutionMode;
	
		//!Half-angle between +z and maximum data field of view
		// can be between 0 and M_PI	
		float reconFOV;
	
		//!Initial radius of sphere used for recon	
		float initialRadius;

		//!Flight path to image (in reflectron systems, this will be different to the overall flight path)
		float flightPath;
		
		//!Shank angle, when in shank angle mode
		float shankAngle;
		
		//!Detector efficiency
		float detEfficiency;	
	public:
		//!Default constructor
		ReconstructionSphereOnCone(); 

		//!Set a given projection model. 
		void setProjModel(AtomProbe::SphericPlaneProjection *model);

		//!Set the method of evolution for the shank, as per EVOLUTION_MODE_ enum
		void setRadiusEvolutionMode(unsigned int evolutionMode);

		//!Set the initial tip radius
		void setInitialRadius(float rInit);

		//!Set initial angle, in radiians
		void setShankAngle(float angle);
		
		//!Set detector efficiency in range (0,1] 
		void setDetectorEfficiency(float detEff);
		
		//!Set flight path. Units must match detector units (eg mm)
		void setFlightPath(float detEff);

		//!Set the angle (between 0 and pi radiaans). This is 
		// the angle between +Z and the maximum detectable point
		// on a circle 
		void setReconFOV(float angle);

		//!Using the current reconstruction model, reconstruct a detector sequence
		/*! Uses the projection model given in ::setProjModel, to perform the reconstruction
		*  - This is a "shank" reconstruction
		*  - Detector sequence input should typically be centered around 0,0 - automatic recentring may not be desired, so this will not be performed
		*  - Sequence scale should match flight path (e.g. mm)
		*  - Ions may not be reconstructed if outside FOV, so |outputPts| < |detector events|
		*/
		bool reconstruct(const std::vector<float> &detectorX, 
				const std::vector<float> &detectorY,
				const std::vector<float> &timeOfFlight,
				const std::vector<float> &ionVolume,
				std::vector<IonHit> &outputPts) const ;

};


//Run the unit tests for this module
bool reconstructTest();


}

#endif
