/* generate.cpp :  Crystal tiling functions
 * Copyright (C) 2020  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATOMPROBE_CRYSTALGEN_H
#define ATOMPROBE_CRYSTALGEN_H

//C++ standard lib
#include <vector>

#include "atomprobe/primitives/ionHit.h"

namespace AtomProbe
{

//This is an abstract base class for the generation of crystal data
class CrystalGen 
{
	protected:
		//Point data
		std::vector<IonHit> localData;

		//The generated crystal should lie within the box 
		//set by the  origin and the bounding point "farPoint"
		Point3D farPoint;
		
	public:
		CrystalGen() {};
		virtual ~CrystalGen() {};
	
		virtual unsigned int generateLattice() =0;

//		virtual bool checkSanity();
		//Swap-out the ion hit data from this object, to obtain the internally gennerated lattice
		virtual void swap(std::vector<IonHit> &data) {localData.swap(data);};
		//Extract the point data from the object
		virtual void extractPositions(std::vector<Point3D> &data);

		//Mirror the data across all 3 axes
		virtual void mirrorOut();
};

class SimpleCubicGen : public CrystalGen
{
	private:
		//!"a" spacing in cubic unit cell
		float spacing;
		//!mass to charge for each atom in the unit cell
		float massToCharge;
	public:
		SimpleCubicGen(float latticeSpacing, float massToC, const Point3D &p);
		unsigned int generateLattice();
};

class FaceCentredCubicGen : public CrystalGen
{
	private:
		//!"a" spacing in cubic unit cell
		float spacing;
		//!mass to charge for each atom in the unit cell
		float massToChargeCorner, massToChargeFace[3];
	public:
		//The m/c for the faces should be a 3-wide array, with x,y and z normal-faces
		FaceCentredCubicGen(float latticeSpacing, float mToCCorner, 
					const float *mToCFace,const Point3D &p);
		unsigned int generateLattice();
};

class BodyCentredCubicGen : public CrystalGen
{
	private:
		//!"a" spacing in cubic unit cell
		float spacing;
		//!mass to charge for each atom in the unit cell
		float massToChargeCorner, massToChargeCentre;
	public:
		//The m/c for the unit cell corner and body atoms (centre)
		BodyCentredCubicGen(float latticeSpacing, float mToCCentre, 
					const float mToCCorner,const Point3D &p);
		//Generate the lattice, up to the specified point
		unsigned int generateLattice();
};
enum
{
	CRYSTAL_BAD_UNIT_CELL,
	CRYSTAL_ENUM_END
};

#ifdef DEBUG
bool runGenerateTests();
#endif
		
}
		
#endif

