/* millterIndex.cpp :  Miller index sequence generation
 * Copyright (C) 2020  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATOMPROBE_MILLERINDEX_H
#define ATOMPROBE_MILLERINDEX_H

#include "atomprobe/primitives/point3D.h"

#include <vector>


namespace AtomProbe
{
enum
{
	LATTICE_FCC,
	LATTICE_BCC,
	LATTICE_HCP,
	LATTICE_END_OF_ENUM	
};


class MILLER_TRIPLET
{
	private:
		//Miller indicies (h,k,l)
		int idx[3];
	public:
		MILLER_TRIPLET(int a, int b, int c); 

		int h() const;
		int k() const;
		int l() const;

		bool operator==(const MILLER_TRIPLET &m) const;

		//Divide hkl by GCD(h,k,l) to produce minimal numerical
		// equivalent vector (ie [240] becomes [120])
		void simplify();
		
		//Convert a miller triplet to the normal vector to said plane.
		Point3D tripletToNormal(unsigned int h, unsigned int k, unsigned int l);
		//Convert a miller triplet to the normal vector to said plane.
		Point3D tripletToNormal(const MILLER_TRIPLET &m);
};

#ifdef DEBUG
bool millerTest();
#endif

}

#endif
