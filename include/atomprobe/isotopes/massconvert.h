/* massconvert.h : Simple mass-mole input conversion 
 * Copyright (C) 2020  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <map>
#include <vector>

namespace AtomProbe
{


//!composition parsing error messages
enum
{
	COMPPARSE_BAD_INPUT_FORMAT=1, //could not parse input format
	COMPPARSE_BAD_COMP_VALUE, //could not parse composition data
	COMPPARSE_BAD_BAL_USAGE, // "bal" tag used more than once
	COMPPARSE_BAD_SYMBOL, // Atomic symbol not understood
	COMPPARSE_DUPLICATE_SYMBOL, // Atomic symbol used more than once 
	COMPPARSE_BAD_TOTAL_COMP, //Negative balance required to match total
};

//!Convert atomic string label data, in the form "Fe 0.81" to fraction map. 
/*! The input type (mol/mass) is not relevant for this function, but should be in fraction 
 the label "bal" can be used in place of a composition value exactly
 once to provide "balance" mass to sum to 1
 returns 0 on success, nonzero on error (see COMPPARSE enum)
*/
unsigned int parseCompositionData(const std::vector<std::string> &s, 
	const AbundanceData &atomTable, std::map<unsigned int, double> &atomData);

//!Convert the given composition from molar fraction data to mass fraction
/*!Inputs are:
 - atomTable - the natural abundance table, to provide weights
 - compositionMols : relative molar fractions (this will be normalised automatically). 
 	First entry in map is ion index in abundance table. Second is relative fraction.

 Outputs are:
 - compositionMass : output results, as mass fractions First entry is
   the position in  the abundance table, second is relative fraction
 */
void convertMolToMass( const AbundanceData &atomTable,
	const std::map<unsigned int, double> &compositionMols,
	std::map<unsigned int, double> &compositionMass);


//!Convert the given composition from mass fraction data to molar fraction
/*!inputs are:
 compositionMass - relative mass fractions (this will be normalised automatically)
 massValues - weights of the masses input
compositionMols - output results 
*/
void convertMassToMol( const AbundanceData &atomTable,
		const std::map<unsigned int, double> &compositionMass,
		std::map<unsigned int, double> &compositionMols);

#ifdef DEBUG
bool testMassMolConversion();
#endif

}

