/* 
 * Copyright (C) 2015  D Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATOMPROBE_ABUNDANCE_H
#define ATOMPROBE_ABUNDANCE_H

#include "atomprobe/apt_config.h"

#include <vector>
#include <string>
#include <utility>

namespace AtomProbe
{ 

//Structure containing information relating to a specific isotope
// such as say, U-238.
// Example (2^H, Deuterium)
//======
//  |------|
//  | 2    |<- mass number
//  |    H |<- Symbol
//  | 1    |<- atomic number
//  |------|
//    2.014101 <- mass
//    0.00000000006 <- mass error
//    0.000115 <- abundance
//    0.000070 <- abundance error
//======
struct ISOTOPE_ENTRY
{
	std::string symbol; //Isotope symbol
	size_t massNumber; //Mass number (for C, eg 12 or 13)
	size_t atomicNumber; //Atomic number (for C, this can only be 12)
	float mass; // in units specified in as-loaded XML file.
	float massError; //positive if known, zero if unknown
	float abundance; //Natural abundance. Zero is not naturally occurring. Between 0 and 1
	float abundanceError; //positive if known, zero if unknown
};

//!Class to load abundance information for natural isotopes
class AbundanceData
{
	enum
	{
		ABUNDANCE_ERR_BAD_DOC=1,
		ABUNDANCE_ERR_NO_CONTEXT,
		ABUNDANCE_ERROR_BAD_VALUE,
		ABUNDANCE_ERROR_FAILED_VALIDATION,
		ABUNDANCE_ERROR_MISSING_NODE,
		ABUNDANCE_ERROR_MISSING_ROOT_NODE,
		ABUNDANCE_ERROR_WRONG_ROOT_NODE,
		ABUNDANCE_ERROR_ENUM_END
	};
	
	//!vector of atoms, containing a vector of isotope datasets
	// First vector is indexed by position, not atomic number because 
	//elements may be missing from abundance file, eg Tc
	std::vector<std::vector<ISOTOPE_ENTRY> >  isotopeData;

	//Check the abundance table for inconsistencies
	void checkErrors() const; 
	
public:
#ifdef HAVE_LIBXML2
		//!Attempt to open the abundance data file, return 0 on success, nonzero on failure 
		/*! If nonzero, a human readable message can be
		 obtained from getErrorText(...)
		 */
		size_t open(const char *file, bool strict=false);	
#endif
		//!Obtain a human-readable error message from the open( ) call
		static const char *getErrorText(size_t errorCode);

		//!Return the number of isotopes in the loaded table
		size_t numIsotopes() const;
		//!Return the number of elements in the loaded table
		size_t numElements() const;

		//!Return the element's position in table, starting from 0
		/*! Notes
		 - Input is case sensitive by default
		 - returns (unsigned) -1 if the symbol cannot be found
		 */
		size_t symbolIndex(const char *symbol, bool caseSensitive=true) const;

		//!Return a vector of symbol indices
		void getSymbolIndices(const std::vector<std::string> &symbols,
						std::vector<size_t> &indices) const;
		
		//!Obtain the symbol indicies for a string,payload type. Resultant pair has first item in index, second is payload
		void getSymbolIndices(const std::vector<std::pair<std::string,size_t> > &symbols,
							std::vector<std::pair<size_t,unsigned int> > &indices) const;

		//!Obtain the name of an element from its index 
		std::string elementName(size_t elemIdx ) const { return isotopeData[elemIdx][0].symbol;}

		//!Return the symbol index (offset in vector) for the given atomic number, -1 if it cannot be found
		size_t symbolIdxFromAtomicNumber(size_t atomicNumber) const;
	
		//!Obtain a delimited list of element names, using the (optional) supplied separator, over the specified [start,end) range.
		std::string elementNames(size_t start, size_t end, char separator=',') const; 
	
		//!Obtain the atom ID (first) and the isotope value(second).
		/*! The mass and element ID must match exactly
		 NOTE: The element and isotope indices are *NOT* its isotopic number
		 it is the offset to that isotope int the abundance table */
		void isotopeIndex(size_t elem, float mass, size_t &isotopeIdx) const;

		//!Return the first matching element in the table which has a given tolerance to the specified match
		void isotopeIndex(size_t elem, float mass, float tolerance, size_t &isotopeIdx) const;

		//!Obtain the atomic number for the given element, by element index
		unsigned int getAtomicNumber(size_t elemIdx) const;
		
		//!Obtain the atomic number for the given element, by element index. -1 if not found
		unsigned int getAtomicNumber(const char *symbol) const; 

		//!Return the isotope at a given position in the table. Offset *must* exist
		const std::vector<ISOTOPE_ENTRY> &isotopes(size_t offset) const; 

		//!Compute the mass-probability distribution for the given set of ions
		/*! E.g., for C2, this would be elementIdx = elementIdx("C"),
		 and frequency 2. This would return probabilities for each unique mass
		 {C-12 C-12},{C-12 C-13} and {C-13 C-13}....
		 note an internal "grouping" tolerance is used to group-together very close masses. 
		 This may be smaller than what is needed for your application, so if you wish to 
		 obtain a more specific distribution use "generateGroupedIsotopeDist"
                 - massDist is the vector of <mass,abundance> data for the isotope distribution
                 - isotopeList is gives the vector of <element,isotope>s that make up the most abundance component of each peak
		*/
		void generateIsotopeDist(const std::vector<size_t> &elementIdx,
					const std::vector<size_t> &frequency,
					std::vector<std::pair<float,float> > &massDist,
                                        std::vector<std::vector<std::pair<unsigned int, unsigned int> > > &isotopeList) const;

		//!As per generateIsotopeDist, however, this convenience groups the distribution to limit the effect of minute mass changes
		/*! This groups the masses of nearby isotope combinations
		 together. This is desirable due to species, such as
		 Mo2, which have isotopes within <0.001 amu of one another.
		 The mass centres are computed using a weighted average, and the intensities are summed */
                void generateGroupedIsotopeDist(const std::vector<size_t> &elementIdx,
						const std::vector<size_t> &frequency, std::vector<std::pair<float,float> > &massDist,  
                                                std::vector<std::vector<std::pair<unsigned int, unsigned int > > > &isotopes,
						float massTolerance) const;

		//!Obtain the mass distribution from a single isotope that repeats. Output is not grouped
		// - mass dist has the mass/intensity (first/second) values for each peak
		// - isotopes has the set of isotopes that generates each peak in massDist, returned size of isotopes is the same as massDist, each entry corresponds.
		void generateSingleAtomDist(size_t atomIdx, unsigned int repeatCount, 
					std::vector<std::pair<float,float> > &massDist, std::vector<std::vector<unsigned int> >  &isotopes) const;

		//!Obtain the fractional abundance between two limits for the given species [start,end)
		float abundanceBetweenLimits(const std::vector<size_t> &elemIdx,
						const std::vector<size_t> &frequency,
						float massStart, float massEnd) const;

		//!Obtain a reference to a particular isotope, using the element's index in the table, and the isotope index
		const ISOTOPE_ENTRY &isotope(size_t elementIdx, size_t isotopeIdx) const;

		//!Obtain the most prominent isotope's index from the element index
		size_t getMajorIsotopeFromElemIdx(size_t elementIdx) const;

		//!Obtain the isotope weighted mass for the given element
		float getNominalMass(size_t elementIdx) const;

		//!Obtain the closest charge state for the given mass and species group
		/*! In the "molecule" parameter, the pair is <element index, count>, the target mass is
		given in Da, and the maximum charge is the maximum the function will search for 
		*/
		size_t getNearestCharge(const std::vector<std::pair<size_t,size_t> > &molecule,float targetMass,size_t maxCharge) const;

#ifdef DEBUG
		//Run the unit testing code
		static bool runUnitTests();
#endif
};

};

#endif
