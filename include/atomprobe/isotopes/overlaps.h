/* overlaps.h :  Mass spectrum Overlap finding algorithms
 * Copyright (C) 2020  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATOMPROBE_OVERLAPS_H
#define ATOMPROBE_OVERLAPS_H

#include "atomprobe/isotopes/abundance.h"
#include "atomprobe/helper/stringFuncs.h"
#include "atomprobe/io/ranges.h"

#include <utility>
#include <set>

namespace AtomProbe
{

//!Find the overlaps stemming from a given rangefile.
/*!An overlap is defined as "natural abundances within +-massDelta at same mass to charge"
	- Species not in the rangefile will not be marked as overlapping
	- FIXME: The output index is abused at this time, the range's index is actually given by 
			dividing the output by the mass to charge. 
			The charge state is given by modulo (massToCharge) +1 
 	- The range mass information in the rangefile is not used - it is abused as an ion list

 Inputs are:
  	- rng: The rangefile to use. The species in the rangefile are used to compute the overlaps
  	- massDelta: tolerance in amu,
	- maxCharge: the maximum charge that species can be estimated to have

Outputs are:
	- overlapIdx: the range indices of the overlapping species (ion index in rangefile)
	- overlapMasses: the masses of the overlapping species, which should be within +-massDelta of one another

	Note that ion's names will be decomposed, so Fe2H will be considered an Fe-Fe-H molecule
*/
void findOverlaps(const AbundanceData &natData,
			const RangeFile &rng, float massDelta, unsigned int maxCharge,
			std::vector<std::pair< size_t,size_t> > &overlapIdx, std::vector<std::pair<float,float> > &overlapMasses );

//!As per findOverlaps(...Rangefile ...) , but uses a vector of ions instead
void findOverlaps(const AbundanceData &natData,
			const std::vector<std::string> &ions, float massDelta, unsigned int maxCharge,
			std::vector<std::pair< size_t,size_t> > &overlapIdx, std::vector<std::pair<float,float> > &overlapMasses );


//!Find overlaps in the given mass distributions, as per  findOverlaps(...Rangefile...) 
// - massDistributions is a vector of isotope fingerprints, where the first entry in the pair is mass and the second (unused) is intensity
// - skipIds (optional - can be empty) are the IDs of mass distributions to ignore
void findOverlapsFromSpectra(const std::vector<std::vector<std::pair<float,float> > > &massDistributions,
			float massDelta,const std::set<unsigned int> &skipIDs,
			std::vector<std::pair<size_t ,size_t> > &overlapIdx,
			std::vector<std::pair<float,float> > &overlapMasses);
#ifdef DEBUG
bool testOverlapDetect();
#endif

}
#endif
