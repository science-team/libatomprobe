/* 
 * abundance.cpp : module to load, manipulate and compute natural abundance information
 * Copyright (C) 2015  D Haley 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cstdlib>
#include <stack>
#include <map>
#include <cmath>
#include <limits>
#include <algorithm>
#include <numeric>
#include <tuple>

#include "atomprobe/apt_config.h"
#include "atomprobe/isotopes/abundance.h"
#include "atomprobe/helper/XMLHelper.h"
#include "atomprobe/helper/stringFuncs.h"
#include "atomprobe/helper/misc.h"
#include "atomprobe/helper/aptAssert.h"
#include "atomprobe/helper/maths/misc.h"


#include "helper/helpFuncs.h"

#include "atomprobe/helper/aptAssert.h"

namespace AtomProbe
{


using std::vector;
using std::pair;
using std::make_pair;
using std::map;
using std::string;
using std::tuple;
using std::make_tuple;

const char *ABUNDANCE_ERROR[] = { "Unable to read abundance data (opening file)",
				 "Unable to create XML reader.",
				 "Bad property found in XML file",
				 "XML document did not match expected layout (DTD validation)",
				 "Unable to find required node during parse",
				 "Root node missing, expect <atomic-mass-table>!",
				 "Found incorrect root node. Expected <atomic-mass-table>"
};


const char *AbundanceData::getErrorText(size_t errorCode)
{
	ASSERT(errorCode < ABUNDANCE_ERROR_ENUM_END);
	return ABUNDANCE_ERROR[errorCode];
}

size_t AbundanceData::numIsotopes() const
{
	size_t v=0;
	for(size_t ui=0;ui<isotopeData.size();ui++)
		v+=isotopeData[ui].size();
	
	return v;
}

size_t AbundanceData::numElements() const
{
	return isotopeData.size();
}

#ifdef HAVE_LIBXML2

size_t AbundanceData::open(const char *file, bool strict)
{

	xmlDocPtr doc;
	xmlParserCtxtPtr context;

	context =xmlNewParserCtxt();

	if(!context)
		return ABUNDANCE_ERR_NO_CONTEXT;

	//Open the XML file
	doc = xmlCtxtReadFile(context, file, NULL, XML_PARSE_DTDVALID| XML_PARSE_NOENT | XML_PARSE_NONET);

	if(!doc)
	{
		xmlFreeParserCtxt(context);
		return ABUNDANCE_ERR_BAD_DOC;
	}
	else
	{
		//Check for context validity
		if(!context->valid)
		{
			if(strict)
			{
				xmlFreeDoc(doc);
				xmlFreeParserCtxt(context);
				return ABUNDANCE_ERROR_FAILED_VALIDATION;
			}
		}
	}

	try
	{
	
	//retrieve root node	
	std::stack<xmlNodePtr> nodeStack;
	xmlNodePtr nodePtr = xmlDocGetRootElement(doc);

	if(!nodePtr)
		throw ABUNDANCE_ERROR_MISSING_ROOT_NODE;
	
	//This *should* be an abundance file
	if(xmlStrcmp(nodePtr->name, (const xmlChar *)"atomic-mass-table"))
		throw ABUNDANCE_ERROR_WRONG_ROOT_NODE;

	nodeStack.push(nodePtr);

	nodePtr=nodePtr->xmlChildrenNode;
	while(!XMLHelpFwdToElem(nodePtr,"entry"))
	{
		ISOTOPE_ENTRY curIsoData;
		
		if(XMLHelpGetProp(curIsoData.symbol,nodePtr,"symbol"))
			throw ABUNDANCE_ERROR_BAD_VALUE;
		
		if(XMLHelpGetProp(curIsoData.atomicNumber,nodePtr,"atomic-number"))
			throw ABUNDANCE_ERROR_BAD_VALUE;


		nodeStack.push(nodePtr);
		nodePtr=nodePtr->xmlChildrenNode;
		
		//Move to natural-abundance child
		if(XMLHelpFwdToElem(nodePtr,"natural-abundance"))
			throw ABUNDANCE_ERROR_MISSING_NODE;
		
		nodePtr=nodePtr->xmlChildrenNode;

		vector<ISOTOPE_ENTRY> curIsotopes;	
		//TODO: value checking	
		while(!XMLHelpFwdToElem(nodePtr,"isotope"))
		{
			//Spin to mass node
			if(XMLHelpGetProp(curIsoData.massNumber,nodePtr,"mass-number"))
				throw ABUNDANCE_ERROR_MISSING_NODE;
			
			nodeStack.push(nodePtr);
			nodePtr=nodePtr->xmlChildrenNode;

			//Spin to mass node
			if(XMLHelpFwdToElem(nodePtr,"mass"))
				throw ABUNDANCE_ERROR_MISSING_NODE;

			if(XMLHelpGetProp(curIsoData.mass,nodePtr,"value"))
				throw ABUNDANCE_ERROR_BAD_VALUE;
			
			if(XMLHelpGetProp(curIsoData.massError,nodePtr,"error"))
				throw ABUNDANCE_ERROR_BAD_VALUE;
			else
				curIsoData.massError=0;


			//spin to abundance node
			if(XMLHelpFwdToElem(nodePtr,"abundance"))
				throw ABUNDANCE_ERROR_MISSING_NODE;
			
			if(XMLHelpGetProp(curIsoData.abundance,nodePtr,"value"))
				throw ABUNDANCE_ERROR_BAD_VALUE;
			
			if(XMLHelpGetProp(curIsoData.abundanceError,nodePtr,"error"))
				throw ABUNDANCE_ERROR_BAD_VALUE;
			else
				curIsoData.abundanceError=0;

			curIsotopes.push_back(curIsoData);

			nodePtr=nodeStack.top();
			nodePtr=nodePtr->next;
			nodeStack.pop();
		}

		isotopeData.push_back(curIsotopes);
		curIsotopes.clear();	

		nodePtr=nodeStack.top();
		nodeStack.pop();
		nodePtr=nodePtr->next;
		
	}
	}
	catch( int &excep)
	{
		xmlFreeDoc(doc);
		xmlFreeParserCtxt(context);
		return excep;
	}
		
	xmlFreeDoc(doc);
	xmlFreeParserCtxt(context);
	return 0;
}

#endif

size_t AbundanceData::symbolIndex(const char *symbol, bool caseSensitive) const
{
	ASSERT(isotopeData.size());
	if(!caseSensitive)
	{
		for(size_t ui=0;ui<isotopeData.size();ui++)
		{
			if(isotopeData[ui].size() && lowercase(isotopeData[ui][0].symbol) == lowercase(symbol) )
				return ui;
		}
	}
	else
	{
		for(size_t ui=0;ui<isotopeData.size();ui++)
		{
			if(isotopeData[ui].size() && isotopeData[ui][0].symbol == symbol)
				return ui;
		}
	}

	return (size_t)-1;
}


void AbundanceData::isotopeIndex(size_t elemIn, float mass ,size_t &isoIndex) const
{
	ASSERT(isotopeData.size());
	for(size_t uj=0;uj<isotopeData[elemIn].size();uj++)
	{
		if(isotopeData[elemIn][uj].mass == mass)
		{
			isoIndex=uj;
			return;
		}
	}

	isoIndex=(size_t)-1;
	return;
}

void AbundanceData::isotopeIndex(size_t elem, float mass, float tolerance, size_t &isoIndex) const
{
	ASSERT(isotopeData.size());
	for(size_t uj=0;uj<isotopeData[elem].size();uj++)
	{
		if(fabs(isotopeData[elem][uj].mass - mass) < tolerance)
		{
			isoIndex=uj;
			return;
		}
	}

	isoIndex=(size_t)-1;
	return;
}


const ISOTOPE_ENTRY &AbundanceData::isotope(size_t elemIdx,size_t isotopeIdx) const
{
	ASSERT(isotopeData.size());
	return isotopeData[elemIdx][isotopeIdx];
}

void AbundanceData::generateIsotopeDist(const vector<size_t> &elementIdx,
					const vector<size_t> &frequency,
				vector<pair<float,float> > &massDist, vector<vector<pair<unsigned int, unsigned int > > > &isotopes) const
{
	ASSERT(isotopeData.size());
	ASSERT(frequency.size() == elementIdx.size());
	//Search out the given isotopes, and compute the peaks
	// that would be seen for this isotope combination

	//mass,intensity, [list of (elem|isotopes)]
	typedef tuple<float,float,vector<pair<unsigned int,unsigned int>> > ISOTOPE_TUPLE;

	//map of vectors for the available isotopes
	map<unsigned int, vector< ISOTOPE_TUPLE > > isotopeMassDist;

	//For each element, retrieve  its (mass,probability dist) values
	// placing into a map
	//--
	for(size_t ui=0;ui<elementIdx.size();ui++)
	{
		size_t curElem;
		curElem = elementIdx[ui];

		//If we have seen this before, move on
		if(isotopeMassDist.find(curElem) != isotopeMassDist.end())
			continue;

		vector<ISOTOPE_TUPLE> thisIsotope;
		vector<pair<unsigned int, unsigned int> > thisElem;
		for(auto uj=0u;uj<isotopeData[curElem].size();uj++)
		{
			thisElem.clear();
			thisElem.push_back(make_pair(curElem,uj));

			ISOTOPE_TUPLE t;
			t= make_tuple(isotopeData[curElem][uj].mass,
					isotopeData[curElem][uj].abundance,thisElem);

			//Record the mass-probability dist of this elements isotopes
			thisIsotope.push_back(t);
				
		}

		isotopeMassDist[curElem] = thisIsotope;
		thisIsotope.clear();

	}
	//--

	//Given the isotopes we have, permute the mass spectra
	vector<ISOTOPE_TUPLE> peakProbs;
	for(size_t ui=0;ui<elementIdx.size();ui++) //Loop over element type
	{
		unsigned int thisElem;
		thisElem=elementIdx[ui];

		for(size_t repeat=0;repeat<frequency[ui];repeat++) //Loop over the numberof times element occurs
		{
			vector< ISOTOPE_TUPLE >::const_iterator  isoBegin,isoEnd;

			isoBegin = isotopeMassDist[thisElem].begin();
			isoEnd = isotopeMassDist[thisElem].end();

			vector<ISOTOPE_TUPLE > newProbs;
			//If this is the very first item in our list,
			// simply push on the value, rather than modifying the
			// distribution
			if(peakProbs.empty())
			{
				//The masses will be added to, and the probabilities multiplied	
				for(vector<ISOTOPE_TUPLE >::const_iterator it=isoBegin; it!=isoEnd;++it)
					peakProbs.push_back(*it);
			}
			else
			{
				const unsigned int peakProbSize = peakProbs.size();
				newProbs.resize(peakProbSize*isotopeMassDist[elementIdx[ui]].size());
				unsigned int offset=0;

				for(size_t uj=0;uj<peakProbSize;uj++) //Loop over mass distribution
				{
			
					//Convolve it with other peaks
					//The masses will be added to, and the probabilities multiplied	
                                        // The isotope listing will be appended.
					for(vector<ISOTOPE_TUPLE >::const_iterator it=isoBegin;it!=isoEnd;++it) 
					{
						ISOTOPE_TUPLE newMass;
						newMass = peakProbs[uj];
						std::get<0>(newMass)+=std::get<0>(*it);
						std::get<1>(newMass)*=std::get<1>(*it);
						std::get<2>(newMass).push_back(make_pair(thisElem,std::distance(isoBegin,it))); 

						newProbs[offset]=newMass;
						offset++;
					}

				}
				
				peakProbs.swap(newProbs);
				newProbs.clear();
			}

			}
	}


	float tolerance=sqrt(std::numeric_limits<float>::epsilon());

	vector<bool> killPeaks(peakProbs.size(),false);
	const unsigned int peakProbSize=peakProbs.size();
#pragma omp parallel for
	//Find the non-unique peaks and sum them
	for(size_t ui=0;ui<peakProbSize;ui++)
	{
		for(size_t uj=ui+1;uj<peakProbSize;uj++)
		{
			if(fabs(std::get<0>(peakProbs[ui])-std::get<0>(peakProbs[uj])) <tolerance &&
				std::get<0>(peakProbs[uj]) > 0.0f)
			{

				std::get<1>(peakProbs[ui])+=std::get<1>(peakProbs[uj]);
				std::get<1>(peakProbs[uj])=0.0f;
				killPeaks[uj]=true;
			}
		}
	}
	
	vectorMultiErase(peakProbs,killPeaks);


	massDist.resize(peakProbs.size());

	for(auto ui=0u;ui<peakProbs.size();ui++)
		massDist[ui]= std::make_pair(std::get<0>(peakProbs[ui]),std::get<1>(peakProbs[ui]));

	//FIXME : Implement isotope list unpack
        isotopes.clear();
        isotopes.reserve(peakProbs.size());
        //Loop over the isotope tuple vector
        for(const auto &v : peakProbs)
            isotopes.push_back(std::move(std::get<2>(v)));
}

void AbundanceData::generateGroupedIsotopeDist(const vector<size_t> &elementIdx,
						const vector<size_t> &frequency, vector<pair<float,float> > &massDist,  
                                                vector<vector<pair<unsigned int, unsigned int > > > &isotopes,
						float massTolerance) const
{
	//First, generate the isotope distribution
	vector<vector<pair<unsigned int, unsigned int > > > isotopeList;
	generateIsotopeDist(elementIdx,frequency, massDist,isotopeList);

	//We can't group zero or 1 results
	if(massDist.size() < 2)
		return;


        //
        typedef vector<pair<unsigned int, unsigned int > > ISOTOPE_LABELS;
        typedef pair<float,float> ISOTOPE_PEAKS;

        //Repack isotope
        vector<pair<ISOTOPE_PEAKS,ISOTOPE_LABELS>> peakLabelPairs;
        for(auto ui=0;ui<massDist.size();ui++)
        {
            peakLabelPairs.push_back(make_pair(massDist[ui],isotopeList[ui]));
        }


       
        //Sort by increasing mass
        auto sFunc = [](const pair<ISOTOPE_PEAKS,ISOTOPE_LABELS> &x,
                const pair<ISOTOPE_PEAKS,ISOTOPE_LABELS> &y) { 
                return ( x.first.first < y.first.first);};
        std::sort(peakLabelPairs.begin(), peakLabelPairs.end(),sFunc);

	//Now we have to accumulate overlapping entries.
	vector<vector<unsigned int> > overlappingEntries;

        //Indices to peakLabelPairs
	vector<unsigned int> curGroup;
	curGroup.push_back(0);

	float threshPos=peakLabelPairs[0].first.first + massTolerance;
	for(unsigned int ui=1;ui<massDist.size(); ui++)
	{
		if(peakLabelPairs[ui].first.first > threshPos)
		{
			//Terminate the current group
			overlappingEntries.push_back(curGroup);
			curGroup.clear();

		}
		curGroup.push_back(ui);
		threshPos=peakLabelPairs[ui].first.first + massTolerance;
	}

	overlappingEntries.push_back(curGroup);

	massDist.clear();
        isotopes.clear();
	//OK, so now we have each overlap set as a group, average each
	// group's mass to provide a good mass estimate, but sum the abundance intensities
	for(unsigned int ui=0;ui<overlappingEntries.size(); ui++)
	{

                pair<const ISOTOPE_LABELS*, float > maxPair;
                vector<float> x,y;
                

                x.clear();
                y.clear();
                ISOTOPE_LABELS i;
		for(unsigned int uj=0;uj<overlappingEntries[ui].size();uj++)
		{
                        const auto &a= peakLabelPairs[overlappingEntries[ui][uj]];
			x.push_back(a.first.first);
			y.push_back(a.first.second);

                        if(maxPair.second < a.first.second)
                        {
                            maxPair.first=&(a.second);
                            maxPair.second=a.first.second;
                        }
		}

		float wMean;
		wMean=weightedMean(x,y);

		float summedAbundance;
		summedAbundance=std::accumulate(y.begin(),y.end(),0.0f);

		massDist.push_back(make_pair(wMean,summedAbundance));
                isotopes.push_back(*(maxPair.first));

	}


}

float AbundanceData::abundanceBetweenLimits( const std::vector<size_t> &elemIdx,
						const std::vector<size_t> &frequency,
						float massStart, float massEnd) const
{
	vector<vector<pair<unsigned int, unsigned int > > > isotopeList;
	vector<pair<float,float> > massDist;
	generateIsotopeDist(elemIdx,frequency,massDist,isotopeList);
	float abundance=0;
	for(const auto &isotope : massDist)
	{
		if(isotope.first >=massStart &&  isotope.first < massEnd)
			abundance+=isotope.second;


	}

	ASSERT(abundance >=0 && abundance <=1);
	return abundance;
}

size_t AbundanceData::getMajorIsotopeFromElemIdx(size_t elementIdx) const
{
	ASSERT(elementIdx < isotopeData.size());
	const std::vector<ISOTOPE_ENTRY> &curIsotopes= isotopeData[elementIdx];

	ASSERT(isotopeData.size());
	
	size_t maxIdx=0;
	float maxV=curIsotopes[0].abundance;
	for(size_t ui=1; ui<curIsotopes.size();ui++)
	{
		if(curIsotopes[ui].abundance > maxV )
		{
			maxIdx=ui;
			maxV=curIsotopes[ui].abundance;
		}
	}

	return maxIdx;
}

unsigned int AbundanceData::getAtomicNumber(size_t elementIdx) const
{
	ASSERT(elementIdx < isotopeData.size());
	const std::vector<ISOTOPE_ENTRY> &curIsotopes= isotopeData[elementIdx];

#ifdef DEBUG
	for(size_t ui=1;ui<curIsotopes.size();ui++)
	{
		ASSERT(curIsotopes[ui].atomicNumber == curIsotopes[0].atomicNumber);
	}
#endif
	return curIsotopes[0].atomicNumber;
}

unsigned int AbundanceData::getAtomicNumber(const char *symbol) const
{
    unsigned int idx;
    idx = symbolIndex(symbol);
    if(idx == -1)
        return idx;


    return getAtomicNumber(idx);
}

void AbundanceData::generateSingleAtomDist(size_t atomIdx, unsigned int repeatCount, 
						std::vector<std::pair<float,float> > &massDist, std::vector<vector<unsigned int> > &isotopes) const
{
	using std::tuple;
	using std::make_tuple;
	massDist.clear();
	isotopes.clear();

	//Obtain the single Isotope distribution
	vector<tuple<float,float, vector<unsigned int>> > singleDist;
	const std::vector<ISOTOPE_ENTRY> &curIso=isotopeData[atomIdx];
	for(auto ui=0u;ui<curIso.size();ui++)
	{
		singleDist.push_back(make_tuple(curIso[ui].mass,curIso[ui].abundance,vector<unsigned int>{ui}));
	}

	//Duplicate the single dist for the first atom
	//
	auto runningDist= singleDist;
	//For the number of elements, branch the distribution
	// out from this using the single mass distribution to continually
	// create new members 
	for(size_t ui=1;ui<repeatCount;ui++)
	{
		vector<tuple<float,float,vector<unsigned int> > >  newDist;
		//Add the new element to the multiDist
		for(size_t uj=0;uj<singleDist.size();uj++)
		{
			for(size_t uk=0;uk<runningDist.size();uk++)
			{
				tuple<float,float,vector<unsigned int> > chainedDist;
                                //Chain isotope data into the new distribution
                                std::get<0>(chainedDist) = std::get<0>(runningDist[uk])+std::get<0>(singleDist[uj]); //Mass is additive
                                std::get<1>(chainedDist) = std::get<1>(runningDist[uk])*std::get<1>(singleDist[uj]); //Abundance is multiplicative

                                //Chain on new isotope index (chaining set)
                                std::get<2>(chainedDist)= std::get<2>(runningDist[uk]);
				std::get<2>(chainedDist).push_back(std::get<2>(singleDist[uj])[0]); 

				newDist.push_back(chainedDist);
			}
		}
		runningDist.swap(newDist);
		newDist.clear();
	}

        //Unpack isodist
        for(auto item : runningDist)
        {
            massDist.push_back(make_pair(std::get<0>(item),std::get<1>(item)));
            isotopes.push_back(std::get<2>(item));
        }

	ASSERT(massDist.size() == isotopes.size());
#ifdef DEBUG
	for(const auto &v : isotopes)
	{
		ASSERT(v.size());
	}
#endif
}



void AbundanceData::getSymbolIndices(const vector<string> &symbols,vector<size_t> &indices) const
{
	indices.resize(symbols.size());
	for(size_t ui=0;ui<symbols.size(); ui++)
		indices[ui]=symbolIndex(symbols[ui].c_str());

}

void AbundanceData::getSymbolIndices(const vector<pair<string,size_t> > &symbols,
			vector<pair<size_t,unsigned int >> &indices) const
{
	//Extract first
	vector<string> sV;
	vector<size_t> iV;
	sV.resize(symbols.size()); iV.resize(symbols.size());
	for(unsigned int ui=0;ui<symbols.size(); ui++)
	{
		sV[ui]= symbols[ui].first;
		iV[ui]= symbols[ui].second;
	}

	vector<size_t> indicesV;
	getSymbolIndices(sV,indicesV);

	//Repackage into pair
	indices.resize(sV.size());
	for(unsigned int ui=0; ui<indices.size();ui++)
	{
		indices[ui].first = indicesV[ui];  //Index
		indices[ui].second = iV[ui]; //Payload
	}

}

std::string AbundanceData::elementNames(size_t start,size_t end, char separator) const
{
	ASSERT(start < end &&  isotopeData.size());
	std::string retStr,sep;
	sep=separator;
	for(size_t ui=start; ui<end; ui++)	
	{
		size_t symbolIdx;
		symbolIdx=symbolIdxFromAtomicNumber(ui);
		if(symbolIdx !=(size_t)-1)
			retStr+=elementName(symbolIdx) + sep;
	}

	return retStr;
}

size_t AbundanceData::symbolIdxFromAtomicNumber(size_t atomicNumber) const
{
	for(size_t ui=0;ui<isotopeData.size(); ui++)
	{
		if(isotopeData[ui].size() && 
			isotopeData[ui][0].atomicNumber == atomicNumber)
			return ui;
	}

	return (size_t)-1;
}

const std::vector<ISOTOPE_ENTRY> &AbundanceData::isotopes(size_t offset) const 
{
	ASSERT(offset < isotopeData.size()); return isotopeData[offset];
}

float AbundanceData::getNominalMass(size_t elementIdx) const
{

	const vector<ISOTOPE_ENTRY> &isoDat=isotopeData[elementIdx];

	float total=0;
	for(size_t ui=0;ui<isoDat.size();ui++)
	{
		total+=isoDat[ui].abundance*isoDat[ui].mass;
	}

	//return mean
	return total;
	
}

size_t AbundanceData::getNearestCharge(const vector<pair<size_t,size_t> > &element,float targetMass,size_t maxCharge) const
{
	//find the nearest isotope
	vector<pair<float,float> > isotopeDist;

	vector<size_t> elemIdx, elemFreq;
	elemIdx.resize(element.size());
	elemFreq.resize(element.size());

	for(size_t ui=0;ui<element.size();ui++)
	{
		elemIdx[ui] = element[ui].first;
		elemFreq[ui] = element[ui].second;
	}

	vector<vector<pair<unsigned int, unsigned int > > > isotopeList;
	generateIsotopeDist(elemIdx,elemFreq,isotopeDist,isotopeList);

	float minDelta=std::numeric_limits<float>::max();
	//this is the position of the isotope in the dist
	size_t bestCharge=0;
	for(size_t curCharge=1;curCharge<maxCharge; curCharge++)
	{
		for(size_t ui=0;ui<isotopeDist.size();ui++)
		{
			float curDelta;
			curDelta=fabs(isotopeDist[ui].first/curCharge-targetMass);
			if( curDelta< minDelta)
			{
				minDelta = curDelta;
				bestCharge=curCharge;
			}
		}
	
	}

	return bestCharge;
}

#ifdef DEBUG
void AbundanceData::checkErrors() const
{
	//Ensure all isotopes sum to 1-ish
	// Rounding errors limit our correctness here.
	for(size_t ui=0;ui<isotopeData.size();ui++)
	{
		if(!isotopeData[ui].size())
			continue;

		float sum;
		sum=0.0f;
		for(size_t uj=0; uj<isotopeData[ui].size();uj++)
		{
			sum+=isotopeData[ui][uj].abundance;
		}

		ASSERT(fabs(sum -1.0f) < 0.000001);

	}


	//Ensure Ti has 5 isotopes (original data file was missing)
	ASSERT(isotopeData[symbolIndex("Ti")].size() == 5);
}

bool AbundanceData::runUnitTests()
{
#ifdef HAVE_LIBXML2
	AbundanceData massTable;
	TEST(massTable.open("../data/naturalAbundance.xml") == 0,"load table");
	//FIXME: Getting the isotope dis

	size_t ironIndex=massTable.symbolIndex("Fe");
	TEST(ironIndex != (size_t)-1,"symbol lookup");

	//Generate the mass peak dist for iron
	vector<size_t> elements;
	vector<size_t> concentrations;
	elements.push_back(ironIndex);
	concentrations.push_back(1);

	std::vector<std::pair<float,float> > massDist;
	vector<vector<pair<unsigned int, unsigned int > > > isotopeList;
	massTable.generateIsotopeDist(elements,concentrations,massDist,isotopeList);

	TEST(massDist.size() == 4, "Iron has 4 isotopes");
	for(auto ui=0;ui<isotopeList.size();ui++)
	{
		TEST(isotopeList[ui].size() == 1 , "Single Fe ions should have only one isotope per peak");
		TEST(isotopeList[ui][0].first == ironIndex , "Fe dist should only contain Fe");
	}
	
	massDist.clear();

	//Should reutnr the same as generateIsotopeDist
	vector<vector<unsigned int > > feIsotopes;
	massTable.generateSingleAtomDist(ironIndex,1,massDist,feIsotopes);
	ASSERT(massDist.size() ==4);
	//Should return square result
	massTable.generateSingleAtomDist(ironIndex,2,massDist,feIsotopes);
	ASSERT(massDist.size() ==16);



	elements.clear();
	massDist.clear();

	//Check the isotope dist (grouped) for Mo2
	//==========
	size_t molyIdx = massTable.symbolIndex("Mo");
	concentrations.clear();
	//Mo2
	elements.push_back(molyIdx);
	concentrations.push_back(2);



	massTable.generateGroupedIsotopeDist(elements,concentrations,
                massDist,isotopeList,0.005f);

	// AJL Reports the following
	const float MO2_ISOTOPES[15][2] ={
		{183.8136,0.022},
		{185.8119,0.0275},
		{186.8126,0.0473},
		{187.811,0.0581},
		{188.8119,0.0578},
		{189.8111,0.1278},
		{190.8108,0.0708},
		{191.8118,0.1315},
		{192.811,0.1087},
		{193.8115,0.1074},
		{194.8124,0.0768},
		{195.8117,0.0904},
		{196.8135,0.0184},
		{197.8129,0.0465},
		{199.8149,0.0093},
	};

	ComparePairFirst cmp;
	std::sort(massDist.begin(),massDist.end(),cmp);

	const unsigned int NUM_ISOTOPES_MO2=15;
	TEST(massDist.size() == NUM_ISOTOPES_MO2,"Mo2 dist test");

        //Check that the isotope listing has one per isotope peak
        TEST(isotopeList.size() == massDist.size() , "Same counts");

        //Should all be Mo -  adding masses together should yield near-original mass 
        //(but not intensity, as mixing data is not kept)
        //Loop over each peak
        unsigned int idx=0;
        for(auto &v : isotopeList)
        {
            float thisMass;
            thisMass=0;
            //Consider each isotope
            for(auto &w : v) 
            {
                //Check the element is Mo
                TEST(w.first == molyIdx,"check Isotope dist elements");
                TEST(w.second < massTable.isotopes(molyIdx).size(),"check Isotope dist elements");

                thisMass+=massTable.isotope(w.first,w.second).mass;
            }
            TEST(TOL_EQ_V(thisMass,massDist[idx].first,0.1),"Test")
            idx++;
        }


	for(unsigned int ui=0;ui<NUM_ISOTOPES_MO2; ui++)
	{
		float m,i;
		m=MO2_ISOTOPES[ui][0];
		i=MO2_ISOTOPES[ui][1];

		float dm,di;
		dm = fabs(m-massDist[ui].first);
		di = fabs(i-massDist[ui].second);
		TEST(dm < 0.01,"Mo2 Isotope position");
		TEST(di < 0.01,"Mo2 Isotope intensity");

	}
	//==========

#else
	WARN(false,"No XML support : unable to load isotope data, test skipped");
#endif

	return true;	

}
#endif
}
