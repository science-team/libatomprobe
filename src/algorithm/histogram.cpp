/* 
* histogram.cpp - Precise KD tree implementation
* Copyright (C) 2015  Daniel Haley
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "atomprobe/algorithm/histogram.h"

#include "atomprobe/helper/aptAssert.h"
#include <limits>

using std::vector;

namespace AtomProbe
{


bool incrementCorrelationHist(const std::vector<EPOS_ENTRY> &eposIons,
		std::vector<std::vector<unsigned int> > &histogram,float stepMass,
		float endMass)
{
	#pragma omp parallel for
	for(unsigned int ui=0; ui<eposIons.size();ui++)	
	{
		unsigned int nMultihits;
		nMultihits =eposIons[ui].hitMultiplicity;

		//zero implies not the start of a multihit sequence
		// 1 implies only a single hit
		if(nMultihits  <= 1)
			continue;

		//loop over every possible combination of multi-hit pairings
		// but only the lower-triangular section
		for(unsigned int uj=0;uj<nMultihits; uj++)
		{
			for(unsigned int uk=uj+1;uk<nMultihits; uk++)
			{
				//find the masses of the two hits
				float m1,m2;
				m1 =eposIons[ui+uj].massToCharge;
				m2 =eposIons[ui+uk].massToCharge;
				unsigned int x,y;
				x=m1/stepMass;
				y=m2/stepMass;

				if(x >=histogram.size() || y>=histogram.size())
					continue;

				//Data is not always ordered
				if(x > y)
					std::swap(x,y);
				//increment the histogram.
				// break into two steps to minimise 
				// thread locking time (maybe.)
				unsigned int *v;
				v = &(histogram[x][y]);

				#pragma atomic
				(*v)++;
			}	

		}

		ui+=nMultihits;

	}



}
	

bool correlationHistogram(const std::vector<EPOS_ENTRY> &eposIons,
		std::vector<std::vector<unsigned int> > &histogram,float stepMass,
		float endMass, bool lowerTriangular)
{

	if(stepMass < sqrt(std::numeric_limits<float>::epsilon())
		|| endMass < sqrt(std::numeric_limits<float>::epsilon()) )
		return false;

	const unsigned int nBins = endMass/stepMass;
	histogram.clear();
	histogram.resize(nBins);
	for(unsigned int ui=0;ui<nBins;ui++)
		histogram[ui].resize(nBins,0);

	incrementCorrelationHist(eposIons,histogram,stepMass,endMass);

	if(!lowerTriangular)
	{
		//Loop over the upper-trianglular part of the array
		for(unsigned int ui=0;ui<nBins;ui++)
		{
			for(unsigned int uj=ui+1;uj<nBins;uj++)
			{
				//mirror the histogram, copying from lower-triangular
				// component
				histogram[uj][ui]=histogram[ui][uj];
			}
		}
	}

	return true;
}

bool accumulateCorrelationHistogram(const std::vector<EPOS_ENTRY> &eposIons,
		std::vector<std::vector<unsigned int> > &histogram,float stepMass,
		float endMass, bool lowerTriangular)
{

	if(stepMass < sqrt(std::numeric_limits<float>::epsilon()) 
		|| endMass < sqrt(std::numeric_limits<float>::epsilon()) )
		return false;
	
	const unsigned int nBins = endMass/stepMass;
	if(!histogram.size())
	{
		histogram.resize(nBins);

		for(unsigned int ui=0;ui<nBins;ui++)
			histogram[ui].resize(nBins,0);
	}

	incrementCorrelationHist(eposIons,histogram,stepMass,endMass);

	if(!lowerTriangular)
	{
		//Loop over the upper-trianglular part of the array
		for(unsigned int ui=0;ui<nBins;ui++)
		{
			for(unsigned int uj=ui+1;uj<nBins;uj++)
			{
				//mirror the histogram, copying from lower-triangular
				// component
				histogram[uj][ui]=histogram[ui][uj];
			}
		}
	}


	return true;
}


}
