#include "atomprobe/io/apthdf5.h"

#ifdef HAVE_HDF5
#ifdef EXPERIMENTAL


#include <deque>
#include <utility>
#include <cstring>
#include <map>
#include <cmath>
#include <algorithm>

#include "atomprobe/helper/stringFuncs.h"

#ifdef DEBUG
#include <fstream>
#include <iostream>
#endif

namespace AtomProbe{

using std::string;
using std::vector;
using std::deque;
using std::pair;
using std::make_pair;
using std::map;


using H5::DataSpace;
using H5::DataSet;
using H5::PredType;

class HDFQueueEntry
{
	public:
		hid_t parent;
		hid_t offset;
		unsigned int depth;

		HDFQueueEntry(hid_t par, hid_t off, unsigned int dpth) :parent(par),offset(off),depth(dpth)
		{
		}
};

enum
{
	HDF_TYPE_GROUP,
	HDF_TYPE_DATASET,
	HDF_TYPE_LINK,
	HDF_TYPE_USER_DEFINED_LINK,
	HDF_TYPE_ENUM_END
};

static_assert(std::numeric_limits<double>::is_iec559);

APTHDF5::APTHDF5()
{
	file=0;
}

APTHDF5::~APTHDF5()
{
	if(file)
		delete file;
}

unsigned int APTHDF5::open(const char *fName, unsigned int flag)
{
	//Try to open the file
	try
	{
		file = new H5::H5File(fName,flag);
	}
	catch(...)
	{
		return ERR_HDF_EXCEPTION;
	}

	return 0;
}

bool APTHDF5::validateFieldSizesMatch(const vector<string> &paths, unsigned int &firstMismatch)
{
	if(!paths.size())
		return true;

	firstMismatch=(unsigned int)-1;

	unsigned int dataSize;
	//Obtain the dimensions of the first entry
	{
	DataSet ds;
	ds=file->openDataSet(paths[0].c_str());

	DataSpace dSpace;
	dSpace=ds.getSpace();

	dataSize=dSpace.getSimpleExtentNpoints();
	}

	for(auto ui=1u;ui<paths.size();ui++)
	{
		DataSet ds;
		ds=file->openDataSet(paths[ui].c_str());

		DataSpace dSpace;
		dSpace=ds.getSpace();

		unsigned int tmpSize;
		tmpSize=dSpace.getSimpleExtentNpoints();

		if(tmpSize !=dataSize)
		{
			firstMismatch=ui;
			break;
		}
	}

	if(firstMismatch !=(unsigned int)-1)
		return false;

	return true;

}

bool APTHDF5::fetch1DSelection(DataSet &ds, unsigned int offset, 
		unsigned int n, vector<double> &data)
{
	//Empty vector before we refill
	data.clear();

	DataSpace slabSpace=ds.getSpace();

	//obtain the dimensions of this object
	unsigned int dimsFloat = slabSpace.getSimpleExtentNdims();
	
	if(dimsFloat !=1)
		return false;

	hsize_t dimSizes;
	slabSpace.getSimpleExtentDims(&dimSizes);


	//Check corner is in dataset
	if(offset*n >=dimSizes)
		return false;
	
	hsize_t start=offset*n;

	//Check and then shrink if the data is to small
	if(offset*n + n >=dimSizes)
	{
		//Ensure left most element is in range
		assert(offset*n < dimSizes);
		n=dimSizes-offset*n;
	}

	//Set count,and offset
	hsize_t count;
	count=n; 
	slabSpace.selectHyperslab(H5S_SELECT_SET,&count,&start);


	hsize_t nTmp=n;
	DataSpace memSpace(1,&nTmp);


	data.resize(n);

	ds.read(&(data[0]),getPredType(&(data[0])),
				memSpace,slabSpace);

	return true;

}

bool APTHDF5::validateFieldContents(vector<string> &datasets)
{
	for( const auto &v : datasets)
	{
		//Open dataset
		DataSet ds=file->openDataSet(v);

		//Retrieve type
		H5T_class_t classType =ds.getTypeClass();

		//Validate against type
		switch(classType)
		{
			case H5T_STRING:
			{
				//Check UTF-8 sequence well formed
				std::string s;
				DataSpace dSpace = ds.getSpace();
				auto dType=ds.getDataType();
				ds.read(s,dType);

				//Check UTF-8 validity
				long unsigned int codePoints;
				if(!countCodePoints((unsigned char*)s.c_str(),&codePoints))
					return false;

				break;
			}
			case H5T_FLOAT:
			{
				//Obtain dimensions
				vector<unsigned int> dims = getDimensions(v.c_str());

				//Check 1D data
				if(dims.size() ==1) 
				{
					const unsigned int CHUNK_SIZE=1000000;
					auto nItems=ds.getSpace().getSimpleExtentNpoints();
					auto nFetches = nItems/CHUNK_SIZE;

					vector<double> data;

					for(auto ui=0;ui<nFetches;ui++)
					{
						if(!fetch1DSelection(ds,ui,CHUNK_SIZE,data))
							return false;

						//check data is OK - should not contain nan or inf
						for (auto &val : data)
						{
							if(std::isnan(val) || std::isinf(val))
							{
								return false;
							}
						}
					}
				}
				else
				{
					//FIXME : Check multi-column 
				}


				break;

			}
			case H5T_INTEGER:
			{
				//Do nothing
				break;
			}
			default:
			{
				//Unknown object -- do nothing
				;
			}

		}

		

	}

	return true;
}

bool APTHDF5::isValid(unsigned int minVersion)
{

	if(!file)
		return false;

	const map<string,hid_t> requiredFields={	
						{"/ExperimentContext/SampleDescription", H5T_STRING} ,
						{"/ExperimentContext/ExperimentType", H5T_STRING},
						{"/ExperimentContext/SampleName", H5T_STRING},
						{"/ExperimentContext/SampleUniqueIdentifier", H5T_STRING},
						{"/ExperimentContext/ApertureType", H5T_STRING},
						{"/ExperimentContext/ApertureUniqueIdentifier", H5T_STRING},
						{"/ExperimentContext/Version", H5T_STRING}, 
						{"/ToolEnvironment/ExperimentStartDateUTC", H5T_STRING},
						{"/ToolEnvironment/ExperimentEndDateUTC", H5T_STRING},
						{"/ToolEnvironment/ExperimentStartDateLocal", H5T_STRING},
						{"/ToolStateAndSettings/DetectorGeometryOpticalEquiv", H5T_FLOAT}, 
						{"/ToolStateAndSettings/DetectorType", H5T_STRING},
						{"/ToolStateAndSettings/DetectorSize", H5T_FLOAT},
						{"/ToolStateAndSettings/DetectorReadout", H5T_STRING},
						{"/ToolStateAndSettings/DetectorResolution", H5T_FLOAT}, 
						{"/ToolStateAndSettings/FlightPathSpatial", H5T_FLOAT},
						{"/ToolStateAndSettings/FlightPathTiming", H5T_FLOAT},
						{"/ToolStateAndSettings/InstrumentIdentifier", H5T_STRING},
						{"/ToolStateAndSettings/LaserIncidence", H5T_FLOAT},
						{"/ToolStateAndSettings/LaserWavelength", H5T_FLOAT},
						{"/ToolStateAndSettings/ReflectronInfo", H5T_STRING},
						{"/ToolStateAndSettings/Tip2ReconSpaceMapping", H5T_FLOAT},
						{"/ExperimentResults/LaserPosition", H5T_FLOAT},
						{"/ExperimentResults/LaserEnergy", H5T_FLOAT},
						{"/ExperimentResults/PulseFrequency", H5T_FLOAT},
						{"/ExperimentResults/PulseFraction", H5T_FLOAT},
						{"/ExperimentResults/StandingVoltage", H5T_FLOAT},
						{"/ExperimentResults/ReflectronVoltage", H5T_FLOAT},
						{"/ExperimentResults/TipTemperature", H5T_FLOAT},
						{"/ExperimentResults/StagePosition", H5T_FLOAT},
						{"/ExperimentResults/TimeOfFlight", H5T_FLOAT},
						{"/ExperimentResults/DetectorHitPositions", H5T_FLOAT},
						{"/ExperimentResults/PulseNumber", H5T_INTEGER},
						};

	vector<pair<string,unsigned int> > datasets;
	enumerateDatasets(datasets);

	//Check that each required field exists, and has the correct type
	//----
	for(const auto &v : requiredFields)
	{
		pair<string, unsigned int> p;
		p = make_pair(v.first,v.second);
		if(std::find(datasets.begin(),datasets.end(),p) == datasets.end())
			return false;
	}
	//----


	//Check that the listed fields have the same size
	//----
	// Note, the fields must exist in the call to validateFieldSizesMatch
	vector<string> sameSizeFields = { 
					"/ExperimentResults/PulseNumber",
					"/ExperimentResults/TimeOfFlight", 
				};


	unsigned int mismatchId = -1;
	validateFieldSizesMatch(sameSizeFields,mismatchId);

	if(mismatchId != -1)
		return false;
	//----
	

	vector<string> dSets;
	dSets.reserve(datasets.size());
	for(const auto &v : datasets)
		dSets.push_back(v.first);

	validateFieldContents(dSets);

	//Check the version field makes sense
	{
	const char *VERSION_FIELD="/ExperimentContext/Version";

	DataSet ds=file->openDataSet(VERSION_FIELD);
	auto dType = ds.getDataType();
	string text;
	ds.read(text,dType);


	//Should be YYYY-MM[.X], 
	// year, month, increment.
	bool versionValid=true;
	if(!(text.size()==7 || text.size()==9))
		versionValid=false;

	for(auto ui=0;ui<4;ui++)
	{
		if(!isdigit(text[ui]))
			versionValid=false;
	}
	
	unsigned int versionYear =atoi(text.substr(0,4).c_str());


	if(text[4] != '-')
		versionValid=false;
	for(auto ui=5;ui<7;ui++)
	{
		if(!isdigit(text[ui]))
			versionValid=false;
	}
	unsigned int versionMonth =atoi(text.substr(5,7).c_str());

	if(versionMonth>12)
		return false;

	for(auto ui=5;ui<7;ui++)
	{
		if(!isdigit(text[ui]))
			versionValid=false;
	}
	unsigned int versionRevision=0;
	if(text.size() ==9)
	{
		if(text[7] !='.' || !isdigit(text[8]))
			versionValid=false;
	
		versionRevision=atoi(text.substr(7,9).c_str());
	}

	//Version not valid
	if(!versionValid)
		return false;

	unsigned int compositeVersion = versionYear*1000 +
				versionMonth*10 + versionRevision;
	//version number too low?
	if(compositeVersion < minVersion)
		return false;
	}

	//Check the sample name is short
	{
	const char *SAMPLENAME_FIELD="/ExperimentContext/SampleName";

	DataSet ds=file->openDataSet(SAMPLENAME_FIELD);
	auto dType = ds.getDataType();
	string text;
	ds.read(text,dType);
	if(text.size() > 200)
		return false;
	}
	
	//Check sample identifier size	
	{
	const char *SAMPLEID_FIELD="/ExperimentContext/SampleUniqueIdentifier";

	DataSet ds=file->openDataSet(SAMPLEID_FIELD);
	auto dType = ds.getDataType();
	string text;
	ds.read(text,dType);
	if(text.size() > 100)
		return false;
	}
	
	//Check aperture max identifier size
	{
	const char *APERTUREID_FIELD="/ExperimentContext/ApertureUniqueIdentifier";

	DataSet ds=file->openDataSet(APERTUREID_FIELD);
	auto dType = ds.getDataType();
	string text;
	ds.read(text,dType);
	if(text.size() > 100)
		return false;
	}
	return true;
}



H5::H5File *APTHDF5::getFilePtr()
{
	return file;
}

bool APTHDF5::enumerateDatasets(vector<pair<string, unsigned int>  > &datasets) const
{
	assert(file);

	datasets.clear();

	//Maximum length of any item within the file
	const unsigned int BUF_MAX=8192;

	try
	{
		hid_t fileID = file->getId();

		//Open the base "Group".
		auto baseGroup = file->openGroup("/");

		//Queue up the objects in this group
		deque<pair<HDFQueueEntry,string> > objQueue;
		for(auto ui=0u;ui<baseGroup.getNumObjs();ui++)
			objQueue.push_back(make_pair(HDFQueueEntry(fileID,ui,0),"/"));
		char *buffer = new char[BUF_MAX]; //FIXME: Use scoped pointer, for exception saftey

		//Traverse the directory heirarchy, 
		// - enumerate objects to console
		// - mark off required objects
		vector<string> groupStack;
		while(objQueue.size())
		{
			HDFQueueEntry entry = objQueue.front().first;
			string objPath=objQueue.front().second;
			objQueue.pop_front();

			//FIXME: Apparently this has been deprecated. However, the replacement
			//functon H5L_GET_NAME_BY_IDX requires a name, which we don't have
			ssize_t size;
			size=H5Gget_objname_by_idx(entry.parent,
					entry.offset,buffer,BUF_MAX) ; 


			//Get the object type
			int type;
			type=H5Gget_objtype_by_idx(entry.parent,entry.offset);

			string fullPath;
			fullPath=objPath + buffer;


			H5T_class_t classType;
			//If it is a group (analagous to a directory), expand it
			switch(type)
			{
				case HDF_TYPE_GROUP:
				{
					//Open group
					hid_t locId = H5Gopen(entry.parent,buffer,H5P_DEFAULT);
		
					//Queue up the objects in the group.	
					hsize_t numObjects;
					if(H5Gget_num_objs(locId,&numObjects)>=0)
					{
						//Push each new object found on, recording the queue entry, and the full path
						for(auto ui=0u;ui<numObjects;ui++)	
							objQueue.push_front( make_pair(HDFQueueEntry(locId,ui,entry.depth+1),
										objPath + string(buffer) + string("/")) );
					}

					break;
				}
				case HDF_TYPE_DATASET:
				{
					hsize_t dims;
					H5T_class_t classId;
					size_t typeSize;
					if(H5LTget_dataset_info(entry.parent,buffer,
							&dims,&classId,&typeSize) < 0)
						break;

					hid_t datasetId = H5Dopen(entry.parent,buffer,H5P_DEFAULT);
					if(datasetId <0)
						break;
					DataSet dataSet(datasetId);
					classType=dataSet.getTypeClass();
					datasets.push_back(make_pair(fullPath,(unsigned int)classType));
				}
				default:
					//Do-nothing
					;
			}

		}

		delete[] buffer;

	
	}
	catch(std::exception &e)
	{
		return false;
	}

	return true;
}


unsigned int APTHDF5::hasDataset(const char *datasetName) const
{
	vector<pair<string, unsigned int> > dataNames;

	if(!enumerateDatasets(dataNames))
		return -1;

	for(auto v : dataNames)
	{
		if(strcmp(v.first.c_str(),datasetName))
			return 1;
	}

	return 0;
}

unsigned int APTHDF5::getGroupDataType(const char *datasetName) const
{
	vector<pair<string, unsigned int> > dataNames;

	if(!enumerateDatasets(dataNames))
		return (unsigned int)-1;

	for(auto v : dataNames)
	{
		if(strcmp(v.first.c_str(),datasetName))
			return v.second;
	}


	return (unsigned int)-1;
}

std::vector<unsigned int> APTHDF5::getDimensions(const char *groupName) const
{
	assert(file);

	vector<unsigned int> dimensions;
	DataSet ds;
	try{
	ds=file->openDataSet(groupName);
	}
	catch(std::exception &e)
	{
		return dimensions;
	}
	catch(...)
	{
		return dimensions;
	}
	//Obtain slab
	DataSpace slabSpace=ds.getSpace();
	//Find dimensionality
	unsigned int dims= slabSpace.getSimpleExtentNdims();


	//Find size of each dimension
	//hsize_t dimV;
	hsize_t *dimV = new hsize_t[dims];
        slabSpace.getSimpleExtentDims(dimV);

	//Copy to output vector
	dimensions.resize(dims);
	for(auto ui=0;ui<dims; ui++)
		dimensions[ui]  =dimV[ui];

	delete[] dimV;

	return dimensions;
}

bool APTHDF5::getStringData(const char *groupName,std::string &data ) const
{
	assert(file);

	DataSet ds;
	try{
        ds=file->openDataSet(groupName);
	}
	catch(std::exception &e)
	{
		return false;
	}
	catch(...)
	{
		return false;
	}

        auto dType = ds.getDataType();                                                                   
        ds.read(data,dType);
	
	return true;
}



bool APTHDF5::getDatasetParams( const char *groupName, unsigned int &dimsData,
		DataSet &ds,DataSpace &slabSpace, DataSpace* &memSpace,
		hsize_t* &count, hsize_t* &dims
		) const
{
	assert(file);


	try{
        ds=file->openDataSet(groupName);
	}
	catch(std::exception &e)
	{
		return false;
	}
	catch(...)
	{	
		return false;
	}

	slabSpace=ds.getSpace();

	//obtain the dimensions of this object
	dimsData = slabSpace.getSimpleExtentNdims();

	//Function is capped to data sizes of max dimension 2
	if(dimsData >2)
		return false;

	//Obtain the size of each dimension
	slabSpace.getSimpleExtentDims(dims);

	count[0] =1;
	memSpace=0;
	if(dimsData >1)
	{
		memSpace = new DataSpace(1,&dims[1]);
		count[1]=dims[1];
	}
	else
		memSpace=new DataSpace(1,&dims[0]);

	return true;
}

bool APTHDF5::getRealData(const char *groupName,
		std::vector<std::vector<float> > &data) const
{
	DataSet ds;
	DataSpace slabSpace,*memSpace;
	hsize_t count[2],dims[2];

	hsize_t *cPtr=count;
	hsize_t *dPtr=dims;

	unsigned int dimsData;
	if(!getDatasetParams(groupName,dimsData,ds,
				slabSpace,memSpace,cPtr, dPtr ))
		return false;

	//FIXME: Do we want a 1D dataset to be one
	// single vector, or split into many small vectors of one element?
	data.resize(dims[0]);

	//Read each row into a vector
	hsize_t start[2];
	start[0]=start[1]=0;
	for(auto &v : data)
	{
		v.resize(dims[1]);
		slabSpace.selectHyperslab(H5S_SELECT_SET,count,start);

		//read row
		ds.read(&(v[0]),PredType::NATIVE_FLOAT,
					*memSpace,slabSpace);
	
		//increment start
		start[0]++;
	}

	delete memSpace;

	return true;
}

bool APTHDF5::getUIntegerData(const char *groupName,
		std::vector<std::vector<uint64_t > > &data) const
{
	DataSet ds;
	DataSpace slabSpace,*memSpace;
	hsize_t count[2],dims[2];

	hsize_t *cPtr=count;
	hsize_t *dPtr=dims;

	unsigned int dimsData;
	if(!getDatasetParams(groupName,dimsData,ds,
				slabSpace,memSpace,cPtr, dPtr ))
		return false;

	//FIXME: Do we want a 1D dataset to be one
	// single vector, or split into many small vectors of one element?
	data.resize(dims[0]);

	//Read each row into a vector
	hsize_t start[2];
	start[0]=start[1]=0;
	for(auto &v : data)
	{
		v.resize(dims[1]);
		slabSpace.selectHyperslab(H5S_SELECT_SET,count,start);

		//read row
		ds.read(&(v[0]),PredType::NATIVE_INT64,
					*memSpace,slabSpace);
	
		//increment start
		start[0]++;
	}

	delete memSpace;

	return true;
}

bool APTHDF5::writeData(const std::string &data, const char *groupName)
{
	assert(file);
	assert(groupName);

	//This is a little silly, we have to "copy" the type, and change its size for every string.
        // Not sure if its possible to store strings of different sizes
        hsize_t dims=1;

        hid_t strType=H5Tcopy(H5T_C_S1);
        H5Tset_size (strType, H5T_VARIABLE);


        DataSpace dataSpace(1,&dims);
        DataSet *dString = new DataSet(file->createDataSet(groupName,strType,dataSpace));

	char *cData = new char[data.size()+1];

	strcpy(cData,data.c_str());



	H5Dwrite(dString->getId(), strType, dataSpace.getId(), 
			H5S_ALL, H5P_DEFAULT,&cData); 

	delete[] cData;

	delete dString;

	return true;
}




#ifdef DEBUG
#define TEST(f) { if(!(f))  return false; }

bool hdfFloatTest()
{
	const char *GROUPNAME="group";
	vector<double> vDataIn = { 1,2,3 };
	{
	APTHDF5 hFile;
	if(hFile.open("test.hdf5",H5F_ACC_TRUNC))
		return false;

	hFile.writeData(vDataIn, GROUPNAME);
	}

	//Now attempt to read 
	{
	APTHDF5 hFile;
	if(hFile.open("test.hdf5",H5F_ACC_RDONLY))
		return false;

	auto vDim = hFile.getDimensions(GROUPNAME);

	TEST(vDim.size() <=2 && vDim[0] == 3); 


	vector<vector<float> > vDataRead;

	TEST(hFile.getRealData(GROUPNAME,vDataRead));

	if(vDim.size() == 2)
	{
		TEST(vDataRead.size() == vDim[0]);
		TEST(vDataRead[0].size() == vDim[1]);

		for(auto ui=0;ui<vDim[0];ui++)
		{
			TEST(vDataIn[ui] == vDataRead[ui][0]);
		}
	}





	
	}
	return true;
}

bool runAPTHDF5Tests()
{
	const char *GROUPNAME="groupInt";

	if(!hdfFloatTest())
		return false;

	//ReadWrite test for uintegers
	{
	vector<unsigned int> vDataIn = { 1,2,3 };
	{
	APTHDF5 hFile;
	if(hFile.open("test.hdf5",H5F_ACC_TRUNC))
		return false;

	hFile.writeData(vDataIn, GROUPNAME);
	}

	//Now attempt to read 
	{
	APTHDF5 hFile;
	if(hFile.open("test.hdf5",H5F_ACC_RDONLY))
		return false;

	auto vDim = hFile.getDimensions(GROUPNAME);

	TEST(vDim.size() <=2 && vDim[0] == 3); 


	vector<vector<uint64_t> > vDataRead;

	TEST(hFile.getUIntegerData(GROUPNAME,vDataRead));

	if(vDim.size() == 2)
	{
		TEST(vDataRead.size() == vDim[0]);
		TEST(vDataRead[0].size() == vDim[1]);

		for(auto ui=0;ui<vDim[0];ui++)
		{
			TEST(vDataIn[ui] == vDataRead[ui][0]);
		}
	}





	
	}
	}

	//ReadWrite test for string
	{
	//Write
	string strData= {"Lorem ipsum dolor sit amet"};
	{
	{
	APTHDF5 hFile;
	if(hFile.open("test.hdf5",H5F_ACC_TRUNC))
		return false;

	hFile.writeData(strData, GROUPNAME);
	}

	}
	

	//Read
	{
	APTHDF5 hFile;
	if(hFile.open("test.hdf5",H5F_ACC_RDONLY))
		return false;

	string str;
	TEST(hFile.getStringData(GROUPNAME,str));

	TEST(str ==strData);
	}

	}


	//Open test data file
	{
	APTHDF5 hFile;

	const char *FILE="../testdata/201125-valid.apth5";
	
	//Check file exists first, before attempting to load it as an HDF file
	std::ifstream f;
	f.open(FILE);
	if(f)
	{
		f.close();
		if(hFile.open(FILE,H5F_ACC_RDONLY))
			return false;


		if(!hFile.isValid())
			return false;
	}
	else
		std::cerr << "WARN: HDF file missing" << FILE << std::endl;
	}	


	return true;
}
#endif

}

#endif
#endif

