/* 
 * convert.cpp: Inter-datatype conversion routines
 * Copyright (C) 2018  D Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <iostream>
#include <cstdlib>

#include "atomprobe/primitives/ionHit.h"


using namespace AtomProbe;


void convertEPOStoPos(const std::vector<EPOS_ENTRY> &eposEntry, 
					std::vector<IonHit> &posFile)
{
	posFile.resize(eposEntry.size());
#pragma omp parallel for
	for(auto ui=0u;ui<eposEntry.size();ui++)
		posFile[ui]=eposEntry[ui].getIonHit();
}
