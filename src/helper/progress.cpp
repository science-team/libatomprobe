/* 
 * progress.cpp: Simple progress bar
 * Copyright (C) 2015  D Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "atomprobe/helper/progress.h"

#include <iostream>
#include <cstdlib>

using std::endl;
using std::cerr;

namespace AtomProbe{

ProgressBar::~ProgressBar()
{
	if(printEnd)
		finish();	
}

void ProgressBar::init()
{
	cerr << endl << "|" ;
	for(size_t ui=0;ui<length;ui++)
		cerr << "-";
	cerr << "|" << endl <<"|";
}

void ProgressBar::reset()
{
	accumulatedTicks=0;
	lastProg=0;
	length=100;
	printEnd=true;
}


void ProgressBar::finish()
{
	update(100);
}


void ProgressBar::update(unsigned int progress)
{
	if(!printEnd)
		return;
	unsigned int delta;

	delta = progress-lastProg;
	lastProg=progress;

	float ticks  = ((float)delta*(float)length/100.0f);
	accumulatedTicks+=ticks;

	if(accumulatedTicks >=1.0f)
	{	

		delta=accumulatedTicks;
		accumulatedTicks-=delta;

		while(delta--) 
			cerr << ".";
	}

	if(progress >= 100)
	{
		cerr << "| Done." << endl;
		printEnd=false;
	}
}

#ifdef DEBUG
bool testProgressBar()
{
	ProgressBar pb;
	pb.setLength(50);

	pb.init();
	pb.update(24);
	pb.finish();

	return true;
}
#endif
}
