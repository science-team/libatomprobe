/*
 *	stringFuncs.cpp - string manipulation routines
 *	Copyright (C) 2014, D Haley 

 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.

 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.

 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "atomprobe/helper/stringFuncs.h"

#include "atomprobe/helper/aptAssert.h"


#include <fstream>
#include <ctime>

namespace AtomProbe
{

using std::string;
using std::vector;

std::string onlyFilename( const std::string& path) 
{
#if defined(_WIN32) || defined(_WIN64)
	//windows uses "\" as path sep, just to be different
	return path.substr( path.find_last_of( '\\' ) +1 );
#else
	//The entire world, including the interwebs, uses  "/" as the path sep.
	return path.substr( path.find_last_of( '/' ) +1 );
#endif
}

std::string onlyDir( const std::string& path) 
{
#if defined(_WIN32) || defined(_WIN64)
	//windows uses "\" as path sep, just to be different
	return path.substr(0, path.find_last_of( '\\' ) +1 );
#else
	//The entire world, including the interwebs, uses  "/" as the path sep.
	return path.substr(0, path.find_last_of( '/' ) +1 );
#endif
}

void nullifyMarker(char *buffer, char marker)
{
	while(*(buffer))
	{
		if(*buffer == marker)
		{
			*buffer='\0';
			break;
		}
		buffer++;
	}
}

void ucharToHexStr(unsigned char c, std::string &s)
{
	s="  ";
	char h1,h2;

	h1 = c/16;
	h2 = c%16;

	if(h1 < 10)
	{
		s[0]=(h1+'0');
	}
	else
		s[0]=((h1-10)+'a');

	if(h2 < 10)
	{
		s[1]=(h2+'0');
	}
	else
		s[1]=((h2-10)+'a');
	
}

void hexStrToUChar(const std::string &s, unsigned char &c)
{
	ASSERT(s.size() ==2);

	ASSERT((s[0] >= '0' && s[0] <= '9') ||
	      		(s[0] >= 'a' && s[0] <= 'f'));
	ASSERT((s[1] >= '0' && s[1] <= '9') ||
			(s[1] >= 'a' && s[1] <= 'f'));

	int high,low;
	if(s[0] <= '9' && s[0] >='0')
		high = s[0]-(int)'0';
	else
		high = s[0] -(int)'a' + 10;	
	
	if(s[1] <= '9' && s[1] >='0')
		low = s[1]-(int)'0';
	else
		low = s[1] -(int)'a' + 10;	

	c = 16*high + low;
}

std::string digitString(unsigned int thisDigit, unsigned int maxDigit)
{
	std::string s,thisStr;
	stream_cast(thisStr,thisDigit);

	stream_cast(s,maxDigit);
	for(unsigned int ui=0;ui<s.size();ui++)
		s[ui]='0';


	s=s.substr(0,s.size()-thisStr.size());	

	return  s+thisStr;
}


//Strip "whitespace"
std::string stripWhite(const std::string &str)
{
	return stripChars(str,"\f\n\r\t ");
}

std::string stripChars(const std::string &str, const char *chars)
{
	using std::string;

	size_t start;
	size_t end;
	if(!str.size())
	      return str;

 	start = str.find_first_not_of(chars);
	end = str.find_last_not_of(chars);
	if(start == string::npos) 
		return string("");
	else
		return string(str, start, 
				end-start+1);
}

void stripZeroEntries(std::vector<std::string> &sVec)
{
	//Create a truncated vector and reserve mem.
	std::vector<std::string> tVec;
	tVec.reserve(sVec.size());
	std::string s;
	for(unsigned int ui=0;ui<sVec.size(); ui++)
	{
		//Only copy entries with a size.
		if(sVec[ui].size())
		{
			//Push dummy string and swap,
			// to avoid copy
			tVec.push_back(s);
			tVec.back().swap(sVec[ui]);
		}
	}
	//Swap out the truncated vector with the source
	tVec.swap(sVec);
}	

std::string lowercase(std::string s)
{
	for(unsigned int ui=0;ui<s.size();ui++)
	{
		if(isalpha(s[ui]) && isupper(s[ui]))
			s[ui] = tolower(s[ui]);
	}
	return s;
}

//Split strings around a delimiter
void splitStrsRef(const char *cpStr, const char delim,std::vector<string> &v )
{
	const char *thisMark, *lastMark;
	string str;
	v.clear();

	//check for null string
	if(!*cpStr)
		return;
	thisMark=cpStr; 
	lastMark=cpStr;
	while(*thisMark)
	{
		if(*thisMark==delim)
		{
			str.assign(lastMark,thisMark-lastMark);
			v.push_back(str);
		
			thisMark++;
			lastMark=thisMark;
		}
		else
			thisMark++;
	}

	if(thisMark!=lastMark)
	{
		str.assign(lastMark,thisMark-lastMark);
		v.push_back(str);
	}	
		
}

//Split strings around any of a string of delimiters
void splitStrsRef(const char *cpStr, const char *delim,std::vector<string> &v )
{
	const char *thisMark, *lastMark;
	string str;
	v.clear();

	//check for null string
	if(!(*cpStr))
		return;
	thisMark=cpStr; 
	lastMark=cpStr;
	while(*thisMark)
	{
		//Loop over possible delimiters to determine if this char is a delimiter
		bool isDelim;
		const char *tmp;
		tmp=delim;
		isDelim=false;
		while(*tmp)
		{
			if(*thisMark==*tmp)
			{
				isDelim=true;
				break;
			}
			tmp++;
		}
		
		if(isDelim)
		{
			str.assign(lastMark,thisMark-lastMark);
			v.push_back(str);
		
			thisMark++;
			lastMark=thisMark;
		}
		else
			thisMark++;
	}

	if(thisMark!=lastMark)
	{
		str.assign(lastMark,thisMark-lastMark);
		v.push_back(str);
	}	
		
}

bool parseColString(const std::string &str,
	unsigned char &r, unsigned char &g, unsigned char &b, unsigned char &a)
{
	//Input string is in 2 char hex form, 3 or 4 colour, with # leading. RGB order
	//lowercase string.
	if(str.size() != 9 && str.size() != 7)
		return false;

	if(str[0] != '#')
		return false;

	string rS,gS,bS,aS;
	rS=str.substr(1,2);
	gS=str.substr(3,2);
	bS=str.substr(5,2);

	if(!isxdigit(rS[0]) || !isxdigit(rS[1]))
		return false;
	if(!isxdigit(gS[0]) || !isxdigit(gS[1]))
		return false;
	if(!isxdigit(bS[0]) || !isxdigit(bS[1]))
		return false;

	hexStrToUChar(str.substr(1,2),r);	
	hexStrToUChar(str.substr(3,2),g);	
	hexStrToUChar(str.substr(5,2),b);	
	//3 colour must have a=255.
	if(str.size() == 7)
		a = 255;
	else
	{
		aS=str.substr(7,2);
		if(!isxdigit(aS[0]) || !isxdigit(aS[1]))
			return false;
		hexStrToUChar(str.substr(7,2),a);	
	}
	return true;
}

void genColString(unsigned char r, unsigned char g, 
			unsigned char b, std::string &s)
{
	string tmp;
	s="#";
	ucharToHexStr(r,tmp);
	s+=tmp;
	ucharToHexStr(g,tmp);
	s+=tmp;
	ucharToHexStr(b,tmp);
	s+=tmp;
}

// https://stackoverflow.com/questions/26895428/how-do-i-parse-an-iso-8601-date-with-optional-milliseconds-to-a-struct-tm-in-c
// TODO: Replace with C++20 parse when C++20 well supported
#if !defined(_WIN32) && !defined(_WIN64)
bool parseISO8601(const std::string& input,std::time_t &result)
{
	//Disallow extended format
	if(input.find_first_of(":-") !=std::string::npos)
		return false;

	//Should be 20 chars long
	if (input.length() < 20)
		return false;

	using std::strtol;
	std::tm time = { 0 };
	time.tm_year = strtol(&input[0],nullptr,10) - 1900;
	time.tm_mon = strtol(&input[5],nullptr,10) - 1;
	time.tm_mday = strtol(&input[8],nullptr,10);
	time.tm_hour = strtol(&input[11],nullptr,10);
	time.tm_min = strtol(&input[14],nullptr,10);
	time.tm_sec = strtol(&input[17],nullptr,10);
	time.tm_isdst = 0;

	//Check for optional milliseconds
	const int millis = input.length() > 20 ? strtol(&input[20],nullptr,10) : 0;
        result=timegm(&time) * 1000 + millis;

	return true;
}
#endif

//this function will reject strings that are not ISO8601, but may not be strict enough
// does not accept sub-second values. Reduced accuracy dates are not permitted
// Ordinal dates not permitted
bool isISO8601(const std::string &s)
{
	//Too small or too big.
	if(s.size() < 13 || s.size()  >25)
		return false;

	std::string normalised;
	normalised=s;
	
	bool parseAsExtended=false;
	for(auto c : s)
	{
		//Check is a valid identifier
		if(!(isdigit(c) || c == 'T' || c == 'Z' || c == '-' || c == '+' || c == ':'))
			return false;

		if(c == '-' || c == ':')
			parseAsExtended=true;
	}

	//Must contain time/date delimiter
	auto pos = s.find('T');
	if(pos == std::string::npos || pos== s.size()-1)
		return false;


	string date,time;
	date =s.substr(0,pos);
	time = s.substr(pos +1);

	//Validate and convert extended back to regular
	if(parseAsExtended)
	{
		//Validate date and normalise
		if(date.size() > 10 || date.size() < 8)
			return false;

		if(date[4] != '-')
			return false;

		//normalise back to non-extended	
		string tmp;
		for(auto c : date)
		{
			if(c == '-')
				continue;
			tmp+=c;

		}

		date=tmp;

		if(time.size() > 14 || time.size() < 4)
			return false;
		tmp.clear();
		for(auto c : time)
		{
			if(c == ':')
				continue;
			tmp+=c;
		}
		time=tmp;	

	}


	//Parse date
	{
	if(date.size() < 7 || date.size() > 8)
		return false;

	//Check is numerical
	for(auto c : date)
	{
		if(!isdigit(c))
			return false;
	}
	
	//strip year, no need to validate [0000 - 9999] is valid. 
	// we will by "mutual agreement" accept 0000-1582 (see standard which only permits this under some cases)
	string year = date.substr(0,4);
	date=date.substr(4);

	if(date.size() ==3)
	{
		//ordinal date - reject
		//this is permitted by the standard, but we disallow it for simplicity
		return false;
	}

	string month,day;
	month=date.substr(0,2);
	day = date.substr(2);


	int monthInt,dayInt,yearInt;
	stream_cast(monthInt,month);
	stream_cast(dayInt,day);
	stream_cast(yearInt,year);

	if(!monthInt || monthInt > 12)
		return false;

	if(!dayInt || dayInt > 31)
		return false;

	//Validate calendar months to respective day lengths
	switch(monthInt)
	{
		case 2: //feb
		{
			bool isLeap;
			if(yearInt%4)
				isLeap=false;
			else if(!(yearInt%100))
				isLeap=true;
			else
			{
				isLeap=!(yearInt%400);
			}


			if(isLeap)
			{
				if( dayInt > 29)
					return false;
			}
			else
				if(dayInt > 28)
					return false;

			break;
		}
		case 4:
		case 6:
		case 9:
		case 11:
		{
			if(dayInt>30)
				return false;
			break;
		}
		default:
			; //pass
	}	
	}

	//Parse time
	{
		//Leading two digits should be time
		unsigned int hrsInt,minsInt;
		stream_cast(hrsInt,time.substr(0,2));
		stream_cast(minsInt,time.substr(2,2));

		if(hrsInt > 24)
			return false;

		if(minsInt > 59)
			return false;



	}

	return true;
}


// =====================
// Copyright (c) 2008-2009 Bjoern Hoehrmann <bjoern@hoehrmann.de>
// See http://bjoern.hoehrmann.de/utf-8/decoder/dfa/ for details.

const unsigned int UTF8_ACCEPT=0;
const unsigned int UTF8_REJECT=1;

static const uint8_t utf8d[] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 00..1f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 20..3f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 40..5f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 60..7f
  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, // 80..9f
  7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7, // a0..bf
  8,8,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, // c0..df
  0xa,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x4,0x3,0x3, // e0..ef
  0xb,0x6,0x6,0x6,0x5,0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8, // f0..ff
  0x0,0x1,0x2,0x3,0x5,0x8,0x7,0x1,0x1,0x1,0x4,0x6,0x1,0x1,0x1,0x1, // s0..s0
  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,1, // s1..s2
  1,2,1,1,1,1,1,2,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1, // s3..s4
  1,2,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,3,1,1,1,1,1,1, // s5..s6
  1,3,1,1,1,1,1,3,1,3,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1, // s7..s8
};

uint32_t inline
decodeUnicode(uint32_t* state, uint32_t* codep, uint32_t byte) {
  uint32_t type = utf8d[byte];

  *codep = (*state != UTF8_ACCEPT) ?
    (byte & 0x3fu) | (*codep << 6) :
    (0xff >> type) & (byte);

  *state = utf8d[256 + *state*16 + type];
  return *state;
}

bool countCodePoints(const uint8_t* s, size_t* count) {
  uint32_t codepoint;
  uint32_t state = 0;

  for (*count = 0; *s; ++s)
    if (!decodeUnicode(&state, &codepoint, *s))
      *count += 1;

  return state == UTF8_ACCEPT;
}

// =====================

}


