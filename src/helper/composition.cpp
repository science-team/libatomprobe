/* composition.cpp: Composition calculation
 * Copyright (C) 2020  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "atomprobe/helper/composition.h"

#include <cstdlib>

#include "atomprobe/helper/aptAssert.h"
#include "helper/helpFuncs.h"

namespace AtomProbe 
{
using std::vector;

//Take count information and convert it to compositional basis
void computeComposition(const vector<unsigned int> &countData, vector<float> &compositionData)
{
	unsigned int totalCount=0;
	for(size_t ui=0;ui<countData.size();ui++)
		totalCount+=countData[ui];

	compositionData.resize(countData.size(),0);
	float factor = 1.0f/totalCount;
	for(size_t uj=0;uj<countData.size();uj++)
		compositionData[uj] = factor*countData[uj];
}		

#ifdef DEBUG
bool testComposition()
{
	vector<unsigned int> nCounts = { 3, 7, 2};

	vector<float> compData;
	computeComposition(nCounts,compData);

	TEST_Q(TOL_EQ(compData[0] == 3.0/12.0,0.01));
	TEST_Q(TOL_EQ(compData[1] == 7.0/12.0,0.01));
	TEST_Q(TOL_EQ(compData[2] == 2.0/12.0,0.01));
		
	return true;
}
#endif

}


