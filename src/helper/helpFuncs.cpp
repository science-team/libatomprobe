/* helpFuncs.cpp : Helper functions for libatomprobe
 * Copyright (C) 2014  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cstring>
#include <algorithm>
#include <fstream>
#include <gsl/gsl_linalg.h>

#include "atomprobe/helper/aptAssert.h"

#include "helpFuncs.h"

//Needed for stat call on posix systems
#if !defined(__WIN32__) && !defined(__WIN64__)
#include <sys/types.h>
#include <sys/stat.h>
#endif
namespace AtomProbe
{
//Statically store the previous locale data, 
// so that we can use push/pop locale
static char *oldLocaleStatic;
static int localeStaticType;


char *myStrDup(const char *s)
{
	unsigned int bufSize=strlen(s)+1;
	const unsigned int BUF_MAX=10000;	
	
	if(bufSize > BUF_MAX)
		return 0;

	char *buf =(char *)malloc(bufSize);
	strncpy(buf,s,bufSize); 

	return buf;
}

#if defined(__CYGWIN__) || defined(__MINGW__)
int isascii(int c)
{
	return c < 128 && c > 0;
}
#endif 



void pushLocale(const char *newLocale, int type)
{
	ASSERT(!oldLocaleStatic);
	ASSERT(!localeStaticType);

	ASSERT(type == LC_NUMERIC || type == LC_MONETARY || type == LC_CTYPE 
		|| type == LC_COLLATE || type == LC_ALL || type == LC_TIME);
		

	oldLocaleStatic=setlocale(type,NULL);   

	//setlocale reserves the right to trash the returned pointer      
	// on subsequent calls (i.e. use the returned pointer for later)
	// thus we must duplicate the pointer to own it
	oldLocaleStatic=myStrDup(oldLocaleStatic);     
	if(!oldLocaleStatic)
	{
		localeStaticType=-1;
		return;
	}	
 
	if(strcmp(oldLocaleStatic,newLocale)) 
	{
		setlocale(type,newLocale);        
		localeStaticType=type;
	}
	else
	{
		//record that we did not set this
		localeStaticType=-1;
	}

}

void popLocale()
{
	if(localeStaticType != -1)
		setlocale(localeStaticType,oldLocaleStatic);

	localeStaticType=0;

	free(oldLocaleStatic);
	oldLocaleStatic=0;
}

bool getFilesize(const char *fname, size_t  &size)
{
	std::ifstream f(fname,std::ios::binary);

	if(!f)
		return false;

	f.seekg(0,std::ios::end);

	size = f.tellg();

	return true;
}


void gsl_print_matrix(const gsl_matrix *m)
{
	for (size_t i = 0; i < m->size1; i++)
	{
		for (size_t j = 0; j < m->size2; j++)
			printf("%g ", gsl_matrix_get(m, i, j));

		printf("\n");
	}

}

void gsl_print_vector(const gsl_vector *v)
{
	for (size_t i = 0; i < v->size; i++)
	{
		printf("%g ", gsl_vector_get(v, i));
	}

}

float gsl_determinant(const gsl_matrix *m)
{
	//Matrix must be square
	ASSERT(m->size1==m->size2);

	//Create storage
	gsl_permutation * p = gsl_permutation_alloc (m->size1);
	gsl_matrix *tmpM=gsl_matrix_alloc(m->size1,m->size2);

	//Duplicate, as LU destroys input
	gsl_matrix_memcpy(tmpM,m);

	//Perform decomposition
	int sign;
	gsl_linalg_LU_decomp (tmpM, p, &sign);
	//Compute determinant.
	float d=gsl_linalg_LU_det(tmpM,sign);

	//Free storage
	gsl_matrix_free(tmpM);
	gsl_permutation_free(p);

	return d;
}

#if !defined(__WIN32__) && !defined(__WIN64)
//Confirm that the selected file is not a directory (posix-like systems only)
bool isNotDirectory(const char *filename)
{
	struct stat statbuf;

	if(stat(filename,&statbuf) == -1)
		return false;

	return (statbuf.st_mode !=S_IFDIR);
}
#endif
}
