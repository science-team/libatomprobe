/*
 *	sampling.cpp - statistical sampling functions
 *	Copyright (C) 2015, D Haley 

 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.

 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.

 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "atomprobe/helper/aptAssert.h"

#include "atomprobe/helper/sampling.h"

#include "gsl/gsl_randist.h"


namespace AtomProbe 
{

using std::vector;
void sampleIons(const vector<IonHit> &ions, float sampleFactor, 
			vector<IonHit> &sampled, bool strongRandom)
{
	//obtain library random number generator, if we will use it
	// otherwise, sampling function will use its own weak generator
	gsl_rng *r;
	if(strongRandom)
		r=randGen.getRng();
	else
		r=0;

	size_t nSamples =sampleFactor*ions.size(); 
	sampled.resize(nSamples);
	randomSelect(sampled,ions,nSamples,r);
}

#ifdef DEBUG
bool testSampling()
{
	vector<IonHit> ions,sampled;
	IonHit h(Point3D(1.0f,2.0f,3.0f),1.0f);
	for(unsigned int ui=0;ui<10;ui++)
		ions.push_back(h);

	sampleIons(ions,0.5,sampled);

	TEST(sampled.size() == 5,"sample count");
	for(unsigned int ui=0;ui<sampled.size();ui++)
	{
		TEST(sampled[ui] == h,"sampled data integrity");
	}


	//Try again with strong randomsiation
	sampled.clear();
	sampleIons(ions,0.5,sampled,true);
	TEST(sampled.size() == 5,"sample count");
	for(unsigned int ui=0;ui<sampled.size();ui++)
	{
		TEST(sampled[ui] == h,"sampled data integrity");
	}

	return true;
}
#endif

}
