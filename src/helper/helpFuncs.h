/* helpFuncs.h: Helper functions for libatomprobe
 * Copyright (C) 2014  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATOMPROBE_HELPFUNCS_H
#define ATOMPROBE_HELPFUNCS_H

#include <cstdio>
#include <cmath>

#include "atomprobe/helper/aptAssert.h"

#include <gsl/gsl_blas.h>

namespace AtomProbe
{
#ifndef XOR
#define XOR(a,b) ((!(a)) ^ (!(b)))
#endif

#ifndef ARRAYSIZE
#define ARRAYSIZE(f) (sizeof (f) / sizeof(*f))
#endif

#if defined(__CYGWIN__) || defined(__MINGW__)
	int isascii(int c);
#endif

//Set new locale code. Must be followed by a popLocale call before completion
// Only one locale type can be pushed at a time this way
void pushLocale(const char *newLocale, int type);

//Restore old locale code
void popLocale();

class ComparePairFirst
{
	public:
	template<class T1, class T2>
	bool operator()(const std::pair<  T1, T2 > &p1, const std::pair<T1,T2> &p2) const
	{
		return p1.first< p2.first;
	}
};

class ComparePairSecond
{
	public:
	template<class T1, class T2>
	bool operator()(const std::pair<  T1, T2 > &p1, const std::pair<T1,T2> &p2) const
	{
		return p1.second< p2.second;
	}
};

//C file peek function
inline int fpeek(FILE *stream)
{
	int c;

	c = fgetc(stream);
	ungetc(c, stream);

	return c;
}

class ComparePairFirstReverse
{
	public:
	template<class T1, class T2>
	bool operator()(const std::pair<  T1, T2 > &p1, const std::pair<T1,T2> &p2) const
	{
		return p1.first> p2.first;
	}
};

bool getFilesize(const char *fname, size_t  &size);

//Print a GSL matrix to stdout
void gsl_print_matrix(const gsl_matrix *m);

//Print a GSL vector to stdout
void gsl_print_vector(const gsl_vector *m);

//Compute the determinant of a matrix
float gsl_determinant(const gsl_matrix *m);



#if !defined(__WIN32__) && !defined(__WIN64)
//Confirm that the selected file is not a directory (posix-like systems only)
bool isNotDirectory(const char *filename);
#endif


#ifdef DEBUG
#ifndef TOL_EQ
#define TOL_EQ(a,b) ( fabs(a-b) < 1e-4)
#endif

#define TOL_EQ_V(f,g,h) (fabs( (f) - (g)) < (h))
#endif
}
#endif
