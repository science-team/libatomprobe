/* millterIndex.cpp :  Miller index sequence generation
 * Copyright (C) 2017  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "atomprobe/latticeplanes/millerIndex.h"
#include "atomprobe/helper/aptAssert.h"

#include <algorithm>


namespace AtomProbe 
{

using std::vector;

int gcd(int a, int b)
{
	//a and b need to be nonzero
	if(!a || !b)
		return 0;

	//make a and b positive
	a=abs(a);
	b=abs(b);

	//make a >=b
	if(b>a)
		std::swap(a,b);

	//repeatedly take the modulus of a with smaller b
	while(b)
	{
		int t;
		t=b;
		b=a%b;
		a=t;
	}

	return a;
	
}

MILLER_TRIPLET::MILLER_TRIPLET(int a, int b, int c)
{
	idx[0]=a;
	idx[1]=b;
	idx[2]=c;
}

int MILLER_TRIPLET::h() const
{
	return idx[0];
}

int MILLER_TRIPLET::k() const
{
	return idx[1];
}

int MILLER_TRIPLET::l() const
{
	return idx[2];
}

void MILLER_TRIPLET::simplify()
{
	int gcdAll;
	gcdAll = gcd( gcd(idx[0],idx[1]),idx[2]);

	idx[0]/=gcdAll;
	idx[1]/=gcdAll;
	idx[2]/=gcdAll;
}


bool MILLER_TRIPLET::operator==(const MILLER_TRIPLET &m) const
{
	return (m.idx[0] == idx[0] && m.idx[1]==idx[1] && m.idx[2]==idx[2]);
}

#ifdef DEBUG
bool testMillerTriplets();

bool millerTest()
{
	if(!testMillerTriplets())
		return false;

	return true;
}

bool testMillerTriplets()
{

	MILLER_TRIPLET m(4,6,2);
	m.simplify();

	TEST( (m == MILLER_TRIPLET(2,3,1)) , "Index simplification");



	return true;

}
#endif
}
