/* generate.cpp :  Crystal tiling functions
 * Copyright (C) 2020  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "atomprobe/latticeplanes/generate.h"

namespace AtomProbe
{
using std::vector;

void CrystalGen::extractPositions(std::vector<Point3D> &data)
{
	data.resize(localData.size());
#pragma omp parallel for
	for(size_t ui=0;ui<data.size();ui++)
		data[ui]=localData[ui].getPosRef();

	localData.clear();
}

void CrystalGen::mirrorOut()
{
	vector<IonHit> h;
	h.swap(localData);


	localData.resize(h.size()*8);

	size_t offset=0;
	for(int ui=0;ui<8;ui++)
	{
		//Generate a point that is one of any of the (+-1,+-1,+-1) 
		// permutations 
		Point3D p;
		p=Point3D(2*(ui&1)-1, 2*((ui&2)>>1) -1, 2*((ui&4)>>2) -1);

		//Multiply this point by the data
		for(unsigned int uj=0;uj<h.size();uj++)
		{

			localData[offset].setPos(p*h[uj].getPosRef());
			localData[offset].setMassToCharge(h[uj].getMassToCharge());
			offset++;
		}
	}
	

}

SimpleCubicGen::SimpleCubicGen(float latticeSpacing, float massToC, const Point3D &p)
{
	spacing=latticeSpacing;
	massToCharge=massToC;
	farPoint=p;
}

unsigned int SimpleCubicGen::generateLattice()
{
	//Firstly we have to load the unit cell, which in a cubic
	//lattice and thus contains one atom 
	IonHit atom(Point3D(0,0,0),0);
	//atomic spacing in nanometres

	if(std::isnan(massToCharge))
		return CRYSTAL_BAD_UNIT_CELL;
	
	//check for lack of atomic spacing
	if(spacing <= 0.0f)
		return CRYSTAL_BAD_UNIT_CELL;
	
	//check for a good bounding value
	for(unsigned int ui=0; ui<3; ui++)
	{
		if(farPoint.getValue(ui) <=0.0f)
			return CRYSTAL_BAD_UNIT_CELL;
	}

	//OK, so we have checked we have a good unit cell, now lets populate!
	//estimate how many entries we will need in localData
	localData.clear();
	
	//Use volume estimation to estimate number of points to be stored
	localData.reserve((int)(farPoint.getValue(0)*farPoint.getValue(1)*
				farPoint.getValue(2)/(spacing*
					spacing*spacing)));
	

	//set up the positions of the unit cell
	//These have been kept as function calls because to
	//ensure internal consistency

	//Create the crystal lattice 
	// FIXME: With sufficiently nasty input, this could inf. loop
	for(float fX=0; fX<farPoint.getValue(0); fX+=spacing)
	{
		for(float fY=0; fY<farPoint.getValue(1); fY+=spacing)
		{
			for(float fZ=0; fZ<farPoint.getValue(2);fZ+=spacing)
			{
				atom.setPos(Point3D(fX,fY,fZ));
				localData.push_back(atom);
			}
		}
	}
	return 0;
}

FaceCentredCubicGen::FaceCentredCubicGen(float latticeSpacing, float massToCCorner,
		const float *massToCFace, const Point3D &p)
{
	spacing=latticeSpacing;
	massToChargeCorner=massToCCorner;
	for(unsigned int ui=0;ui<3;ui++)
		massToChargeFace[ui]=massToCFace[ui];
	farPoint=p;
}

unsigned int FaceCentredCubicGen::generateLattice()
{

	//the option vector must contain the unit cell
	//indices are in reciprocal values (integer)
	//if reciprocal is infinite, it becomes 0	
	
	//The unit cell has 14 vertices of which 4 are unique
	// the corner atoms are all the same,
	// the faces however have up/down, east/west,north/south
	// as unique groups
	IonHit atomList[4];
	//Set the mass to charges to a bad value
	atomList[0].setMassToCharge(massToChargeCorner);
	for(unsigned int ui=0;ui<3;ui++)
		atomList[1+ui].setMassToCharge(massToChargeFace[ui]);
	
	//Check atomic spacing is valid
	if(spacing<=0.0f)
		return CRYSTAL_BAD_UNIT_CELL;

	//check for a good bounding value
	for(unsigned int ui=0; ui<3; ui++)
	{
		if(farPoint.getValue(ui) <=0.0f)
			return CRYSTAL_BAD_UNIT_CELL;
	}

	//OK, so now we have a good unit cell lets do the replication thing!

	//This is an approximation and ignores some boundary issues
	//but it is good enough for a call to "reserve".
	localData.reserve(localData.size() + (unsigned int)(farPoint.getValue(0)/spacing)*
				(unsigned int)(farPoint.getValue(1)/spacing)*
				(unsigned int)(farPoint.getValue(2)) );
	
	//Top view
	//===============
	//*    D      *
	//
	//O    X      O
	//
	//*    D      *
	
	//Firstly lay down the simple cubic lattice (*s)
	float xMax,yMax,zMax;
	Point3D pt;
	xMax = farPoint.getValue(0);
	yMax = farPoint.getValue(1);
	zMax = farPoint.getValue(2);

	IonHit curAtom = atomList[0];
	for(float fX=0.0f; fX<xMax; fX+=spacing)
	{
		for(float fY=0.0f; fY<yMax; fY+=spacing)
		{
			for(float fZ=0.0f; fZ<zMax; fZ+=spacing)
			{
				curAtom.setPos(fX,fY,fZ);
				localData.push_back(curAtom);
			}
		}
	}
	
	//Lay down the "X" sites
	curAtom = atomList[1];
	for(float fX=spacing/2.0f; fX<xMax; fX+=spacing)
	{
		for(float fY=spacing/2.0f; fY<yMax; fY+=spacing)
		{
			for(float fZ=0.0f; fZ<zMax; fZ+=spacing)
			{
				curAtom.setPos(fX,fY,fZ);
				localData.push_back(curAtom);
			}
		}
	}

	//Lay down the D sites	
	curAtom = atomList[2];
	for(float fX=spacing/2.0f; fX<xMax; fX+=spacing)
	{
		for(float fY=0.0; fY<yMax; fY+=spacing)
		{
			for(float fZ=spacing/2.0f; fZ<zMax; fZ+=spacing)
			{
				curAtom.setPos(fX,fY,fZ);
				localData.push_back(curAtom);
			}
		}
	}

	//Lay down the "O" sites
	curAtom = atomList[3];
	for(float fX=0.0; fX<xMax; fX+=spacing)
	{
		for(float fY=spacing/2.0f; fY<yMax; fY+=spacing)
		{
			for(float fZ=spacing/2.0f; fZ<zMax; fZ+=spacing)
			{
				curAtom.setPos(fX,fY,fZ);
				localData.push_back(curAtom);
			}
		}
	}

	return 0;
}

BodyCentredCubicGen::BodyCentredCubicGen(float latticeSpacing, float massToCCorner,
		const float massToCCentre, const Point3D &p)
{
	spacing=latticeSpacing;
	massToChargeCorner=massToCCorner;
	massToChargeCentre=massToCCentre;
	farPoint=p;
}

unsigned int BodyCentredCubicGen::generateLattice()
{

	//the option vector must contain the unit cell
	//indices are in reciprocal values (integer)
	//if reciprocal is infinite, it becomes 0	
	
	//The unit cell has 14 vertices of which 4 are unique
	// the corner atoms are all the same,
	// the faces however have up/down, east/west,north/south
	// as unique groups
	IonHit atomList[2];
	//Set the mass to charges to a bad value
	atomList[0].setMassToCharge(massToChargeCorner);
	atomList[1].setMassToCharge(massToChargeCentre);
	
	//Check atomic spacing is valid
	if(spacing<=0.0f)
		return CRYSTAL_BAD_UNIT_CELL;

	//check for a good bounding value
	for(unsigned int ui=0; ui<3; ui++)
	{
		if(farPoint.getValue(ui) <=0.0f)
			return CRYSTAL_BAD_UNIT_CELL;
	}

	//OK, so now we have a good unit cell lets do the replication thing!

	//This is an approximation and ignores some boundary issues
	//but it is good enough for a call to "reserve".
	localData.reserve(localData.size() + (unsigned int)(farPoint.getValue(0)/spacing)*
				(unsigned int)(farPoint.getValue(1)/spacing)*
				(unsigned int)(farPoint.getValue(2)) );
	
	
	//Firstly lay down the simple cubic lattice 
	// then fill in the centre
	float xMax,yMax,zMax;
	Point3D pt;
	xMax = farPoint.getValue(0);
	yMax = farPoint.getValue(1);
	zMax = farPoint.getValue(2);

	IonHit curAtom = atomList[0];
	IonHit curCentreAtom = atomList[1];
	for(float fX=0.0f; fX<xMax; fX+=spacing)
	{
		for(float fY=0.0f; fY<yMax; fY+=spacing)
		{
			for(float fZ=0.0f; fZ<zMax; fZ+=spacing)
			{
				curAtom.setPos(fX,fY,fZ);
				localData.push_back(curAtom);

				//FIXME: This is quite inefficient.
				Point3D pCentre;
				pCentre=Point3D(fX+spacing/2.0f,
					fY+spacing/2.0f,fZ+spacing/2.0f);
				if(pCentre[0]<farPoint[0] && pCentre[1] < farPoint[1] 
					&& pCentre[2] < farPoint[2])
				{
					curCentreAtom.setPos(pCentre);
					localData.push_back(curCentreAtom);
				}
				
			}
		}
	}
	

	return 0;
}

#ifdef DEBUG
#include "atomprobe/helper/aptAssert.h"
#include "helper/helpFuncs.h"

bool runGenerateTests()
{
	SimpleCubicGen scg(1,1,Point3D(10,10,10));
	scg.generateLattice();
	
	vector<Point3D> pts;
	scg.extractPositions(pts);
	TEST(pts.size() > 800,"ion count");
	BoundCube bc;
	bc.setBounds(pts);

	for(unsigned int ui=0;ui<3;ui++)
	{
		TEST(TOL_EQ_V(bc.getSize(0),10,1.1),"cube size");	
	}
	return true;
}
#endif


}

