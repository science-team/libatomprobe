/*
 *	atomprobe.cpp - library-wide function implementations
 *	Copyright (C) 2015, D Haley 

 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.

 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.

 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "atomprobe/atomprobe.h"

#include <cstdlib>
#include <ctime>
#include <chrono>

namespace AtomProbe
{

LibVersion libVersion;
RandNumGen randGen;

static uint64_t getUnixTimeStamp(const std::time_t* t = nullptr)
{
    //if specific time is not passed then get current time
    std::time_t st = t == nullptr ? std::time(nullptr) : *t;
    auto secs = static_cast<std::chrono::seconds>(st).count();
    return static_cast<uint64_t>(secs);
}

void LibVersion::checkDebug()
{
#ifdef DEBUG
	if(!isDebug)
#else
	if(isDebug)
#endif
	{
		std::cerr << "DEBUG MISMATCH!" << std::endl;
		abort();
	}
}

RandNumGen::RandNumGen()
{
	rng = gsl_rng_alloc(gsl_rng_mt19937);

	auto t = std::chrono::system_clock::now();

	time_t tt  =std::chrono::system_clock::to_time_t(t);
	gsl_rng_set(rng, tt);                  // set seed
}


RandNumGen::~RandNumGen()
{
	gsl_rng_free(rng);
}
}
