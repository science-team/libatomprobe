/* readpos.h: Standalone c-only example program for reading Atom Probe "POS" files
 *		Requires a C99 compiler, notably *NOT* the free MS Visual Studio compiler.
 * Copyright (C) 2015  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef READPOS_H
#define READPOS_H

//Read  a pos file into its xyz and mass/charge components.
//input : filename.
// output:  x,y,z,m data and number of points.
// if no error, zero is returned. Otherwise:
// Err: 
//	1 - File could not be opened
//	2 - filesize could not be determined
//	3 - size was not a multiple of 16 - cannot be posfile
//	4 - Unable to allocate memory
//	5 - cannot read file contents 

//If no error, and nPts is nonzero, then you must free() the returned data
unsigned int readPos(const char *filename,
		float **x, float **y, float **z, float **m, unsigned int *nPts);

#endif
