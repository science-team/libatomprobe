/* readpos.c: Standalone c-only example program for reading Atom Probe "POS" files
 *		Requires a C99 compiler, notably *NOT* the free MS Visual Studio compiler.
 * Copyright (C) 2015  Daniel Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "readpos.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Endian detection code
//------------
#if defined (_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
	#ifndef __LITTLE_ENDIAN__
		#define __LITTLE_ENDIAN__
	#endif
#else
	#ifdef __linux__
		#include <endian.h>
	#endif
#endif

#ifdef __BYTE_ORDER
	#if __BYTE_ORDER == __BIG_ENDIAN
		#ifndef __BIG_ENDIAN__
			#define __BIG_ENDIAN__
		#endif
	#elif __BYTE_ORDER == __LITTLE_ENDIAN
		#ifndef __LITTLE_ENDIAN__
			#define __LITTLE_ENDIAN__
		#endif
	#elif __BYTE_ORDER == __PDP_ENDIAN
		#ifndef __ARM_ENDIAN__
			#define __ARM_ENDIAN__
		#endif
	#else
		#error "Endian determination failed"
	#endif
#endif
//------------



#if defined(WIN32) || defined(WIN64)
#include <windows.h>
#else 
	#if defined(__linux__)
		#include <sys/types.h>
		#include <sys/stat.h>
		#include <unistd.h>
	#endif
#endif

void floatSwapBytes(float *inFloat)
{
	//Use a union to avoid strict-aliasing error
	union FloatSwapUnion{
	   float f;
	   char c[4];
	} ;
	union FloatSwapUnion fa,fb;
	fa.f = *inFloat;

	fb.c[0] = fa.c[3];
	fb.c[1] = fa.c[2];
	fb.c[2] = fa.c[1];
	fb.c[3] = fa.c[0];

	*inFloat=fb.f;
}


unsigned int filesize_tmp(FILE *f)
{
	unsigned int origPos=ftell(f);
	fseek(f,0,SEEK_END);
	
	unsigned int size;
	size=ftell(f);
	fseek(f,origPos,SEEK_SET);
	return size;
}

unsigned int filesize(const char *filename)
{
		
#if defined(WIN32) || defined(WIN64)
	HANDLE hfile = CreateFile(filename,GENERIC_READ,FILE_SHARE_READ,
		NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,NULL);

	if(hFile == INVALID_HANDLE_VALUE)
		return (unsigned int) -1;

	DWORD dwFileSize = GetFileSize(hFile,  NULL);
	CloseFile(hFile);

	return dwFileSize;
#else
	struct stat statBuf;
	if(stat(filename, &statBuf) == -1)
		return(unsigned int) -1;
	else
		return statBuf.st_size;
#endif
}


unsigned int readPos(const char *filename,
		float **x, float **y, float **z, float **m, unsigned int *nPts)
{

	*nPts=0;	
	FILE *f;

	f = fopen(filename,"r");
	
	if(!f)
		return 1;

	unsigned int size;
	size= filesize(filename);
	if(size == (unsigned int)-1 )
	{
		fclose(f);
		return 2;
	}

	if(!size)
	{	
		fclose(f);
		return 0;
	}

	//must be mod 16 bytes ( 4 byte floats, 4 times)
	if(size %16 )
	{
		fclose(f);
		return 3;
	}

	unsigned int numEntries;
	numEntries=size/16;

	*x = malloc(numEntries*4);
	if(!(*x))
	{
		fclose(f);
		return 4;
	}
	*y = malloc(numEntries*4);
	if(!(*y))
	{
		fclose(f);
		free(*x);
		return 4;
	}
	*z = malloc(numEntries*4);
	if(!(*z))
	{
		fclose(f);
		free(*x);
		free(*y);
		return 4;
	}
	*m = malloc(numEntries*4);
	if(!(*m))
	{
		fclose(f);
		free(*x);
		free(*y);
		free(*z);
		return 4;
	}	


	unsigned int errCode=0;

	float buf[16];
	unsigned int ui;
	for(ui=0;ui<numEntries; ui++)
	{
		if(fread((void*)buf,16,1,f) !=1)
		{
			errCode=5;
			break;
		}


#if defined(__LITTLE_ENDIAN__)
		unsigned int uj;
		for(uj=0;uj<4;uj++)	
		{
			floatSwapBytes(buf+uj);
		}
#endif
		//Unmap the data
		(*x)[ui] = buf[0];
		(*y)[ui] = buf[1];
		(*z)[ui] = buf[2];
		(*m)[ui] = buf[3];

	}
	

	fclose(f);
	if(errCode)
	{
		free(*x);
		free(*y);
		free(*z);
		free(*m);
	}

	*nPts=size;

	return errCode;
}


int main(int argc, char **argv)
{

	float *x,*y,*z,*m;
	unsigned int nPts;	

	unsigned int errCode;
	errCode=readPos("test.pos",&x,&y,&z,&m,&nPts);

	if(errCode)
	{
		printf("Did not load properly error code is %u \n",errCode);
	}
	else
	{
		printf("we loaded %u points!\n",nPts);

		if(nPts > 10)
			nPts=10;

		unsigned int ui;
		for(ui=0;ui<nPts; ui++)
			printf("point : ( %f,%f,%f,%f)\n",x[ui],y[ui],z[ui],m[ui]);

		free(x);
		free(y);
		free(z);
		free(m);
	}


	return 0;	
}
