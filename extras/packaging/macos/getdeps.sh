#!/bin/bash

if [ x`which port` == x"" ] ;
	echo "Macports not found. you need to install it from https://www.macports.org/"
	exit 1;
fi

sudo port selfupdate

GCCPACKAGE="gcc5"
sudo port install $GCCPACKAGE
sudo port select gcc $GCCPACKAGE

DEPS="cmake qhull gsl"
sudo port install $DEPS
