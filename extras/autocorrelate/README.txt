Compute the radial autocorrelation function defined in the "Blue" atom
 probe book (Atom Probe Field Ion Microscopy)


The POSFILE/RANGEFILE and radius and binstep are as they say (search
radius for function, and step size of function).

  The sampling value should be in the range (0,1], and determines what
fraction of ions are retained from the initial dataset before the
calculations are done. A small value means that the calculation will go
quickly, but the result will be noisy. A large value (eg 1.0) will mean
that the program runs at its slowest, but the output is of maximum quality.
I recommend trying small values like 0.05, then increasing it as you need
higher quality results.

Only ranged ions are kept, and they are not decomposed (C2 is considered a
different species to C). You'll need to tune your range file if you want to
do this differently.
